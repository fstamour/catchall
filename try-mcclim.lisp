(in-package #:common-lisp-user)

(defpackage #:try-mcclim
  (:nicknames ":tmclim")
  (:use :cl))

(in-package #:try-mcclim)

(ql:quickload 'mcclim)

;; For NixOS
(setf mcclim-truetype:*truetype-font-path* #p"~/.nix-profile/share/fonts/truetype/")
(probe-file #p"~/.nix-profile/share/fonts/truetype/")

(clim:define-application-frame hello
    ;; superclasses
    ()
  ;; Slots
  ()
  ;; Options
  (:panes (interactor :interactor))
  (:layouts (default interactor)))

(defparameter *hello-frame* (clim:make-application-frame 'hello)
  "an hello-application frame")

(clim:run-frame-top-level *hello-frame*)
;; That works...


(ql:system-apropos-list "clim")
(#<QL-DIST:SYSTEM acclimation / acclimation-20180831-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM acclimation-temperature / acclimation-20180831-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM automaton / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clim / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clim-basic / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clim-core / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clim-debugger / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clim-examples / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clim-examples/superapp / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clim-lisp / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clim-listener / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clim-pdf / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clim-pdf/test / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clim-postscript / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clim-postscript-font / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clim-postscript/test / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clim-widgets / clim-widgets-20180228-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM climacs / climacs-20190107-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM climc / climc-20150923-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM climc-test / climc-20150923-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM climon / climon-20151031-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM climon-test / climon-20151031-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clouseau / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM clouseau/test / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM conditional-commands / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM drei-mcclim / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM drei-mcclim/test / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM esa-mcclim / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM functional-geometry / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-backend-common / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-backend-common/test / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-bezier / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-bezier/clx / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-bezier/core / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-bitmaps / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-clx / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-clx-fb / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-clx/truetype / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-fonts / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-fonts/clx-truetype / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-fonts/truetype / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-franz / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-image / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-layouts / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-layouts/tab / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-null / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-raster-image / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-raster-image/test / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-render / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim-tree-with-cross-edges / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim/extensions / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim/looks / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim/test / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM mcclim/test-util / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM persistent / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM scigraph / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM scigraph/dwim / mcclim-20200218-git / quicklisp 2020-02-18>
                  #<QL-DIST:SYSTEM slim / mcclim-20200218-git / quicklisp 2020-02-18>)


(ql:quickload 'clim-listener)
(clim-listener:run-listener)



(clim:define-application-frame hello
    ;; superclasses
    ()
  ;; Slots
  ()
  ;; Options
  (:pointer-documentation t)
  (:panes (interactor :interactor)
          (application :application :display-time nil))
  (:layouts (default (clim:vertically () application interactor))))

(progn
  (when (eq :enabled (clim:frame-state *hello-frame*))
    (clim:frame-exit *hello-frame*))
  (defparameter *hello-frame* (clim:make-application-frame 'hello)
    "an hello-application frame")
  (clim:run-frame-top-level *hello-frame*))

(define-hello-command (com-quit :name t) ()
  (clim:frame-exit *hello-frame*))

;; The "~%" is really important, the result doesn't appear without it.
(define-hello-command (com-pwd :name t) ()
  (format t "~&~a~%" (truename ".")))

(define-hello-command (com-ls :name t) ()
  (format t "~{~A~^~%~}~%"
          (directory #p"./*")))

;; Now we can type "Quit" and it will quit.



(ql:quickload 'clim-examples)

(clim:run-frame-top-level
 (clim:make-application-frame
  'clim-demo::draggable-graph-demo))


;;; Playing with the draggable graph demo to try to understand how it works
;;;
;;; After some investigation, it relies on clim-core/graph-formatting.lisp
;;; It's all defined in the package clim-internals, nickenamed climi.
;;; I'm not sure if any part of the file is exported.
;;;
;;; Looking at graph-formatting.lisp, it seem to support:
;;; - trees
;;; - directed acyclic graphs
;;; - directed graph
;;; See climi::+built-in-graph-types+
;;;
;;; - generate-graph draws the graph
;;; - redisplay-edges is called during drag
;;; - define-presentation-to-command-translator record-dragging-translator
;;;  links the presentation to the command to drag
;;;
;;; I create a package that only uses :cl, so that all the elements from clim
;;; are clear. Once I did that, I spotted the function "format-graph-from-roots"
;;; used in "generate-graph" and figured that by adding ":merge-duplicates t"
;;; it will understand that I'm trying to draw a graph, not just a tree.
;;; => (setf graph-type (or graph-type (if merge-duplicates :digraph :tree)))

(in-package #:common-lisp-user)

(defpackage #:draggable-graph
  (:use :cl))

(in-package #:draggable-graph)

(clim:define-application-frame draggable-graph-demo () ()
  (:menu-bar nil)
  (:pane (clim:make-pane 'clim:application-pane
                         :width :compute
                         :height :compute
                         :display-function 'generate-graph
                         :display-time t)))

;; static as opposed to generated by calling (class-name)
(defparameter *static-graph*
  '((1 . 2)
    (1 . 3)
    (3 . 4)
    (3 . 5)
    ;; Maybe don't start with a loop
    (5 . 1)
    ))

#+nil ;; original
(defun generate-graph (frame pane)
  (declare (ignore frame))
  (format-graph-from-roots
   (list (find-class 'number))
   (lambda (object stream)
     (present (class-name object) (presentation-type-of object) :stream stream))
   #'c2mop:class-direct-subclasses
   :stream pane))

(defun generate-graph (frame pane)
  "Draws the graph"
  (declare (ignore frame))
  (let ((edge-seen (make-hash-table :test 'equal)))
    (clim:format-graph-from-roots
     ;; Root objects
     (list 1)
     ;; Object printer
     (lambda (object stream)
       ;; (setf (gethash object presentations))
       (clim:present object (clim:presentation-type-of object) :stream stream))
     ;; inferior-producer
     (lambda (parent-id)
       (loop
         :for edge :in *static-graph*
         ;; prevent infinite loop
         :when (and (eq (car edge) parent-id)
                    (not (gethash edge edge-seen)))
           :collect (prog1 (cdr edge)
                      (setf (gethash edge edge-seen) edge))))
     ;; Keyword arguments to format-graph-from-roots
     :merge-duplicates t
     :stream pane)))

(defun find-graph-node (record)
  "Searches upward until a graph node parent of the supplied output record is found."
  (loop for current = record then (clim:output-record-parent current)
        while current
        when (clim:graph-node-output-record-p current)
          do (return current)))

(defun node-edges (node)
  (append (alexandria:hash-table-values (slot-value node 'climi::edges-from))
          (alexandria:hash-table-values (slot-value node 'climi::edges-to))))

(defun stupid-copy-rectangle (region)
  "Copy a rectangle (an geometric object)"
  (clim:with-bounding-rectangle* (x0 y0 x1 y1) region
    (clim:make-rectangle* x0 y0 x1 y1)))

(defun node-and-edges-region (node edges)
  (stupid-copy-rectangle (reduce #'clim:region-union edges :initial-value node)))

(defun redisplay-edges (graph edges)
  (dolist (edge edges)
    (climi::layout-edge-1 graph (climi::from-node edge) (climi::to-node edge))))

(define-draggable-graph-demo-command (com-drag-node)
    ((record t) (offset-x real :default 0) (offset-y real :default 0))
  "The drag command"
  (let* ((stream *standard-output*)
         ;; Find the node we're trying to drag
         (node-record (find-graph-node record))
         ;; Find the edges of that node
         (edge-records (node-edges node-record))
         ;; Find the graph the node is part of?
         (graph-record (clim:output-record-parent node-record))
         ;; To keep track of region that was erased when issuing a repaint
         (erase-region))
    (assert (typep graph-record 'clim:graph-output-record))
    (clim:drag-output-record stream node-record
                             :feedback (lambda (record stream old-x old-y x y mode)
                                         (declare (ignore old-x old-y))
                                         (ecase mode
                                           (:erase
                                            (clim:erase-output-record record stream)
                                            (map nil #'clim:clear-output-record edge-records)
                                            (setf erase-region
                                                  (node-and-edges-region record edge-records)))
                                           (:draw
                                            (setf (clim:output-record-position record)
                                                  (values (- x offset-x) (- y offset-y)))
                                            (clim:add-output-record record graph-record)
                                            (redisplay-edges graph-record edge-records)
                                            ;; FSTA: There a bug when you mode a node and the edge is not
                                            ;; on the same "corner" on the node, there are some traces of the
                                            ;; previous edge that is not erased
                                            (clim:repaint-sheet
                                             stream (clim:region-union (or erase-region clim:+nowhere+)
                                                                       (node-and-edges-region record edge-records))))))
                             :finish-on-release t
                             :multiple-window nil)))

(clim:define-presentation-to-command-translator record-dragging-translator
    (t com-drag-node draggable-graph-demo
       :tester ((object presentation)
                (find-graph-node presentation)))
  (object presentation x y)
  (multiple-value-bind (old-x old-y) (clim:output-record-position presentation)
    (list presentation (- x old-x) (- y old-y))))


(clim:run-frame-top-level (clim:make-application-frame 'draggable-graph-demo))
