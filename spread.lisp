(defpackage #:catchall.spread
  (:documentation "Spreading uniformly discrete resources into buckets")
  (:use #:cl))

(in-package #:catchall.spread)

(let* ((resources (loop :for i :below 90 :collect i))
       (buckets (loop :for i :below 11 :collect i))
       (r (length resources))
       (b (length buckets))
       (allocations
         (multiple-value-bind (base remainder) (floor r b)
           (loop :for i :below b
                 :collect (if (< i (+ base remainder))
                              base
                              (+ base remainder)))))
       (number-of-resources-allocated
         (reduce #'+ allocations)))
  (list allocations
        number-of-resources-allocated
        (= r number-of-resources-allocated)
        (= b (length allocations))))
;; => ((8 8 8 8 8 8 8 8 8 9 9) 90 T T)

;; Ok... I thought I was going to have a harder time xD
#|

Pseudo-javascript:

const base = Math.floor(r/b);
const remainder = r % b;

const allocations = Array.from({length: 5}, (v, k) => k+1);

|#

(defun allocate (r b)
  (multiple-value-bind (base remainder) (floor r b)
    (let ((first-run (if (zerop base) 1 base))
          (first-run-length (if (zerop base) remainder (- b remainder)))
          (second-run (if (zerop base) 0 (1+ base))))
      (loop :for i :below b
            :if (< i first-run-length)
              :collect first-run
            :else
              :collect second-run))))

(defun test (r b)
  (let* ((allocations (allocate r b))
         (number-of-resources-allocated
           (reduce #'+ allocations)))
    (list allocations
          number-of-resources-allocated
          (= r number-of-resources-allocated)
          (= b (length allocations)))))

(list
 (test 10 10)
 (test 10 11)
 (test 11 10)
 (test 90 11)

 (test 1 10)
 (test 2 10))
