# This is my first try at making a flake that includes sbcl, emacs,
# slime and swank, all-in-one.
#
# TODO Move the aliases into actual scripts, so it can work with any shells (not just bash)
# TODO Move some things into their own files
# TODO Integrate with direnv
# TODO Setup quicklisp in current folder, at runtime
# TODO Make emacs optional
# TODO Add [n]vim as an option
# TODO Readme
# TODO Figure out how to actually make a package out of it. Right now, I just make a devShell
# TODO Look at https://github.com/numtide/devshell and
#
# Useful links:
# - https://nix.dev/tutorials/ad-hoc-developer-environments
{
  description = "Common Lisp Dev environment";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        # Alias, to shorten the rest of the code.
        pkgs = nixpkgs.legacyPackages.${system};
        lispWithPackages = pkgs.lispPackages_new.lispWithPackages;

        # This derivation is sbcl, plus some systems already compiled
        # and ready to be loaded.
        sbclWithPackages = (lispWithPackages "${pkgs.sbcl}/bin/sbcl --script"
          (p: [
            # TODO Maybe not load both swank and slynk
                p.swank
                p.slynk
                # p.quicklisp-slime-helper This fails to load, because quicklisp is not setup
                # p.clhs
                p.alexandria
                p.april
              ]));

        # This derivation is used to find the path to swank, so that
        # emacs can load it too.
        slime-dir =
          pkgs.stdenv.mkDerivation {
            name = "emacs-init";
            phases = [ "installPhase" ];
            buildInputs = [sbclWithPackages];
            installPhase = ''
            source $stdenv/setup
            mkdir $out
            sbcl --noinform --no-sysinit --no-userinit \
                --eval '(require :asdf)' \
                --eval '(princ (uiop:pathname-directory-pathname (asdf:system-source-file :swank)))' \
                --quit \
                > $out/slime
            '';
          };

        # An emacs init file, it loads slime and configures it
        emacs-use-slime = pkgs.writeText "use-slime.el"
          ''
          ;; TODO Check if slime is already loaded and/or configured
          ;; TODO Check if **sly** is already loaded
           (let ((slime-directory "${builtins.readFile "${slime-dir}/slime"}"))
             (add-to-list 'load-path slime-directory)
             (require 'slime-autoloads)
             (setq slime-backend (expand-file-name "swank-loader.lisp" slime-directory))
             (setq slime-path slime-directory)
             (slime-setup '(slime-fancy)))

           (setq slime-lisp-implementations
             `((sbcl ("${sbclWithPackages}/bin/sbcl" "--no-sysinit" "--no-userinit"
                     "--eval" "(require 'asdf)"))))
          '';

        # A small script to start sbcl and start swank
        # TODO It could be nice to have a function that generates the
        # sbcl command given a list of forms to eval
        # TODO optional --no-sysinit --no-userinit
        swank-listener = pkgs.writeScriptBin "swank-listener"
          ''
          ${pkgs.rlwrap}/bin/rlwrap ${sbclWithPackages}/bin/sbcl --noinform \
            --eval "(asdf:load-system '#:swank)" \
            --eval "(swank:create-server :dont-close t)" \
            --eval '(print (merge-pathnames "slime.el" (uiop:pathname-directory-pathname (asdf:system-source-file :swank))))'
          '';

        # TODO A small script to start sbcl and start swank

        # Small script to load slime (swank's emacs client) using emacsclient
        swank-emacsclient = pkgs.writeScriptBin "swank-emacsclient"
          ''
          ${pkgs.emacs}/bin/emacsclient -n --eval "(load \"${emacs-use-slime}\")"
          '';

      in {
        devShells.default = pkgs.mkShell {
          buildInputs = [
            sbclWithPackages
            pkgs.rlwrap
            # pkgs.emacs

            swank-listener
            swank-emacsclient
          ];

          # shellHook =
          #   ''
          #     # For debugging
          #     # echo "${emacs-use-slime}"
          #     # cat "${emacs-use-slime}"

          #   start() {
          #           emacs -nw -q -l "${emacs-use-slime}" --eval '(slime)'
          #   }

          #   echo use \"swank-listener\" to start hacking
          # '';
        };
      });

  # TODO use rlwrap
  # This kinda works, but the paths inside are wrong:
  #                 --eval '(clhs:install-clhs-use-local :destination-directory ".")' \


  # Install quicklisp in a specific folder:
  # sbcl --no-sysinit --no-userinit --load quicklisp.lisp --eval '(quicklisp-quickstart:install :path #P"./quicklisp/")' --quit
  # sbcl also has a "--userinit" argument
  #
  # Start emacs with a specific configuration:
  # emacs -q -l ./init.el
}
