# https://nixos.wiki/wiki/Flakes
{
  description = "Common Lisp Dev environment";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        sbcl = (pkgs.lispPackages_new.lispWithPackages "${pkgs.sbcl}/bin/sbcl --script"
              (p: [
                p.swank
                # p.quicklisp-slime-helper This fails to load, because quicklisp is not setup
                # p.clhs
                p.alexandria
                p.april
              ]));

        slime =
          pkgs.stdenv.mkDerivation {
            name = "emacs-init";
            phases = [ "installPhase" ];
            buildInputs = [sbcl];
            # TODO Look into runCommand
            installPhase = ''
            source $stdenv/setup
            mkdir $out
            sbcl --noinform --no-sysinit --no-userinit \
                --eval '(require :asdf)' \
                --eval '(princ (uiop:pathname-directory-pathname (asdf:system-source-file :swank)))' \
                --quit \
                > $out/slime

            '';
          };

        emacs-init = pkgs.writeText "init.el"
          ''
           (let ((slime-directory "${builtins.readFile "${slime}/slime"}"))
             (add-to-list 'load-path slime-directory)
             (require 'slime-autoloads)
             (setq slime-backend (expand-file-name "swank-loader.lisp" slime-directory))
             (setq slime-path slime-directory)
             (slime-setup '(slime-fancy)))

           (setq slime-lisp-implementations
             `((sbcl ("${sbcl}/bin/sbcl" "--no-sysinit" "--no-userinit"
                     "--eval" "(require 'asdf)"))))
          '';
      in {
        # TODO Maybe try devshell
        # https://github.com/numtide/devshell
        # https://yuanwang.ca/posts/getting-started-with-flakes.html
        devShell = pkgs.mkShell {
          buildInputs = [
            # pkgs.sbcl
            sbcl
            pkgs.cowsay
            pkgs.rlwrap
            pkgs.emacs
          ];
          # finputsFrom = builtins.attrValues self.packages.${system};
          shellHook = ''
              # For debugging
              # echo "${emacs-init}"
              # cat "${emacs-init}"

            swank() {
              rlwrap sbcl --no-sysinit --no-userinit \
                --eval '(require :asdf)' \
                --eval '(mapcar (function asdf:load-system) (list :alexandria :swank))' \
                --eval '(swank:create-server :dont-close t)' \
                --eval '(print (merge-pathnames "slime.el" (uiop:pathname-directory-pathname (asdf:system-source-file :swank))))'
            }

            start() {
                    emacs -nw -q -l "${emacs-init}" --eval '(slime)'
            }

            echo use \"start\" to start hacking
          '';
        };
      });

  # TODO use rlwrap
  # This kinda works, but the paths inside are wrong:
  #                 --eval '(clhs:install-clhs-use-local :destination-directory ".")' \


  # Install quicklisp in a specific folder:
  # sbcl --no-sysinit --no-userinit --load quicklisp.lisp --eval '(quicklisp-quickstart:install :path #P"./quicklisp/")' --quit
  # sbcl also has a "--userinit" argument
  #
  # Start emacs with a specific configuration:
  # emacs -q -l ./init.el
}
