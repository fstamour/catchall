(cl:in-package #:cl-user)

(require 'cffi)

(defpackage #:nix
  (:use :cl)
  (:export #:add-library)
  (:documentation "Integrating cffi with nix

Provide a function to install a package in the current directory and
make it loadable by common lisp's cffi. It is not recommended for
production (you're probably better writing a default.nix), but this
is very useful for testing things from the REPL/listener."))

(in-package #:nix)

(defun outputs (name)
  "List the outputs of a derivation."
  (uiop:split-string
   (uiop:run-program
    (format nil
            "nix eval --raw '(builtins.concatStringsSep \"|\" (import <nixpkgs>{}).~A.outputs)'"
            name)
    :output :line)
   :separator "|"))

(defun ensure-one-output (name &optional (default "out"))
  "Choose the right output from a derivation, if possible"
  (let ((outputs (outputs name)))
    (cond
      ((= 1 (length outputs))
       (car outputs))
      ((and
        default
        outputs
        (find default outputs :test #'string=))
       (warn "nixpkgs attribute \"~a\" has multiple outputs (~{~a~^ ~}), using \"~a\"."
             name outputs default)
       default)
      (outputs
       (error "nixpkgs attribute \"~a\" has multiple outputs: ~%~{ - ~a~%~}"
              name outputs))
      (t
       (error "nixpkgs attribute \"~a\" doesn't have outputs." name)))))

;; TODO &optional output - to manually choose an output
(defun add-library (name)
  (let* (;; Validate the package's output
         (output (ensure-one-output name))
         ;; Install the package
         (results (uiop:run-program
                   (format nil "nix-build -E '(import <nixpkgs>{}).~a.~a' -o ~A"
                           name output name)
                   :output :lines))
         (output-path (merge-pathnames
                       (if (string= output "out")
                           (format nil "~a/" name)
                           (format nil "~a-~a/" name output)))))
    ;; Make sure we find the package
    (unless (probe-file output-path)
      (error "Failed to find symlink \"~a\" to \"~a\"."
             output-path
             (car results)))
    (let ((lib-path (merge-pathnames "lib/" output-path)))
      (unless (probe-file lib-path)
        (error "Failed to find \"lib/\" in the package at \"~a\"."
               output-path)))
    ;; Add the new path to CFFI's search directories
    (pushnew
     (merge-pathnames "lib/" output-path)
     cffi:*foreign-library-directories*
     :test #'equal)))

#|
Examples:

(add-library "xorg.libX11")
(add-library "glfw3")
(add-library "libGL")
|#
