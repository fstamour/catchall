;;;; This partially converts zim files to org-mode files.

(in-package #:common-lisp-user)

(ql:quickload '(alexandria cl-ppcre str cl-strings split-sequence))

(defpackage #:2020-01-10-zim-to-org-mode
  (:use :cl :alexandria))

(in-package #:2020-01-10-zim-to-org-mode)

(defun pathname-directory-contains-p (pathname directory-name)
  (position directory-name
            (pathname-directory pathname)
            :test #'string=))

(defun list-notes (&key (extensions
                         '("org" "txt" "md"))
                     (directory *root*))
  (sort (uiop:while-collecting
            (collect-file)
          (loop :for file :in (uiop:directory-files
                               directory
                               "**/*.*")
                :do
                   (cond
                     ;; Ignore some folders
                     ((some (curry #'pathname-directory-contains-p file)
                            '(".stversions"
                              ".stfolder"
                              ".git"
                              "trash"
                              "planck")))
                     ;; Ignore files that starts with a dot
                     ((starts-with #\. (pathname-name file)))
                     ;; Include those extensions
                     ((member (pathname-type file)
                              extensions
                              :test #'string-equal)
                      (collect-file file)))))
        #'string<
        ;; remove diacritics
        :key #'(lambda (pathname)
                 (cl-strings:clean-diacritics
                  (pathname-name pathname)))))

(defun zimp (pathname)
  (starts-with-subseq "Content-Type: text/x-zim-wiki"
                      (read-file-into-string pathname)))

#+nil
(zimp "Bouffe/Recettes/Marinade.txt")
#+nil
(zimp "index.org")

(defun convert-zim-to-org (string)
  (let ((lines
          (cdddr ;; Remove the first 3 lines
           (split-sequence:split-sequence
            #\newline
            (remove-windows-end-of-line
             string)))))
    (cl-strings:join
     (loop :for line :in lines
           :collect
           (loop
             :for (regex replacement) :in '((" =+$" "")
                                            ("^======" "*")
                                            ("^=====" "**")
                                            ("^====" "***")
                                            ("^===" "****")
                                            ("^==" "*****")
                                            ("^=" "******"))
             :do (setf line (ppcre:regex-replace-all regex line replacement))
             :finally (return line)))
     :separator (string #\newline))))

(defun read-zim-file-as-org (pathname)
  (convert-zim-to-org
   (read-file-into-string pathname)))

#+nil
(convert-zim-to-org
 (read-file-into-string
  (first
   (remove-if-not #'zimp (list-notes :extensions '("txt"))))))

(defparameter *zim-files*
  (remove-if-not #'zimp (list-notes :extensions '("txt"))))

;; Convert zim files to org files
#+nil
(loop :for pathname :in *zim-files*
      :do
         (let ((content (readablep pathname)))
           (if content
               (progn
                 (write-string-into-file (convert-zim-to-org content) pathname
                                         :if-exists :overwrite)
                 (format t "~&Wrote into ~s~%" pathname))
               (format t "~&Couldn't read ~s~%" pathname))))

(defun copy-pathname (pathname
                      &key host device directory name type version)
  (make-pathname
   :host  (or host (pathname-host pathname))
   :device  (or device (pathname-device pathname))
   :directory  (or directory (pathname-directory pathname))
   :name  (or name (pathname-name pathname))
   :type  (or type (pathname-type pathname))
   :version  (or version (pathname-version pathname))))

#+nil
(let* ((pathname #p"test.txt")
       (new-pathname (copy-pathname pathname :type "org")))
  new-pathname)

;; Rename all zim files
#+nil
(loop :for pathname :in *zim-files*
      :collect (rename-file pathname
                            (copy-pathname pathname :type "org")))
