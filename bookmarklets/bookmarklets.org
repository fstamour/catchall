#+title: Bookmarklets

#+begin_src elisp :results silent
  (setf org-confirm-babel-evaluate nil
        org-link-elisp-confirm-function nil)
#+end_src

* Utils

#+name: utils
#+begin_src js :eval never
  const complement = f => x => !f(x);

  /**************************************************/
  /* DOM utilities */

  const isElement = x => x.tagName || x.nodeName;

  function t(tagName) {
      const args = Array.prototype.slice.call(arguments, 1).flat();
      const children = args.filter(isElement);
      const options = Object.assign({}, ...args.filter(complement(isElement)));
      const element = document.createElement(tagName);
      for(let key of Object.keys(options)) {
          element[key] = options[key];
      }
      element.replaceChildren(...children);
      return element;
  }

  function text(string) {
      return document.createTextNode(string);
  }

  /**************************************************/

  const blink = (el) => {
      if(el.blink_animation) {
          clearInterval(el.blink_animation);
      }

      let step = 0;
      const maxTime = 5000; // in ms
      const interval = 5; // in ms
      const maxStep = maxTime / interval;
      const id = setInterval(() => {
          if(step > maxStep){
              clearInterval(id);
              el.style.backgroundColor = "";
          } else {
              const percent = step/maxStep*100; // Ascending
              const inversePercent = 100 - percent; // Descending
              const color = `hsl(50, 100%, 65%, ${inversePercent}%)`;
              el.style.backgroundColor = color;
              step++;
          }
      }, interval);

      el.blink_animation = id;
  };
#+end_src

* GitLab

** Bookmarklet to help navigating a job's log

#+name: gitlab-goto-step-bookmarklet
#+begin_src js :noweb yes :eval never :title "GitLab jobs - Jump to step"
  <<utils>>
  const steps = Array.from(document.querySelectorAll('div.js-line.log-line > span.term-fg-l-green'))
        .filter(el => el.textContent.startsWith('$'));

  const showModal = (content) => {
      const modal = t('div', content);

      const closeModal = (event) => {
          modal.remove();
      };

      const closeButton = t('button', text('X'));
      closeButton.onclick = closeModal;

      const controls = t('p', closeButton);
      modal.appendChild(controls);

      const style = `
        background: black;
        color: white;
        padding: 1em;
        position: fixed;
        /*float: left;*/
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);`;
      modal.style.cssText = style;
      document.body.appendChild(modal);
  }

  const stepsList = t('ol', steps.map(el => {
      const textEl = text(el.textContent);
      const link = t('a', textEl);
      link.onclick = () => {
          el.scrollIntoView({
              behavior: 'smooth',
              // vertical alignment
              block: 'center',
              // horizontal alignment
              // inline: 'center'
          });
          blink(el);
      };
      return t('li', link)
  }));

  showModal(stepsList);
#+end_src

** TODO Bookmarklet to mark "merged todo items" as done

#+begin_src js :eval never :title "GitLab - Todos cleanup"
  const todos = Array.from(document.getElementsByClassName('todo'));

  todos.filter(todo =>
    Array.from(todo.getElementsByTagName('span')).filter(el =>
    el.textContent == "Merged").length > 0);
#+end_src

** Show dates in GitLab Activities

#+begin_src js :noweb yes :eval never :title "GitLab - Show dates in GitLab Activities"
  <<utils>>

  var timeagos = [...document.getElementsByClassName('js-timeago')];

  timeagos.forEach(ta =>
      ta.appendChild(t('p', text(ta.title))));
#+end_src

* Modifying firefox bookmarks

I was trying to update firefox's bookmarks by modifying directly the
sqlite database that holds them, but it doens't work while firefox is
running because of locks on the database.

** Find the database location

#+name: db_path
#+begin_src shell
  find ~/.mozilla/ -name 'places.sqlite'
  find ~/snap/firefox/ -name 'places.sqlite'
#+end_src

#+RESULTS: db_path
: /home/fstamour/snap/firefox/common/.mozilla/firefox/2u580t9r.default/places.sqlite

#+RESULTS:
: /home/fstamour/.mozilla/firefox/2u580t9r.default/places.sqlite

** Validating that we can query Firefox's bookmark databases

*** List tables

#+begin_src shell :noweb yes :shebang #!/usr/bin/env bash :var db_path=db_path
  echo .tables | sqlite3 $db_path
#+end_src

#+RESULTS:
| moz_anno_attributes                     |
| moz_annos                               |
| moz_bookmarks                           |
| moz_bookmarks_deleted                   |
| moz_historyvisits                       |
| moz_inputhistory                        |
| moz_items_annos                         |
| moz_keywords                            |
| moz_meta                                |
| moz_origins                             |
| moz_places                              |
| moz_places_metadata                     |
| moz_places_metadata_groups_to_snapshots |
| moz_places_metadata_search_queries      |
| moz_places_metadata_snapshots           |
| moz_places_metadata_snapshots_extra     |
| moz_places_metadata_snapshots_groups    |
| moz_previews_tombstones                 |
| moz_session_metadata                    |
| moz_session_to_places                   |

*** Select some bookmarks

#+begin_src sql
  select
    places.id,
    places.URL,
    places.GUID,
    bookmarks.title,
    bookmarks.id,
    bookmarks.parent
  from
    moz_places as places
      join moz_bookmarks as bookmarks
      on places.id = bookmarks.fk
#+end_src

;; #+header: :stdin list-bookmarks
#+begin_src shell :noweb yes :shebang #!/usr/bin/env bash :var db_path=db_path
  cat <<EOF |
  <<list-bookmarks>>
  EOF
  sqlite3 $db_path | head
#+end_src

#+RESULTS:
|   3 | https://support.mozilla.org/products/firefox                                                                                                                        | lX5K3F4sV6RR | Get       | Help     |         8 |  7 |   |
|   4 | https://support.mozilla.org/kb/customize-firefox-controls-buttons-and-toolbars?utm_source=firefox-browser&utm_medium=default-bookmarks&utm_campaign=customize       | IQp9WflBv86P | Customize | Firefox  |         9 |  7 |   |
|   5 | https://www.mozilla.org/contribute/                                                                                                                                 | t-FPd9QOy94j | Get       | Involved |        10 |  7 |   |
|   6 | https://www.mozilla.org/about/                                                                                                                                      | jrsglmboAWUI | About     | Us       |        11 |  7 |   |
|  65 | https://support.mozilla.org/en-US/products/firefox                                                                                                                  | pYeQmAJZVCYk | Get       | Help     |        13 |  7 |   |
| 111 | https://support.mozilla.org/en-US/kb/customize-firefox-controls-buttons-and-toolbars?utm_source=firefox-browser&utm_medium=default-bookmarks&utm_campaign=customize | oMO93hKmFdGq | Customize | Firefox  |        14 |  7 |   |
| 284 | https://www.mozilla.org/en-US/contribute/                                                                                                                           | pDbCjLPNTPke | Get       | Involved |        15 |  7 |   |
| 359 | https://www.mozilla.org/en-US/about/                                                                                                                                | s-MFiNb6kq7D | About     | Us       |        16 |  7 |   |
|  65 | https://support.mozilla.org/en-US/products/firefox                                                                                                                  | pYeQmAJZVCYk | Help      | and      | Tutorials | 17 | 7 |
| 128 | place:type=6&sort=14&maxResults=10                                                                                                                                  | 45yncUPIxjxe | Recent    | Tags     |        18 |  2 |   |


* AWS

** Swap Account Id and IAM user name

Because Dashlane is stupid

#+begin_src js :eval never :title "AWS - Swap Account Id and IAM user name"
  const accountEl = document.getElementById('account');
  const account = accountEl.value;

  const usernameEl = document.getElementById('username');
  const username = usernameEl.value;

  accountEl.value = username;
  accountEl.dispatchEvent(new Event('change'));

  usernameEl.value = account;
  usernameEl.dispatchEvent(new Event('change'));
#+end_src


* Process all bookmarklets

#+begin_src elisp :results raw
  ;; TODO Maybe use org-babel-map-src-blocks instead
  ;; TODO Maybe somehow marked some bookmarklet as "WIP" or "TODO"
  (defun bookmarklet-find-all ()
    (save-restriction
      (save-excursion
        ;; Go the the start of the buffer
        (goto-char (point-min))
        (cl-loop
         while (ignore-error 'user-error (org-babel-next-src-block))
         for (language body arguments switches name start coderef) = (org-babel-get-src-block-info t)
         when (and (string= language "js")
                   (assoc :title arguments))
         collect start))))


  (defun bookmarklet-generate (bookmarklet-position)
    (save-restriction
      (save-excursion
        (goto-char bookmarklet-position)
        (let ((info (org-babel-get-src-block-info)))
          (cl-destructuring-bind (language body arguments switches name start coderef) info
            (let* ((title (cdr (assoc :title arguments)))
                   (src (org-babel--expand-body info))
                   (wrapped-src (format "(function(){ %s })();" src))
                   (url (format "javascript:%s" (url-hexify-string wrapped-src))))
              (format "- %s\n  - [[elisp:(goto-char %s)][Goto source block]]\n  - [[elisp:(progn (kill-new \"%s\") nil)][Copy bookmarklet]]"
                      title start url)
              ;; (list title url)
              ))))))

  ;; Interactive variant
  (defun fsta/bookmarklet-generate (bookmarklet-position arg)
    (interactive "d\nP")
    (save-restriction
      (save-excursion
        (goto-char bookmarklet-position)
        (let ((info (org-babel-get-src-block-info)))
          (cl-destructuring-bind (language body arguments switches name start coderef) info
            (let* ((title (cdr (assoc :title arguments)))
                   (src (org-babel--expand-body info))
                   (wrapped-src (format "(function(){ %s })();" src))

                   (url (format "javascript:%s" (url-hexify-string wrapped-src))))
              (message "%s" src)
              (kill-new url)
              ;; (format "- %s\n  - [[elisp:(goto-char %s)][Goto source block]]\n  - [[elisp:(progn (kill-new \"%s\") nil)][Copy bookmarklet]]" title start url)
              ))))))


  ;; thanks https://emacs.stackexchange.com/a/16793/28469
  (defun fsta/current-line-empty-p ()
    (save-excursion
      (beginning-of-line)
      (looking-at-p "[[:blank:]]*$")))

  (defun bookmarklet-goto-results ()
    (let* ((next-sibling (save-excursion
                           (ignore-error 'user-error  (org-goto-sibling))
                           (point)))
           (next-thing (or next-sibling
                           (ignore-error (save-excursion
                                           (forward-paragraph)
                                           (point))))))
      (when (and next-sibling
                 )
        (message "%s" (- next-sibling (point))))
      (goto-char next-thing)))

  (string-join
   (mapcar #'bookmarklet-generate (bookmarklet-find-all))
   "\n")


  ;; TODO Save those into FF's database instead
#+end_src

#+RESULTS:
- GitLab jobs - Jump to step
  - [[elisp:(goto-char 1791)][Goto source block]]
  - [[elisp:(progn (kill-new "javascript:%28function%28%29%7B%20const%20complement%20%3D%20f%20%3D%3E%20x%20%3D%3E%20%21f%28x%29%3B%0A%0A%2F%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2F%0A%2F%2A%20DOM%20utilities%20%2A%2F%0A%0Aconst%20isElement%20%3D%20x%20%3D%3E%20x.tagName%20%7C%7C%20x.nodeName%3B%0A%0Afunction%20t%28tagName%29%20%7B%0A%20%20%20%20const%20args%20%3D%20Array.prototype.slice.call%28arguments%2C%201%29.flat%28%29%3B%0A%20%20%20%20const%20children%20%3D%20args.filter%28isElement%29%3B%0A%20%20%20%20const%20options%20%3D%20Object.assign%28%7B%7D%2C%20...args.filter%28complement%28isElement%29%29%29%3B%0A%20%20%20%20const%20element%20%3D%20document.createElement%28tagName%29%3B%0A%20%20%20%20for%28let%20key%20of%20Object.keys%28options%29%29%20%7B%0A%20%20%20%20%20%20%20%20element%5Bkey%5D%20%3D%20options%5Bkey%5D%3B%0A%20%20%20%20%7D%0A%20%20%20%20element.replaceChildren%28...children%29%3B%0A%20%20%20%20return%20element%3B%0A%7D%0A%0Afunction%20text%28string%29%20%7B%0A%20%20%20%20return%20document.createTextNode%28string%29%3B%0A%7D%0A%0A%2F%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2F%0A%0Aconst%20blink%20%3D%20%28el%29%20%3D%3E%20%7B%0A%20%20%20%20if%28el.blink_animation%29%20%7B%0A%20%20%20%20%20%20%20%20clearInterval%28el.blink_animation%29%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20let%20step%20%3D%200%3B%0A%20%20%20%20const%20maxTime%20%3D%205000%3B%20%2F%2F%20in%20ms%0A%20%20%20%20const%20interval%20%3D%205%3B%20%2F%2F%20in%20ms%0A%20%20%20%20const%20maxStep%20%3D%20maxTime%20%2F%20interval%3B%0A%20%20%20%20const%20id%20%3D%20setInterval%28%28%29%20%3D%3E%20%7B%0A%20%20%20%20%20%20%20%20if%28step%20%3E%20maxStep%29%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20clearInterval%28id%29%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20el.style.backgroundColor%20%3D%20%22%22%3B%0A%20%20%20%20%20%20%20%20%7D%20else%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20const%20percent%20%3D%20step%2FmaxStep%2A100%3B%20%2F%2F%20Ascending%0A%20%20%20%20%20%20%20%20%20%20%20%20const%20inversePercent%20%3D%20100%20-%20percent%3B%20%2F%2F%20Descending%0A%20%20%20%20%20%20%20%20%20%20%20%20const%20color%20%3D%20%60hsl%2850%2C%20100%25%2C%2065%25%2C%20%24%7BinversePercent%7D%25%29%60%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20el.style.backgroundColor%20%3D%20color%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20step%2B%2B%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%2C%20interval%29%3B%0A%0A%20%20%20%20el.blink_animation%20%3D%20id%3B%0A%7D%3B%0Aconst%20steps%20%3D%20Array.from%28document.querySelectorAll%28%27div.js-line.log-line%20%3E%20span.term-fg-l-green%27%29%29%0A%20%20%20%20%20%20.filter%28el%20%3D%3E%20el.textContent.startsWith%28%27%24%27%29%29%3B%0A%0Aconst%20showModal%20%3D%20%28content%29%20%3D%3E%20%7B%0A%20%20%20%20const%20modal%20%3D%20t%28%27div%27%2C%20content%29%3B%0A%0A%20%20%20%20const%20closeModal%20%3D%20%28event%29%20%3D%3E%20%7B%0A%20%20%20%20%20%20%20%20modal.remove%28%29%3B%0A%20%20%20%20%7D%3B%0A%0A%20%20%20%20const%20closeButton%20%3D%20t%28%27button%27%2C%20text%28%27X%27%29%29%3B%0A%20%20%20%20closeButton.onclick%20%3D%20closeModal%3B%0A%0A%20%20%20%20const%20controls%20%3D%20t%28%27p%27%2C%20closeButton%29%3B%0A%20%20%20%20modal.appendChild%28controls%29%3B%0A%0A%20%20%20%20const%20style%20%3D%20%60%0A%20%20%20%20%20%20background%3A%20black%3B%0A%20%20%20%20%20%20color%3A%20white%3B%0A%20%20%20%20%20%20padding%3A%201em%3B%0A%20%20%20%20%20%20position%3A%20fixed%3B%0A%20%20%20%20%20%20%2F%2Afloat%3A%20left%3B%2A%2F%0A%20%20%20%20%20%20left%3A%2050%25%3B%0A%20%20%20%20%20%20top%3A%2050%25%3B%0A%20%20%20%20%20%20transform%3A%20translate%28-50%25%2C%20-50%25%29%3B%60%3B%0A%20%20%20%20modal.style.cssText%20%3D%20style%3B%0A%20%20%20%20document.body.appendChild%28modal%29%3B%0A%7D%0A%0Aconst%20stepsList%20%3D%20t%28%27ol%27%2C%20steps.map%28el%20%3D%3E%20%7B%0A%20%20%20%20const%20textEl%20%3D%20text%28el.textContent%29%3B%0A%20%20%20%20const%20link%20%3D%20t%28%27a%27%2C%20textEl%29%3B%0A%20%20%20%20link.onclick%20%3D%20%28%29%20%3D%3E%20%7B%0A%20%20%20%20%20%20%20%20el.scrollIntoView%28%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20behavior%3A%20%27smooth%27%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%2F%2F%20vertical%20alignment%0A%20%20%20%20%20%20%20%20%20%20%20%20block%3A%20%27center%27%2C%0A%20%20%20%20%20%20%20%20%20%20%20%20%2F%2F%20horizontal%20alignment%0A%20%20%20%20%20%20%20%20%20%20%20%20%2F%2F%20inline%3A%20%27center%27%0A%20%20%20%20%20%20%20%20%7D%29%3B%0A%20%20%20%20%20%20%20%20blink%28el%29%3B%0A%20%20%20%20%7D%3B%0A%20%20%20%20return%20t%28%27li%27%2C%20link%29%0A%7D%29%29%3B%0A%0AshowModal%28stepsList%29%3B%20%7D%29%28%29%3B") nil)][Copy bookmarklet]]
- GitLab - Todos cleanup
  - [[elisp:(goto-char 3185)][Goto source block]]
  - [[elisp:(progn (kill-new "javascript:%28function%28%29%7B%20const%20todos%20%3D%20Array.from%28document.getElementsByClassName%28%27todo%27%29%29%3B%0A%0Atodos.filter%28todo%20%3D%3E%0A%20%20Array.from%28todo.getElementsByTagName%28%27span%27%29%29.filter%28el%20%3D%3E%0A%20%20el.textContent%20%3D%3D%20%22Merged%22%29.length%20%3E%200%29%3B%20%7D%29%28%29%3B") nil)][Copy bookmarklet]]
- GitLab - Show dates in GitLab Activities
  - [[elisp:(goto-char 3497)][Goto source block]]
  - [[elisp:(progn (kill-new "javascript:%28function%28%29%7B%20const%20complement%20%3D%20f%20%3D%3E%20x%20%3D%3E%20%21f%28x%29%3B%0A%0A%2F%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2F%0A%2F%2A%20DOM%20utilities%20%2A%2F%0A%0Aconst%20isElement%20%3D%20x%20%3D%3E%20x.tagName%20%7C%7C%20x.nodeName%3B%0A%0Afunction%20t%28tagName%29%20%7B%0A%20%20%20%20const%20args%20%3D%20Array.prototype.slice.call%28arguments%2C%201%29.flat%28%29%3B%0A%20%20%20%20const%20children%20%3D%20args.filter%28isElement%29%3B%0A%20%20%20%20const%20options%20%3D%20Object.assign%28%7B%7D%2C%20...args.filter%28complement%28isElement%29%29%29%3B%0A%20%20%20%20const%20element%20%3D%20document.createElement%28tagName%29%3B%0A%20%20%20%20for%28let%20key%20of%20Object.keys%28options%29%29%20%7B%0A%20%20%20%20%20%20%20%20element%5Bkey%5D%20%3D%20options%5Bkey%5D%3B%0A%20%20%20%20%7D%0A%20%20%20%20element.replaceChildren%28...children%29%3B%0A%20%20%20%20return%20element%3B%0A%7D%0A%0Afunction%20text%28string%29%20%7B%0A%20%20%20%20return%20document.createTextNode%28string%29%3B%0A%7D%0A%0A%2F%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2A%2F%0A%0Aconst%20blink%20%3D%20%28el%29%20%3D%3E%20%7B%0A%20%20%20%20if%28el.blink_animation%29%20%7B%0A%20%20%20%20%20%20%20%20clearInterval%28el.blink_animation%29%3B%0A%20%20%20%20%7D%0A%0A%20%20%20%20let%20step%20%3D%200%3B%0A%20%20%20%20const%20maxTime%20%3D%205000%3B%20%2F%2F%20in%20ms%0A%20%20%20%20const%20interval%20%3D%205%3B%20%2F%2F%20in%20ms%0A%20%20%20%20const%20maxStep%20%3D%20maxTime%20%2F%20interval%3B%0A%20%20%20%20const%20id%20%3D%20setInterval%28%28%29%20%3D%3E%20%7B%0A%20%20%20%20%20%20%20%20if%28step%20%3E%20maxStep%29%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20clearInterval%28id%29%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20el.style.backgroundColor%20%3D%20%22%22%3B%0A%20%20%20%20%20%20%20%20%7D%20else%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20const%20percent%20%3D%20step%2FmaxStep%2A100%3B%20%2F%2F%20Ascending%0A%20%20%20%20%20%20%20%20%20%20%20%20const%20inversePercent%20%3D%20100%20-%20percent%3B%20%2F%2F%20Descending%0A%20%20%20%20%20%20%20%20%20%20%20%20const%20color%20%3D%20%60hsl%2850%2C%20100%25%2C%2065%25%2C%20%24%7BinversePercent%7D%25%29%60%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20el.style.backgroundColor%20%3D%20color%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20step%2B%2B%3B%0A%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%7D%2C%20interval%29%3B%0A%0A%20%20%20%20el.blink_animation%20%3D%20id%3B%0A%7D%3B%0A%0Avar%20timeagos%20%3D%20%5B...document.getElementsByClassName%28%27js-timeago%27%29%5D%3B%0A%0Atimeagos.foreach%28ta%20%3D%3E%0A%20%20%20%20ta.appendChild%28t%28%27p%27%2C%20text%28ta.title%29%29%29%29%3B%20%7D%29%28%29%3B") nil)][Copy bookmarklet]]
- AWS - Swap Account Id and IAM user name
  - [[elisp:(goto-char 8104)][Goto source block]]
  - [[elisp:(progn (kill-new "javascript:%28function%28%29%7B%20const%20accountEl%20%3D%20document.getElementById%28%27account%27%29%3B%0Aconst%20account%20%3D%20accountEl.value%3B%0A%0Aconst%20usernameEl%20%3D%20document.getElementById%28%27username%27%29%3B%0Aconst%20username%20%3D%20usernameEl.value%3B%0A%0AaccountEl.value%20%3D%20username%3B%0AaccountEl.dispatchEvent%28new%20Event%28%27change%27%29%29%3B%0A%0AusernameEl.value%20%3D%20account%3B%0AusernameEl.dispatchEvent%28new%20Event%28%27change%27%29%29%3B%20%7D%29%28%29%3B") nil)][Copy bookmarklet]]


** TODO Show results in another window

#+begin_src elisp :eval never
  (let ((result-buffer "*bookmarklet*"))
   (when (get-buffer result-buffer)
     (kill-buffer result-buffer))
   (save-excursion
     (set-buffer (get-buffer-create result-buffer))
     (insert wrapped-src)
     (display-buffer-same-window
      result-buffer
      '((mode . (js-mode))
        (reusable-frames . nil)))))
#+end_src
