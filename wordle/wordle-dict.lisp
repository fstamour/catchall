;;;; I'm a sucker for compression algorithms, let's try compressing
;;;; wordle's set of answers, (because it's all the rage rn)
;;;;
;;;; Took the list from
;;;; https://github.com/tabatkins/wordle-list/blob/39ee14e80dc1ef9df55e682e01979a75ed1ee171/words
;;;;
;;;; I made a lot of mistakes, don't look at this!
;;;;

(ql:quickload '(#:alexandria
                #:flexi-streams
                #:nibbles))

(defpackage #:catchall.wordle-dict
  (:documentation "Compressing the wordle anwsers")
  (:use #:cl))

(in-package #:catchall.wordle-dict)

(defvar *root* (uiop:pathname-directory-pathname *load-pathname*))

(defvar *words-path* (merge-pathnames "words.txt" *root*))

(defparameter *original-size*
  (with-open-file (input *words-path*)
    (file-position input :end)
    (file-position input)))
;; *original-size*
;; 77 681 characters/bytes

(format t "~&The size of word list: ~D." *original-size*)

(defparameter *words*
  (with-open-file (input *words-path*)
    (loop :for word = (read-line input nil nil)
          :while word
          :collect word)))

;; (length *words*)
;; 12947

(unless (zerop (- *original-size*
                  (1-               ; the last line doesn't have a newline
                   (* 6             ; 5 alpha + 1 newline
                      (length *words*)))))
  (error "The number of words doesn't match the size of the file."))


(format t "~&The size of word list without newlines: ~D."
        (* 5 (length *words*)))
;; => 64 735

;; Sort the list of words
(setf *words* (sort *words*  #'string<))


;;; theorically, how many bits do we need to encode N characters when
;;; the alphabet is only 16 characters?

(loop :for n :from 1 :upto 5
      :collect (log (expt 26 n) 2))
;; (4.70044 9.40088 14.101319 18.80176 23.5022)

;; How many bytes would this round up to?
(loop :for n :from 1 :upto 12
      :for bits = (log (expt 26 n) 2)
      :for closest-power-of-2 = (ceiling (log bits 2))
      :for number-of-bytes = (expt 2 (- closest-power-of-2 3))
      :do
         (format
          t "~&In order to store ~D chars of [a-z] as one integer,~%  it ~
          would theorically take ~A bits,~%  which would round up to ~D ~
          bytes."
          n bits number-of-bytes))



;;; Curious to see if there's any saving by removing the first letter

(defparameter *first-chars*
  (sort
   (alexandria:hash-table-alist
    (loop
      :with first-chars = (make-hash-table)
      :for word :in *words*
      :for first-char = (char word 0)
      :do
         (incf (gethash first-char first-chars 0))
      :finally (return first-chars)))
   #'char<
   :key #'car))
;; *first-chars*
'((#\a . 736)
  (#\b . 908)
  (#\c . 920)
  (#\d . 681)
  (#\e . 303)
  (#\f . 595)
  (#\g . 637)
  (#\h . 488)
  (#\i . 165)
  (#\j . 202)
  (#\k . 375)
  (#\l . 575)
  (#\m . 693)
  (#\n . 325)
  (#\o . 262)
  (#\p . 857)
  (#\q . 78)
  (#\r . 628)
  (#\s . 1560)
  (#\t . 815)
  (#\u . 189)
  (#\v . 242)
  (#\w . 411)
  (#\x . 16)
  (#\y . 181)
  (#\z . 105))


;;; So, it would require 2 bytes per letter of the alphabet

(+
 (* 26 2)
 (* 4
    (length *words*)))
;; 51 840 bytes

(defun encode-first-char-stats ()
  (flexi-streams:with-output-to-sequence (output)
    (loop :for (char . count) :in *first-chars*
          :collect (nibbles:write-ub16/be count output))))


;;; If we look only at the 4 remaining chars, how many duplicates are
;;; there?

(hash-table-count
 (loop
   :with suffixes = (make-hash-table :test 'equal)
   :for word :in *words*
   :for suffix = (subseq word 1)
   :do
      (incf (gethash suffix suffixes 0))
   :finally (return suffixes)))
;; => 12947

(- (length *words*) 12947)
;; => 0
;; None!


;;; What does the data looks like if we XORs successive words?

(defun xor-octets (octets1 octets2)
  (map 'vector #'(lambda (byte1 byte2)
                   (logxor byte1 byte2))
       octets1 octets2))

(defun normalize (string)
  (map 'vector
       (alexandria:rcurry #'- (char-code #\a))
       (flexi-streams:string-to-octets string)))

(defun denormalize (octets)
  (flexi-streams:octets-to-string
   (map 'vector
        (alexandria:rcurry #'+ (char-code #\a))
        octets)))

#+nil
(denormalize
 (normalize "asd"))

(defun xor-strings (string1 string2)
  "Apply XOR on each characters, returns a vector of bytes."
  (xor-octets
   (normalize string1)
   (normalize string2)))

(defparameter *xored-words*
  (loop
    :for w1 :in *words*
    :for w2 :in (rest *words*)
    :collect (xor-strings w1 w2)))

;;; If we keep only the last 4 chars, and xor them, there will be a
;;; common prefix consisting of 0 to 3 0s. The length of the prefix
;;; can be encoded in 2 bits. And packed 4x2bits = 1byte
;;;
;;; No need to think about the padding too much, we're done when we
;;; reach the end of the stream.



(defun take (list n)
  (loop :for el :in list
        :for i :below n
        :collect el))

;; Group the words 4 by 4
(defparameter *xored-words-grouped*
  (loop :for rest :on *xored-words* :by #'(lambda (list)
                                            (nthcdr 4 list))
        :for words = (take rest 4)
        :collect words))

(unless (= (ceiling (1- (length *xored-words*)) 4)
           (length *xored-words-grouped*))
  (error "Didn't get the exepected amount of groups"))

(length
 (alexandria:lastcar
  *xored-words-grouped*))
;; => 2



(defun count-zero-prefix (vector &key (start 0))
  (let ((end (position-if-not #'zerop vector :start start)))
    (cons (count-if #'zerop vector
                    :start start
                    :end end)
          (subseq vector end))))
;; (count-zero-prefix #(0 0 1 0))
;; (count-zero-prefix #(0 0 1 0) :start 1)

(defun %encode-group (group &aux (prefix-byte 0))
  (values
   (loop :for word :in group
         :for i :from 3 :downto 0
         :for (prefix . content) = (count-zero-prefix word :start 1)
         :do (setf prefix-byte (logior prefix-byte (ash prefix (* 2 i))))
         :collect content)
   prefix-byte))

(defun encode-group (group)
  (multiple-value-bind (content prefix-byte)
      (%encode-group group)
    (apply #'concatenate 'vector
           `#(,prefix-byte)
           content)))

#+ (or)
(let ((group '(#(0 0 0 0 1)
               #(0 0 0 1 0)
               #(0 0 1 0 0)
               #(0 1 0 0 0))))
  (encode-group group))


(defparameter *encoded*
  (apply #'concatenate 'vector
         (encode-first-char-stats)
         (normalize (subseq (first *words*) 1))
         (mapcar #'encode-group *xored-words-grouped*)))

(length *encoded*)
;; 27994

(float (* 100 (/ (length *encoded*) *original-size*)))
;; 36% of the original size, just with bit fiddling


;;;; Encoding words as integers

(defun b26-string->int (string)
  (loop :for char :across string
        :for power :from (1- (length string)) :downto 0
        :for value = (*
                      (- (char-code char)
                         #. (char-code #\a))
                      (expt 26 power))
        :sum value))

;; I have a feeling this could've been much simpler...
(defun int->b26-string (x &optional (colsize))
  (let ((colsize  (max
                   (if (zerop x)
                       1
                       (ceiling (log x 26)))
                   (or colsize 0)))
        (i 0)
        (remainder x))
    (with-output-to-string (output)
      (loop
        :for power :from (1- colsize) :downto 0
        :for guard :below 100
        :until (zerop remainder)
        :do
           (let* ((code (floor remainder (expt 26 power)))
                  (value (* code (expt 26 power)))
                  (char (code-char (+ code
                                      #. (char-code #\a)))))
             (setf remainder (- remainder value))
             (incf i)
             (write-char char output)))
      (loop :for j :from i :below colsize
            :do (write-char #\a output)))))

(int->b26-string 0 4)
"aaaa"

(int->b26-string 25 4)
"aaaz"

(int->b26-string 26 4)
"aaba"

(b26-string->int "allo")
7736
(int->b26-string 7736 4)
"allo"

(let ((n 4))
  (loop :for i :below (expt 26 n)
        :for string = (int->b26-string i n)
        :for x = (b26-string->int string)
        :unless (= i x)
          :do (error "~d != ~d (~s)" i x string)))


(defun int->octets-stream (x base stream &optional (colsize))
  (let ((colsize  (max
                   (if (zerop x)
                       1
                       (ceiling (log x base)))
                   (or colsize 0)))
        (i 0)
        (remainder x))
    (loop
      :for power :from (1- colsize) :downto 0
      :for guard :below 100
      :until (zerop remainder)
      :do
         (let* ((code (floor remainder (expt base power)))
                (value (* code (expt base power))))
           (setf remainder (- remainder value))
           (incf i)
           (write-byte code stream)))
    (loop :for j :from i :below colsize
          :do (write-byte 0 stream))))

(defun int->octets (x base &optional (colsize))
  (flexi-streams:with-output-to-sequence (output)
    (int->octets-stream x base output colsize)))

;;; TODO octets->int


;;; Encoding each word as 3 bytes.

(log (expt 26 5) 2)
;; 24 bits per word (5 chars)

(log (expt 26 4) 2)
;; 19 bits per 4 chars

(length
 (flexi-streams:with-output-to-sequence (output)
   (loop :for word :in *words*
         ;; :for i :below 2
         :do (int->octets-stream
              (b26-string->int word)
              256 output 3))))
;; => 38841 bytes

(float (* 100 (/ 38841 *original-size*)))
;; 50% of the original size, just with bit fiddling


;;; Each groups currently have 4 * 4 characters.

(log (expt 26 (* 4 4)) 2)
;; 76 bits => 10 bytes

;;; How big would it be if we encoded with the prefix char stuff?

(+
 (* 2 26) ;; first-char frequency
 (ceiling (length *words*) 8) ;; One byte per
 )

;; .. oh wait, it's more complex than that...


(defun encode-group* (group )
  (multiple-value-bind (content prefix-byte)
      (%encode-group group)
    (flexi-streams:with-output-to-sequence (output)
      (write-byte prefix-byte output)
      (int->octets-stream
       (b26-string->int
        (denormalize
         (apply #'concatenate 'vector content)))
       256
       output))))


(let ((group '(#(0 0 0 0 1)
               #(0 0 0 1 0)
               #(0 0 1 0 0)
               #(0 1 0 0 0))))
  (encode-group* group))

(defparameter *encoded2*
  (apply #'concatenate 'vector
         (encode-first-char-stats)
         (normalize (subseq (first *words*) 1))
         (mapcar #'encode-group* *xored-words-grouped*)))

(length *encoded2*)
;; 18657

(float (* 100 (/ (length *encoded2*) *original-size*)))
;; 24.017456, just with bit fiddling, but now it's getting complex to
;; decode


;;; What if if encode the "prefixes" and the "contents" separately?

(defparameter *prefixes-and-contents*
  (let ((prefixes)
        (contents))
    (loop :for group :in *xored-words-grouped*
          :do
             (multiple-value-bind (content prefix-byte)
                 (%encode-group group)
               (push prefix-byte prefixes)
               (push (int->octets
                      (b26-string->int
                       (denormalize
                        (apply #'concatenate 'vector content)))
                      256)
                     contents)))
    (cons
     (nreverse prefixes)
     (nreverse contents))))

;; P.S. It doesn't make sense anymore to call them "prefix-byte"

;;;;;;;;;;;;; TODO Decoding!


;;;; Other technique: just diffing


(defparameter *diff*
  (let ((numbers
          (mapcar #'(lambda (word)
                      (b26-string->int (subseq word 1)))
                  *words*)))
    (loop :for n1 :in numbers
          :for n2 :in (rest numbers)
          :collect (- n2 n1))))

(reduce #'max *diff*)
