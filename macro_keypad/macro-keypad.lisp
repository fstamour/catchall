(in-package #:common-lisp-user)

(defpackage #:serial
  (:use :cl))

(in-package #:serial)


(ql:quickload '(cserial-port
                win32
                bordeaux-threads))

;; Fake keypresses using win32's API
(defun send-input (inputs)
  (let ((num-inputs (length inputs)))
    ;; Allocate an array of input structure
    (cffi:with-foreign-object (winputs 'win32:input num-inputs)
      ;; Zero-out the array of inputs
      (win32:zero-memory winputs (* num-inputs (cffi:foreign-type-size 'win32:input)))
      ;; For each input
      (map nil
           (let ((i 0))
             (lambda (input)
               (let* (;; Make a pointer the the corresponding element in the array
                      (winput (cffi:inc-pointer winputs (* (cffi:foreign-type-size 'win32:input) i)))
                      ;; Make a pointer to the input's field (which is a union)
                      (wunion (cffi:foreign-slot-pointer winput 'win32:input 'win32:input)))
                 (cffi:with-foreign-slots ((win32:type) winput win32:input)
                   (ecase (car input)
                     (:mouse (error "unimplemented"))
                     (:hardware (error "unimplemented"))
                     (:keyboard
                      ;; Set the input's "type" field
                      (setf win32:type win32:+input-keyboard+)
                      (destructuring-bind (vkey . flags) (cdr input)
                        (cffi:with-foreign-slots ((win32:vk win32:flags) wunion win32:keybdinput)
                          ;; Set the virtual key of the keybdinput structure
                          (setf win32:vk vkey)
                          ;; Merging (or'ing) all the flags, an setting the right field on the keybdinput structure
                          (dolist (flag flags)
                            (setf win32:flags (logior win32:flags flag)))))))))
               (incf i)))
           inputs)
      ;; Actually send the inputs
      (win32:send-input num-inputs winputs (cffi:foreign-type-size 'win32:input)))))

;; More keycodes at https://docs.microsoft.com/en-us/windows/win32/inputdev/virtual-key-codes

#+nil
;; see https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-sendinput
(send-input (list (list :keyboard win32:+vk-lwin+)
                  (list :keyboard #x44) ; d key
                  (list :keyboard #x44 win32:+keyeventf-keyup+)
                  (list :keyboard win32:+vk-lwin+ win32:+keyeventf-keyup+)))

(defun keypress-and-release (vk)
  (send-input (list (list :keyboard vk)
                    (list :keyboard vk win32:+keyeventf-keyup+))))

(defun volume-up ()
  (keypress-and-release  win32:+vk-volume-up+))

(defun volume-down ()
  (keypress-and-release win32:+vk-volume-down+))

(defun play/pause ()
  (keypress-and-release win32:+vk-media-play-pause+))

(defun next-track ()
  (keypress-and-release win32:+vk-media-next-track+))

(defun prev-track ()
  (keypress-and-release win32:+vk-media-prev-track+))


;; Open
(defparameter *s*
  (handler-case
      (cserial-port:open-serial "COM3"
                                :baud-rate 9600
                                :encoding :utf8)
    (error (condition)
      (format *debug-io* "~&Failed to open serial port. ~a"
              condition))))

;; Close
#+nil
(when *s*
  (ignore-errors
   (cserial-port:close-serial *s*))
  (setf *s* nil))

;; Thread managements
(defun list-all-other-worker-thread ()
  (let ((current-thread (bt:current-thread)))
    (find-if #'(lambda (thread)
                 (and (not (eq current-thread thread))
                      (string= "worker" (bt:thread-name thread))))
             (sb-thread:list-all-threads))))

(defun kill-all-other-worker-thread ()
  (alexandria:if-let ((threads (list-all-other-worker-thread)))
                     (bordeaux-threads:destroy-thread threads)
                     threads))

;; (list-all-other-worker-thread)
;; (kill-all-other-worker-thread)


;; Reading
(defun read-serial-byte (serial &optional (timeout-ms 100))
  (when serial
    (handler-case
        (cserial-port:read-serial-byte *s* :timeout-ms timeout-ms)
      (cserial-port:timeout-error (condition)
        (declare (ignore condition))
        nil))))

#+nil
(read-serial-byte *s* nil)

(defun decode-message (byte)
  (list
   ;; Which button
   (ldb (byte 7 1) byte)
   ;; Was it a key down?
   (not (zerop (ldb (byte 1 0) byte)))))

#+nil
(loop :for i :below 8 :collect (decode-message i))

(defun handle-key (button keydownp)
  (when (not keydownp)
    (case button
      (0 (play/pause))
      (1 (next-track))
      (2 (prev-track))
      (4 (volume-up))
      (5 (volume-down)))))

(defparameter *stop* nil)
#+nil
(setf *stop* nil)
#+nil
(setf *stop* t)


(bt:make-thread
 (lambda ()
   (declare (special *s* *stop*))
   (if *s*
       (progn
         (loop for message = (read-serial-byte *s* nil)
               while (and *s* (not *stop*))
               do
                  (when message
                    (destructuring-bind (button keydownp)
                        (decode-message message)
                      (format t "~&button ~d, pressed ~a"
                              button
                              keydownp)
                      (handle-key button keydownp))))
         )
       (format t "~&Not connected..."))
   (unless *stop*
     (format t "~&Loop was asked to stop.")))
 :name "Reading inputs for serial port."
                                        ;:initial-bindings
 )

(print
 (bt:all-threads))
