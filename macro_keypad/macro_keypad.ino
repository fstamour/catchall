// https://www.arduino.cc/reference/en/
// https://github.com/LennartHennigs/ESPRotary

#include <ESPRotary.h>

/////////////////////////////////////////////////////////////////

#define ROTARY_PIN1  2
#define ROTARY_PIN2 3
#define ROTARY_STEPS_PER_CLICK 4

/////////////////////////////////////////////////////////////////

uint8_t buttons_state = 0;
ESPRotary rotary_encoder = ESPRotary(ROTARY_PIN1, ROTARY_PIN2, ROTARY_STEPS_PER_CLICK);

/////////////////////////////////////////////////////////////////

// TODO use typedefs
void sendKey(uint8_t button, uint8_t state) {
  uint8_t message = button << 1 | state;
  Serial.write(message);
}

// on left or right rotation
void showDirection(ESPRotary& r) {
  uint8_t direction = (r.getDirection() ==  RE_RIGHT) + 4;
  // sendKey(direction, 1); // Keyup
  sendKey(direction, 0); // Keydown
}

/////////////////////////////////////////////////////////////////

void setup() {
  // start serial connection
  Serial.begin(9600);

  // Initialize the buttons' pin
  for (int i = 4; i < 8; ++i) {
    pinMode(i, INPUT);
  }

  rotary_encoder.setLeftRotationHandler(showDirection);  
  rotary_encoder.setRightRotationHandler(showDirection);
}

void loop() {
  rotary_encoder.loop();
  for (uint8_t i = 0; i < 4; ++i) {
    // Read the ith button
    uint8_t pin = i + 4;
    int button_state = digitalRead(pin);

    // Compare to previous state
    uint8_t button_mask = 1 << i;
    uint8_t previous_state = (buttons_state & button_mask) >> i;
    
    if (button_state != previous_state) {
      uint8_t message = i << 1 | button_state;
      Serial.write(message);
      // Serial.println(message);
      buttons_state ^= button_mask;
    }
  }
}



/////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////
