;;;; was answering somebody on discord
;;;; https://discord.com/channels/297478281278652417/297478350145060875/900444996900098088
;;;; Then, as an exercice, I tried to write it in different ways

(defun parts-sums (ls)
  (let ((sum (reduce #'+ ls)))
    (loop for el in (cons 0 ls)
          for result = sum then (decf sum el)
          collect result)))

(parts-sums '(1 2 3))
;; => (6 5 3 0)

(defun parts-sums (ls)
  (reduce #'(lambda (el current)
              (cons (+
                     (or (car current) 0) el)
                    current))
          ls
          :from-end t
          :initial-value '(0)))

(defun parts-sums (ls)
  (let ((sum (reduce #'+ ls)))
    (mapcar (lambda (x)
              (decf sum x))
            (cons 0 ls))))

(let* ((input '(1 2 3)))
  (print "===")
  (reduce #'(lambda (el current)
              (print (list current el))
              (cons (+ (or (car current) 0) el) current))
          input
          :from-end t
          :initial-value '(0)))
