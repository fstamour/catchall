(defpackage #:catchall.gc
  (:documentation "Writing a toy \"\"\"GC\"\"\" in lisp

- objects are stored in an array
- objects can have any type
- the array can get compacted
- there's a way to \"notify\" when an object is being relocated

The idea came from talking about ECS (Entity-Component System) in the
#lisp-gamedev channel of the lisp discord.

This is just a proof-of-concept, made for fun.
")
  (:use #:cl))

(in-package #:catchall.gc)

(defun make-memory (size)
  (make-array (list size)
              :initial-element nil))

(defstruct (gc
            (:constructor make-gc (&key size)))
  (free-bitmap
   (make-array (list size)
               :element-type 'bit
               :initial-element 1)
   :type bit-vector)
  (fill-pointer 0
   :type (integer 0))
  (memory (make-memory size)
   :type vector))

(defun gc-size (gc)
  (length (gc-memory gc)))

(defun address-free-p (gc address)
  (not (zerop (aref (gc-free-bitmap gc) address))))

(defun (setf free-bitmap) (value gc address)
  (check-type value bit)
  (setf (aref (gc-free-bitmap gc) address) value))

(defun address (gc address)
  (aref (gc-memory gc) address))

(defun (setf address) (value gc address)
  (setf (aref (gc-memory gc) address) value))

;; TODO Check invariants
;;  - (= fill-pointer (position 1 :from-end t)), unless full
;;  - (= (length (gc-memory gc)) (length (free-bitmap)))



(defun find-free-spot (gc)
  (position 1 (gc-free-bitmap gc)))

(defun store (gc object &optional address)
  "Allocate an object, return its address."
  (let ((address (or address (find-free-spot gc))))
    (if address
        (progn
          (setf (address gc address) object
                (free-bitmap gc address) 0)
          (when (= address (gc-fill-pointer gc))
            (incf (gc-fill-pointer gc)))
          address)
        (error "Out of memory"))))

(defun free (gc address)
  (setf (address gc address) nil
        (free-bitmap gc address) 1)
  (when (= (1+ address) (gc-fill-pointer gc))
    ;; TODO Instead of decf, use position 1 free-bitmap :from-end t
    ;; :end (1- fill-pointer)
    (decf (gc-fill-pointer gc))))



(defgeneric notify-relocation (object from to)
  (:method (object from to)))

(defun relocate (gc object from to)
  (free gc from)
  (store gc object to)
  (notify-relocation object from to))

(defun gc-compact-p (gc)
  (let* ((bitmap (gc-free-bitmap gc))
         (first-empty (position 0 bitmap)))
    (or
     ;; free-bitmap == 1111111111
     (null first-empty)
     ;; free-bitmap == 1111...000 or 0000....0000
     (null (position
            1
            bitmap
            :start first-empty)))))

(defun gc-compact-1 (gc range
                     &aux
                       (range-start (car range))
                       (range-end (cdr range)))
  (let ((to (position 1 (gc-free-bitmap gc)
                      :start range-start))
        (from
          (position 0 (gc-free-bitmap gc)
                    :end range-end
                    :from-end t)))
    (when (< to from)
      (let ((object (address gc from)))
        (relocate gc object from to))
      (setf (gc-fill-pointer gc) from)
      (cons to from))))

(defun gc-compact (gc)
  (loop
    :for range = (gc-compact-1 gc '(0 . nil))
      :then (gc-compact-1 gc range)
    :while range))



(defstruct (entity
            (:constructor make-entity (&key (id 0))))
  (id 0 :type (integer 0)))

(defmethod notify-relocation ((entity entity) from to)
  (format *trace-output* "~&Entity ~s is being relocated from ~d to ~d."
          entity from to)
  (setf (entity-id entity) to))

(let ((gc (make-gc :size 16)))
  (flet ((dump (step)
           (format t "~&=======================~%~
                      ~a:~%~
                      ~a~%compacted: ~a"
                   step
                   gc
                   (gc-compact-p gc))))
    (format t "~&=======================")
    (dump "Initialized")

    ;; Fill the memory
    (loop :for i :below (gc-size gc)
          :for address = (find-free-spot gc)
          :for entity = (make-entity :id address)
          :do (store gc entity address))

    (dump "Filled the memory")

    ;; Pseudo-randomly free half of the memory
    (loop
      :with count = 0
      :for i :from 0
      :when (zerop (rem (* i 2011) 73))
        :do  (progn
               (free gc
                     (rem (* i 2011) (gc-fill-pointer gc)))
               (incf count))
      :until (> count (/ (gc-size gc) 2)))

    (dump "Freed half the memory")

    ;; Compact the memory
    (gc-compact gc)

    (dump "Compacted")))

#|
=======================
=======================
Initialized:
#S(GC
   :FREE-BITMAP #*1111111111111111
   :FILL-POINTER 0
   :MEMORY #(NIL NIL NIL NIL NIL NIL NIL NIL NIL NIL NIL NIL NIL NIL NIL NIL))
compacted: T
=======================
Filled the memory:
#S(GC
   :FREE-BITMAP #*0000000000000000
   :FILL-POINTER 16
   :MEMORY #(#S(ENTITY :ID 0) #S(ENTITY :ID 1) #S(ENTITY :ID 2)
             #S(ENTITY :ID 3) #S(ENTITY :ID 4) #S(ENTITY :ID 5)
             #S(ENTITY :ID 6) #S(ENTITY :ID 7) #S(ENTITY :ID 8)
             #S(ENTITY :ID 9) #S(ENTITY :ID 10) #S(ENTITY :ID 11)
             #S(ENTITY :ID 12) #S(ENTITY :ID 13) #S(ENTITY :ID 14)
             #S(ENTITY :ID 15)))
compacted: T
=======================
Freed half the memory:
#S(GC
   :FREE-BITMAP #*1101001001001011
   :FILL-POINTER 14
   :MEMORY #(NIL NIL #S(ENTITY :ID 2) NIL #S(ENTITY :ID 4) #S(ENTITY :ID 5) NIL
             #S(ENTITY :ID 7) #S(ENTITY :ID 8) NIL #S(ENTITY :ID 10)
             #S(ENTITY :ID 11) NIL #S(ENTITY :ID 13) NIL NIL))
compacted: NIL
Entity #S(ENTITY :ID 13) is being relocated from 13 to 0.
Entity #S(ENTITY :ID 11) is being relocated from 11 to 1.
Entity #S(ENTITY :ID 10) is being relocated from 10 to 3.
Entity #S(ENTITY :ID 8) is being relocated from 8 to 6.
=======================
Compacted:
#S(GC
   :FREE-BITMAP #*0000000011111111
   :FILL-POINTER 8
   :MEMORY #(#S(ENTITY :ID 0) #S(ENTITY :ID 1) #S(ENTITY :ID 2)
             #S(ENTITY :ID 3) #S(ENTITY :ID 4) #S(ENTITY :ID 5)
             #S(ENTITY :ID 6) #S(ENTITY :ID 7) NIL NIL NIL NIL NIL NIL NIL NIL))
compacted: NIL
|#
