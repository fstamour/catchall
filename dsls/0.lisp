
(defpackage #:dsls
  (:documentation "Tired of writing the same code in all languages :P")
  (:use #:cl))

(in-package #:dsls)



`(if (file-exists-p path)
     ...)

bash "if [ -f \"$path\" ]; then ... ; fi"
fish "if [ -f \"$path\" ]; ...; end"
js "\"import fs\" if(await fsPromise.exists()) { ... }"





;;; logic expressions (e.g. like TLA+)

;; Abs(x) == IF x < 0 THEN -x ELSE x

(defmethod tr-symbol ((target (eql 'tla+)) symbol)
  (format nil "~:(~a~)" car))

(defmethod tr (target car &optional cdr)
  (cond
    ((and (listp car) (null cdr))
     (tr target (car car) (cdr car)))
    ((and (symbolp car) (null cdr))
     (tr-symbol target car))
    (t (format nil "(untranslated ~a ~a)" car (tr target cdr)))))

(defmethod tr ((target (eql 'tla+)) (car (eql nil)) &optional cdr)
  nil)

(defmethod tr ((target (eql 'tla+)) (car (eql 'def)) &optional cdr)
  (destructuring-bind (name lambda-list &rest body)
      cdr
    (format nil "~:(~a~)(~{~:(~a~)~^, ~}) == ~a"
            name lambda-list (tr target body))))

(defmethod tr ((target (eql 'tla+)) (car (eql 'if)) &optional cdr)
  (destructuring-bind (test then &optional else)
      cdr
    (format nil "IF ~a THEN ~a ELSE ~a"
            (tr target test)
            (tr target then)
            (tr target else))))

(defmethod tr ((target (eql 'tla+)) (car (eql 'plusp)) &optional cdr)
  (format nil "~a > 0" (tr target cdr)))

(defmethod tr ((target (eql 'tla+)) (car (eql '-)) &optional cdr)
  (format nil "-~a" (tr target cdr)))

(defmethod tr ((target (eql 'tla+)) (car (eql 'not)) &optional cdr)
  (format nil "~~~a" (tr target cdr)))

(defmethod tr ((target (eql 'tla+)) (car (eql '=)) &optional cdr)
  (format nil "~{~a~^ = ~}" (mapcar (lambda (x) (tr target x)) cdr)))

(defmethod tr ((target (eql 'tla+)) (car (eql 'and)) &optional cdr)
  (format nil "/\\~a" (tr target cdr)))



(trace tr)

(tr 'tla+ nil)

(tr 'tla+ '(plusp x))

(tr 'tla+ '(= x y))
(tr 'tla+ '(= x y z))

(tr 'tla+ '(= x y z))

(TR 'TLA+ '(X))

(tr 'tla+ `(def abs (x) (if (plusp x) x (- x))))
"Abs(X) == IF X > 0 THEN X ELSE -X"

(tr 'tla+ `(def xor (a b) (= a (not b))))
"Xor(A, B) == A = ~B"
