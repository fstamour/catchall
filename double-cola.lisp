;;;; This is a "code challenge"
;;;; I "discovered" it because of this: https://discord.com/channels/297478281278652417/503574823281229864/918189462691926067
;;;; which links to this: https://www.youtube.com/watch?v=7fylNa2wZaU
;;;; which is a guy trying to solve the problem on code wars but his
;;;; first solutions didn't work because of plarform issues (IIRC)
;;;;
;;;; All the solutions shown in the video are using loops, not this
;;;; one.

(in-package #:common-lisp-user)

(defpackage #:double-cola
  (:use :cl))

(in-package #:double-cola)

#| There is a queue of people. When somebody takes a cola, they
doubles and the 2 pepole goes back to the queue (e.g. after Sheldon
takes the first cola, there are 2 Sheldons that goes to the back of
the line, so Sheldon would take the 6th and 7th cola). Who's going to
take the nth cola?  |#

(defvar *people* '(sheldon leonard penny rajesh howard))

(length *people*)
;; => 5

;; count the number of people after everybody got a cola i-1 times
(loop :for i :from 1 :upto 10
      :for run-length = (* (length *people*)
                           (expt 2 (1- i)))
      :sum run-length :into total
      :collect (list i run-length total))
((1 5 5)
 (2 10 15)
 (3 20 35)
 (4 40 75)
 (5 80 155)
 (6 160 315)
 (7 320 635)
 (8 640 1275)
 (9 1280 2555)
 (10 2560 5115))

;; I solved the recurrence relation with wolframAlpha...
;; The relation:
;; x(0) = 5, x(n) = 2*x(n-1)
;;
;; I got the "5" and "2*x(n-1)" for looking at the first column of the
;; previous loop's output.
;;
;; The solution:
;; x(n) = 5^(2^i)

;; making sure I get the same thing
(loop :for i :from 1 :upto 10
      :collect (list i (* (length *people*)
                          (- (expt 2 i) 1))))
((1 5)
 (2 15)
 (3 35)
 (4 75)
 (5 155)
 (6 315)
 (7 635)
 (8 1275)
 (9 2555)
 (10 5115))

;; Computing the inverse function
(loop :for (_ n) :in '((1 5)
                       (2 15)
                       (3 35)
                       (4 75)
                       (5 155)
                       (6 315)
                       (7 635)
                       (8 1275)
                       (9 2555)
                       (10 5115))
      :collect (list n
                     (floor (log
                             (1+ (/ n (length *people*)))
                             2))))

(defun total-at-run (i)
  (* (length *people*)
     (- (expt 2 (1+ i)) 1)))

(loop :for i :below 10 :collect (total-at-run i))

(defun which-run (n)
  (floor (log
          (1+ (/ n (length *people*)))
          2)))

(loop :for n :from 0 :upto 100
      :collect
      (which-run n))

(progn
  (defun which-person (n)
    (let ((run (which-run n)))
      (floor (- n (total-at-run (1- run)))
             (expt 2 run))))

  (loop :for n :from 0 :upto 100
        ;; :for run = (which-run n)
        :collect
        (which-person n)
        #+nil (list :n (1+ n)
                    :r run
                    :t (total-at-run run)
                    :p (1+ (which-person n)))))
((:N 1 :R 0 :T 0 :P 1)
 (:N 2 :R 0 :T 0 :P 2) (:N 3 :R 0 :T 0 :P 3)
 (:N 4 :R 0 :T 0 :P 4) (:N 5 :R 0 :T 0 :P 5) (:N 6 :R 1 :T 5 :P 1)
 (:N 7 :R 1 :T 5 :P 2) (:N 8 :R 1 :T 5 :P 1) (:N 9 :R 1 :T 5 :P 2)
 (:N 10 :R 1 :T 5 :P 1) (:N 11 :R 1 :T 5 :P 2) (:N 12 :R 1 :T 5 :P 1)
 (:N 13 :R 1 :T 5 :P 2) (:N 14 :R 1 :T 5 :P 1) (:N 15 :R 1 :T 5 :P 2)
 (:N 16 :R 2 :T 15 :P 1))


(locally
    (declare (optimize (speed 3) (safety 0) (debug 0)))
  (let* ((people '(sheldon leonard penny rajesh howard))
         (number-of-people (length people)))
    (labels ((total-at-run (r)
               (* number-of-people
                  (- (expt 2 (1+ r)) 1)))
             (which-run (n)
               (floor (log
                       (1+ (/ n (length people)))
                       2)))
             (which-person (n)
               (let ((run (which-run n)))
                 (floor (- n (total-at-run (1- run)))
                        (expt 2 run)))))
      (list (loop :for n :from 0 :upto 100
                  :collect
                  (nth
                   (which-person n)
                   people))
            (nth
             (time
              (which-person 7230702951))
             people)))))
;; => ~4300 processor cycles

;;; The two implementations below are 4x slower if you remove the ".0"

(locally
    (declare (optimize (speed 3) (safety 0) (debug 0)))
  (time
   (loop for n = (1- 7230702951.0) :then (/ (- n 5) 2)
         repeat 1000
         while (> n 5)
         :finally (return n))))
;; => ~6000 processor cycles

(locally
    (declare (optimize (speed 3) (safety 0) (debug 0)))
  (time
   (do ((n (1- 7230702951.0)))
       ((< n 5) n)
     (setf n (/ (- n 5) 2)))))
;; => ~5500 processor cycles


(defun truncate-to-expt2 (x)
  (do ((r 1))
      ((< x (* 2 r)) r)
    (setf r (* 2 r))))

(defun truncate-to-expt2* (x)
  (expt 2 (floor (log x 2))))

(time
 (truncate-to-expt2 5))
(time
 (truncate-to-expt2* 5))

(locally
    (declare (optimize (speed 3) (safety 0) (debug 0)))
  (let* ((people '(sheldon leonard penny rajesh howard))
         (number-of-people (length people)))
    (labels ((truncate-to-expt2 (x)
               (do ((r 1))
                   ((< x (* 2 r)) r)
                 (setf r (* 2 r))))
             (which-person (n)
               (let ((run-sqr (truncate-to-expt2 (1+ (/ n (length people))))))
                 (floor (- n (* number-of-people
                                (1- run-sqr)))
                        run-sqr))))
      (list (loop :for n :from 0 :upto 100
                  :collect
                  (nth
                   (which-person n)
                   people))
            (nth
             (time
              (which-person 7230702951))
             people)))))
;; => ~5000 processor cycles
