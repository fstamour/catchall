#|
exec sbcl --noinform \
--non-interactive \
--eval "(ql:quickload '(#:win32 #:com.inuoe.winutil))" \
--load "$0"
|#

#|

Useful links:

- https://github.com/Zulu-Inuoe/win32
- https://github.com/Zulu-Inuoe/winutil
- https://docs.microsoft.com/en-us/windows/win32/api/winuser/

|#

(cl:in-package #:cl-user)

(defpackage #:catchall.win32
  (:documentation "")
  (:local-nicknames (#:winutil #:com.inuoe.winutil))
  (:use #:cl))

(in-package #:catchall.win32)

;;; Experimenting with win32 API
;;; - [x] lisitng windows
;;; - [x] switching windows
;;; - [ ] test if webcam is in use
;;; - [x] listen to all keypresses
;;; - [ ] be notified when the focus between windows changes
;;; - [ ] find the right COM port (e.g. plug in an arduino, find
;;;       which usb device it is and find the associated COM port)


;;; Listing windows

;; https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-enumwindows
;; (functionp #'win32::enum-windows)

(defvar *window-list* ())


;; Creating a callback used to append window handlers to the dynamic
;; variable *window-list*
(cffi:defcallback enum-windows-cb :int ((hwnd win32:hwnd)
                                        (lparam win32:lparam))
  (declare (ignore lparam))
  (push hwnd *window-list*)
  ;; (format t "~&enum-windows-cb: ~a" hwnd)
  ;; Return 0 (false) to stop, or 1 (true) to continue
  1)

;; Just making sure that is-window-visible returns a boolean, not 0s
;; and 1s
#+ (or)
(let ((*window-list* ()))
  (win32:enum-windows (cffi:callback enum-windows-cb)  0)
  (mapcar #'win32:is-window-visible *window-list*))

(defun coerce-null-pointer-to-nil (pointer)
  (unless (zerop (cffi:pointer-address pointer))
    pointer))

;; Testing how to use win32:get-parent
#+ (or)
(let ((*window-list* ()))
  (win32:enum-windows (cffi:callback enum-windows-cb)  0)
  (mapcar #'(lambda (hwnd)
              (coerce-null-pointer-to-nil
               (win32:get-parent hwnd)))
          *window-list*))


(defun find-windows (&optional text-predicate)
  (let ((*window-list* ()))
    (win32:enum-windows (cffi:callback enum-windows-cb)  0)
    (remove-if-not
     (lambda (hwnd)
       (and
        ;; Keep visible windows
        (win32:is-window-visible hwnd)
        ;; Keep windows without parents
        (cffi:pointer-address (win32:get-parent hwnd))
        ;; Keep the "un-owned" windows
        (cffi:pointer-address (win32:get-window hwnd win32:+gw-owner+))
        ;; Remove windows that has no text
        (let ((text (com.inuoe.winutil:hwnd-text hwnd)))
          ;; Optionally keep only the windows that contains the needle
          (and text (plusp (length text))
               (or (null text-predicate)
                   (funcall text-predicate text))))))
     *window-list*)))

(defun list-top-windows ()
  (remove-if-not
   (lambda (hwnd)
     (and
      ;; Keep visible windows
      (win32:is-window-visible hwnd)
      ;; Keep windows without parents
      (cffi:pointer-address (win32:get-parent hwnd))
      ;; Keep the "un-owned" windows
      (cffi:pointer-address (win32:get-window hwnd win32:+gw-owner+))))
   (winutil:list-windows)))

(defun remove-anonymous-windows (window-list)
  "Remove windows that have no text"
  (remove-if-not
   #'(lambda (hwnd)
       (let ((text (com.inuoe.winutil:hwnd-text hwnd)))
         ;; Optionally keep only the windows that contains the needle
         (and text (plusp (length text)))))
   window-list))

#+ (or)
(length (remove-anonymous-windows (list-top-windows)))

#+ (or)
(length (find-windows))



#+ (or)
(alexandria:if-let ((hwnd (car (find-windows
                                #'(lambda (text)
                                    (search "Discord" text))))))
                   (activate-window hwnd))

;; Get the currently active window
#+ (or)

(com.inuoe.winutil:hwnd-text
 (win32:get-foreground-window))
;; "win32.lisp<catchall> - GNU Emacs"


;;; Switching to a windows

(defun activate-window (hwnd)
  (if (win32:is-iconic hwnd)
      (win32:show-window hwnd win32:+sw-showmaximized+)
      (win32:set-foreground-window hwnd)))

#+ (or)
(defparameter *w* (car (find-windows
                        #'(lambda (text)
                            (search "Discord" text)))))



;;; Filtering windows based on executable

(defun hwnd-pid (hwnd)
  "Get the PID of the process that owns a window."
  (cffi:with-foreign-pointer (process-id (cffi:foreign-type-size 'win32:dword))
    (win32:get-window-thread-process-id *w* process-id)
    (cffi:mem-ref process-id 'win32:dword)))

;; TODO make a version that wraps the result into `uiop:parse-native-namestring`
;; TODO handle the case where the result is truncated, get-module-file-name-ex returns the length of the string
;; TODO add this to winutil
(defun process-module-name (phandle)
  "Given a process handle, returns the fully qualified path to the file
that contains the module."
  (let ((len 1023)) ; that's a safe length, right? right!?
    (cffi:with-foreign-object (buf 'win32:tchar (1+ len))
      (let ((res (win32:get-module-file-name-ex
                  phandle
                  ;; hmodule: If this parameter is NULL,
                  ;; GetModuleFileNameEx returns the path of the
                  ;; executable file of the process specified in
                  ;; hProcess.
                  (cffi:null-pointer)
                  buf len))
            (last-error (win32:get-last-error)))
        (when (zerop res)
          (winutil:win32-error last-error))
        (winutil:tstring-to-lisp buf :count res)))))

(defun window-module-name (hwnd)
  "Given a window handle, returns the path to the corresponding
executable."
  (let ((phandle))
    (unwind-protect
         (progn
           (setf phandle
                 (win32:open-process
                  ;; desired access
                  (logior win32:+process-query-information+
                          win32:+process-vm-read+)
                  ;; inherit handle
                  nil
                  ;; PID
                  (hwnd-pid hwnd)))
           (process-module-name phandle))
      (when phandle
        (win32:close-handle phandle)))))

#+ (or)
(window-module-name *w*)
;; => "C:\\Users\\...\\Discord.exe"



;;; Global keyboard hook

;; this didn't compile right
#+ (or)
(cffi:with-foreign-slots ((vk-code scan-code flags)
                          (winutil::%coerce-pointer lparam)
                          'win32:kbd-ll-hook))

;;;;;;;;;;;;;;;;;;;;; Defining the hook's callback

#|
;; TODO: call-next-hook was removed from defhookfn, but it is still
;; used in the code below
(flet ((call-next-hook ()
(win32:call-next-hook-ex (cffi:null-pointer) ,ncode ,wparam ,lparam)))
(declare (ignorable #'call-next-hook))
,@body)
|#

(winutil:defhookfn ll-kbd-callback (ncode wparam lparam)
  (flet ((call-next-hook ()
           (win32:call-next-hook-ex (cffi:null-pointer) ncode wparam lparam)))
    (declare (ignorable #'call-next-hook))
    (format t "~&ncode: ~d" ncode)
    (if (minusp ncode)
        (winutil:call-next-hook)
        (let ((vk-code (cffi:foreign-slot-value
                        (winutil::%coerce-pointer lparam)
                        'win32:kbd-ll-hook
                        'win32:vk-code))
              (keyup-p
                (and
                 (or (= wparam win32:+wm-keyup+)
                     (= wparam win32:+wm-syskeyup+))
                 ;; This is redundant, but it serves as documentation
                 (not (or (= wparam win32:+wm-keydown+)
                          (= wparam win32:+wm-syskeydown+)))))
              (syskey-p (or (= wparam win32:+wm-syskeyup+)
                            (= wparam win32:+wm-syskeydown+))))
          (declare (ignorable vk-code
                              keyup-p
                              syskey-p))
          (format t "~&vk-code: ~d" vk-code)
          (cond
            ((= win32:+vk-numpad7+ vk-code)
             (format t "~&coucou")
             (when keyup-p
               ;; The focus doesn't change to the other window without
               ;; this sleep...
               (sleep 0.002)
               (activate-window (car (find-windows
                                      #'(lambda (text)
                                          (search "Firefox" text))))))
             ;; Return a non-zero value to prevent the system from
             ;; passing the message to the rest of the hook chain or
             ;; the target window procedure.
             1)
            (t
             (winutil:call-next-hook)))))))

;;;;;;;;;;;;;;;;;;;;; Installing the hook

;; Using a `progn` so that slime evalutates both set-windows-hook and
;; the message pump in the same thread.
(progn
  (let ((hhook
          (win32:set-windows-hook-ex
           ;; The type of hook
           win32:+wh-keyboard-ll+
           ;; Hook callback
           (cffi:get-callback 'll-kbd-callback)
           ;; hmod - handle to the dll containing the hook, null => current-thread
           (cffi:null-pointer)
           ;; thread-id
           0)))
    (when (cffi:null-pointer-p hhook)
      (error "Failed to install hook.")))

  (winutil:message-pump))
