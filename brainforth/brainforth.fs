\ -- Constants

5  constant stack-size ( To keep track of matching [ ] )
10 constant tape-size
20 constant src-size ( The maximum size of the program's source )

\ -- Initialize the turing machine

create tape tape-size cells allot
variable tape-pos

create stack stack-size allot
variable stack-pos

: init-tape ( -- )
  tape tape-size cells 0 fill
  0 tape-pos !
  stack stack-size cells 0 fill
  0 stack-pos !
;

\ -- Initialize the program

create src src-size chars allot
variable src-pos
variable src-length

: init-src ( -- )
  src src-size chars 0 fill
  0 src-pos !
  0 src-length !
;

: init ( -- ) init-tape init-src ;

: tc ( -- a-addr ) tape-pos @ ; \ tc: tape counter
: tc+ ( -- ) tc 1+ tape-pos ! ;
: tc- ( -- ) tc 1- tape-pos ! ;
: push ( -- )
  stack-pos @ 1+ stack-pos !
  tc stack stack-pos @ cells + !
;
: tos stack stack-pos @ cells + @ ;
: pop ( -- )
  tos tape-pos !
  stack-pos @ 1- stack-pos !
;

: data* ( -- a-addr ) tape tc cells + ;
: data ( -- n ) data* @ ;

: inc ( -- ) data 1+ data* ! ;
: dec ( -- ) data 1- data* ! ;
: zerop ( -- f ) data 0= ;
: !zerop ( -- f ) zerop invert ;

\ Copy a counted string into src
: store-src ( c-addr u-count -- )
  \ TODO Check that count < src-size
  \ saving the length of the source string
  dup src-length !
  src ( addr-from ucount addr-to -- )
  swap ( addr-from addr-to ucount -- )
  cmove
;

: pc ( -- ) src-pos @ ; \ pc: program counter
: pc+ ( -- ) pc 1+ src-pos ! ;
: pc- ( -- ) pc 1- src-pos ! ;

: instr* ( -- c-addr ) src pc chars + ;
: instr ( -- c ) instr* c@ ;

\ "Debugging" words
: show-tape ( -- ) tape-size 0 ?do tape i cells + ? loop cr ;
: show-data ( -- ) data . ;
: th-data ( u -- n ) cells tape + @ ;
: show-src ( -- ) src src-length @ type cr ;
: show-instr ( -- ) instr emit ;
: th-src ( u -- c ) chars src + c@ ;

: ]? [char] ? = ;
: jmp] ( -- ) src-length @ 0 ?do instr ]? if leave else pc+ then loop ;
: skip? ( -- ) zerop if jmp] then pc+ ;
: loop? ( -- ) ." TODO ] " cr ;

: input key ;
: output show-data ;

create actions
char + c, ' inc ,
char - c, ' dec ,
char < c, ' tc- ,
char > c, ' tc+ ,
char [ c, ' skip? ,
char ] c, ' loop? ,
char , c, ' input ,
char . c, ' output ,

: th-actions  ( n -- addr ) 1 chars cell + * actions + ;
: lookup-action ( c -- xt )
  8 0 ?do
    dup
    i th-actions c@ =
    if
      \ ." Match!"
      drop
      i th-actions char+ @
      unloop
      exit
    then
  loop
  ." Invalid operation: " emit cr
  bye
;

: step instr lookup-action execute ;

: run
  1000 0 ?do \ don't loop more than 1000 times, for my sanity
    show-tape
    pc .
    show-instr cr

    step pc+

    pc 0 <
    if
      ." Source out of bound: " pc . cr  bye
    then

    pc src-length @ >= if
      ." Reached the end of the program." cr leave
    then
  loop
;
