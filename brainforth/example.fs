\ Minimal example

s" brainforth.fs" included

init

\ Init the program's source
s" ++[.-]" store-src

\ Show the source
." Source: "
show-src
." (length: " src-length @ . ." )" cr

run
