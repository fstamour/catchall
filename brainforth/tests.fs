s" brainforth.fs" included

: assert ( c-addr u f -- )
  invert
  if
    ." Assertion failed: " type cr
    bye
  then
  2drop
;

: assert= ( c-add u n -- )
  2dup 2>r \ keep a copy of the data to compare
  <> \ compare
  if
    ." Assertion failed: '" type ." '"
    2r> \ restore the copy of the data
    ."  got " .
    ." expected " .
    cr
    bye
  then
  2r> 2drop \ drop the copy of the data
  2drop \ drop the assertion name
;

\ Previous versions of assert0
\ : assert0 ( c-addr u n -- ) 0= assert ;
\ : assert0 ( c-add u n -- )
\   0<> if
\     ." Assertion failed: '" type ." ' expected 0, got " . cr
\     bye
\   then
\ ;
: assert0 0 swap assert= ;

: test-init-tape
  init-tape
  s" tape-pos" tape-pos @ assert0
  tape-size 0 ?do
    i th-data 0<> if
      ." Tape is not initialized to 0 at position " i . cr bye
      ." The value is: " i th-data . cr
    then
  loop
  s" stack-pos" stack-pos @ assert0
  stack-size 0 ?do
    i cells stack + @ 0<> if
      ." Stack is not initialized to 0 at position " i . cr bye
      ." The value is: " i cells stack + @ . cr
    then
  loop
  ." test-init-tape passed" cr
;

: test-init-src
  init-src
  s" src-length" src-length @ assert0
  s" src-pos" src-pos @ assert0
  src-size 0 ?do
    i th-src 0<> if
      ." Src is not initialized to 0 at position " i . cr
      ." The value is: " i th-src . cr
      bye
    then
  loop
  ." test-init-src passed" cr
;

: test-stack
  init
  s" tos should be 0" tos assert0
  s" tos should be 2" tc+ tc+ push tos 2 assert=
  s" tc should be 2" tc 2 assert=
  s" tc should be restored after pop" tc+ tc+ pop tc 2 assert=
;

: test-tc
  init
  s" tape-pos should be 0" tc assert0
  s" tc should have been incremented" tc+ tc 1 = assert
  s" tc should have been decremented" tc- tc assert0
  ." test-tc passed" cr
;

: test-data
  init
  s" data should be 0" data 0= assert
  s" data should have been incremented" inc data 1 = assert
  s" data should have been decremented" dec data assert0
  ." test-data passed" cr
;

\ TODO test-pc

: test-action
  s" looking up + should return inc"
  [char] + lookup-action ['] inc = assert
  s" looking up - should return dec"
  [char] - lookup-action ['] dec = assert

  s" looking up < should return tc-"
  [char] < lookup-action ['] tc- = assert
  s" looking up > should return tc+"
  [char] > lookup-action ['] tc+ = assert

  s" looking up [ should return skip?"
  [char] [ lookup-action ['] skip? = assert
  s" looking up ] should return loop?"
  [char] ] lookup-action ['] loop? = assert

  s" looking up , should return input"
  [char] , lookup-action ['] input = assert
  s" looking up . should return output"
  [char] . lookup-action ['] output = assert
;

test-init-tape
test-init-src
test-stack
test-tc
test-data
test-action
." All tests passed!" cr bye
