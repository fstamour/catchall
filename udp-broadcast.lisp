(in-package #:common-lisp-user)

(defpackage #:udp-broadcast
  (:use :cl))

(in-package #:udp-broadcast)

(ql:quickload 'usocket)

(defparameter *s* (usocket:socket-connect
                   "255.255.255.255"
                   21027
                   :protocol :datagram
                   ;; in seconds
                   :timeout 1
                   :element-type '(unsigned-byte 8)))

;; (usocket:datagram-usocket )


(format t "~&socket-receive: ~a"
        (multiple-value-list
         (usocket:socket-receive *s* nil 32)))

;; (usocket:socket-send *s* )



(defparameter *s* (usocket:socket-listen
                   ;; "10.0.0.255"
                   "255.255.255.255"
                   21027
                   :reuse-address t
                   :element-type '(unsigned-byte 8)))
