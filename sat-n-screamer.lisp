
(in-package #:common-lisp-user)

(ql:quickload '(screamer))

(defpackage #:sat-n-screamer
  (:use :cl)
  (:import-from #:screamer
                #:fail
                #:either
                #:all-values))

(in-package #:sat-n-screamer)

(all-values
 (let ((x (either 1 2 3 4)))
   (if (oddp x)
       x
       (fail))))

(screamer:one-value
 (either 1 2))

;; Demonstrating how to define a generator (my nomenclature).
(screamer::defun an-int-between (min max)
  (if (> min max)
      (fail)
      (either min (an-int-between (1+ min) max))))

;; Using screamer's an-integer-between
(all-values
 (screamer:an-integer-between 1 5))

(all-values
 (an-int-between 5 10))

(let ((a '(1 2 3 4))
      (b '(a b c)))
  (loop for i = a then (cdr i)
        for j = b then (cdr j)
        for k from 1 below 10
        while (and i j)
          thereis nil ;; (= (car i) 2)
        finally (return (list i j))))

(defun compare-symbol (a b)
  (string< (symbol-name a)
           (symbol-name b)))

(defun compare-list-of-symbol (a b)
  (loop for i = a then (cdr i)
        for j = b then (cdr j)
        while (and i j)
          thereis (compare-symbol (car i)
                                  (car j))
        finally (return (and (null i) (not (null j))))))

(compare-list-of-symbol '() '())
(compare-list-of-symbol '(a) '())
(compare-list-of-symbol '() '(a))

(defun merge-types (types)
  (let ((deduped
          (remove-duplicates
           (sort types
                 #'(lambda (x y)
                     (if (symbolp x)
                         (if (symbolp y)
                             (compare-symbol x y)
                             t)
                         (if (symbolp y)
                             nil
                             (compare-list-of-symbol x y)))))
           :test #'equal)))
    (if (cdr deduped)
        `(or ,@deduped)
        (car deduped))))

(merge-types '(boolean boolean number seq boolean))
;; => (OR BOOLEAN NUMBER SEQ)
(merge-types '(boolean boolean boolean))
;; => BOOLEAN
(merge-types '((seq boolean) (seq boolean)))
;; => (SEQ BOOLEAN)


(defun type-of* (x)
  (cond
    ((eq t x) 'boolean)
    ((null x) 'null)
    ((symbolp x)
     (let ((first-char (aref (symbol-name x) 0)))
       (cond
         ((eq first-char #\_) 'hole)
         ((eq first-char #\?) 'var )
         (t 'symbol))))
    ((numberp x) 'number)
    ((and
      (listp x)
      (member (car x) '(map reduce filter)))
     (case (car x)
       (map 'map)
       (reduce 'reduce)
       (filter 'filter)))
    ((or
      (listp x)
      (arrayp x))
     `(seq ,(merge-types (mapcar #'type-of* x))))
    (t
     (cl:type-of x))))

(mapcar #'type-of*
        '(0 1      ;; number
          t      ;; boolean
          nil ()    ;; null
          (a b c)   ;; (seq symbol)
          (1 2 3)   ;; (seq number)
          (t 0 (a)) ;; (seq (or boolean number (seq symbol))
          _      ;; hole
          _x      ;; hole
          ?x      ;; var
          ))

;; TODO
(type-of* '(map function list))
(type-of* '(reduce fn list))
(type-of* '(filter predicate list))

(let ((input '((71 75 84) (90 87 95) (68 77 80)))
      (output '((75 84) (87 95) (77 80))))
  (values (type-of* input)
          (type-of* output))

  (let ((form (screamer:a-member-of  '(x
                                       y
                                       ;; boolean expressions
                                       (and _ _)
                                       (or _ _)

                                       ;; arithmethic
                                       (+ _ _)
                                       (- _ _)
                                       (* _ _)
                                       (/ _ _)

                                       ;; list operations
                                       (map _ _)
                                       (reduce _ _)
                                       (filter _ _)

                                       ))))))
