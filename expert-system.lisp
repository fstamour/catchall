(ql:quickload '(alexandria))

;; make it easier to debug
(declaim (optimize (speed 0) (safety 3) (debug 3)))

#|

Goal: Explore expert-system like program to help debugging

Motivation: I would like to automate more stuff

What am I going to try first: Write something that can run
things "intelligently" to localize the source of an issue, I'll make
it kind of abstract, so I can test it easily.

2024-08-07 Conclusion: this helped be make some concepts more concrete
and it also helped me figure out what I would like (I elicited my own
requirements out of myself 🤯). I conlcude that I think that
propagator networks would fit better, maybe.

|#


;; Let's start by defining a list of checks in some logical order...
(has-network-device
 has-ip-address
 has-internet)

;; "has-internet" implies that the earlier checks are true, but we
;; don't need to run them, we can assume they are true because
;; "has-internet" would be false otherwise.

(has-network-device
 has-ip-address
 has-internet)

;; furthermore, "has-internet" could be check by any of these pings,
;; but if one fails it doesn't mean "has-internet" is false.
(has-internet (ping "amazonaws.com")
              (ping "google.com")
              (ping "8.8.8.8"))


;; (do 'var) (check 'var) (goal 'var)


;; What about a more concrete example?

;; I want myelin to use ssh to connect to another host and to another
;; instance of myelin.

;; The current host must have some ip address (probably don't need
;; internet)
;;
;; The current host must have an ssh-agent running and it must have
;; keys int it.
;;
;; The other host must be reachable
;; The other host's name must be resolvable

;;;;; Design decision: keep most stuff in strings, so it could
;;;;; potentially be shown to a user. Or perhaps used by an LLM (!).

(define-check (has internet)
  "Try to determine if we have access to internet."
  (one-of
   ("Can ping one of google's DNS." (can shell "ping 8.8.8.8"))
   ("Can ping google.com (TODO: implies that DNS resolution works _or_ that
hostname was in the cache _or_ the DNS server is on the local
network.)" (can shell "ping google.com"))))


(define-check (running ssh-agent)
  "Checks if ssh-agent is running"
  ;; Must be able to run "ssh-add -l" without errors
  )

(define-check (has ssh-agent-key)
  "Checks if ssh-agent is running"
  ;; "ssh-add -l"'s output should indicate at least one key
  )


(define-goal (myelin contact ?host)
  "Ask for myelin to make contact with another instance running on ?host."
  :preconditions
  ((optional (has internet)
             "we don't need to have internet to be able to make contact with another
instance, but if every other conditions seems ok and we still cannot
make contact, then we can make theses further checks.")
   (running ssh-agent)))

;; TODO add :initform (error ...) where needed

(defclass documentation-mixin ()
  ((documentation :type string
                  :initarg :documentation)))

(defmethod documentation ((object documentation-mixin) (doc-type (eql t)))
  (slot-value object 'documentation))

(defclass name-mixin ()
  ((name :initarg :name
         :accessor name)))

#++ ;; later?
(defclass id-mixin ()
  (id :initarg :id))

(defclass fact (documentation-mixin name-mixin)
  ((support
    :initform nil
    :initarg :support
    :accessor support)))

(defun make-fact (name documentation &optional support)
  (make-instance 'fact
                 :name name
                 :documentation documentation
                 :support support))

(defmethod print-object ((fact fact) stream)
  (print-unreadable-object
      (fact stream :type t :identity nil)
    (format stream "~s" (name fact))))

(defun factp (x)
  (typep x 'fact))

;; (make-fact 'has-internet "Internet seems to be accessible.")
;; => #<FACT HAS-INTERNET>

(defclass rule (documentation-mixin)
  ((antecedant :initarg :antecedant :accessor antecedant)
   (consequent :initarg :consequent :accessor consequent)))

(defun make-rule (documentation antecedant consequent)
  (make-instance 'rule
                 :documentation documentation
                 :antecedant antecedant
                 :consequent consequent))

(defmethod print-object ((rule rule) stream)
  (print-unreadable-object
      (rule stream :type t :identity nil)
    (format stream "~s => ~s" (antecedant rule) (consequent rule))))

(defun rulep (x)
  (typep x 'rule))

(let ((rule (make-rule "test" 'a 'b)))
  (documentation rule t))
;; => "test"

;; I will need an object to represent the whole system
;;  - goals, subgoals facts, rules, what else?
;; I will need an object to hold all the facts
;; I will need an object to hold the rules


;; Inference engine: mainly top-down, but when a consequent adds a
;; fact, we can infer new facts "bottom-up" from this one.

(defclass expert (documentation-mixin)
  ((rules :initform (make-hash-table) :accessor rules)
   (antecedant-index :initform (make-hash-table))
   (consequent-index :initform (make-hash-table))
   (facts :initform (make-hash-table) :accessor facts)
   (fact-index :initform (make-hash-table))))

(defun make-expert ()
  (make-instance 'expert
                 :documentation "A mysteriously undocumentated expert system."))

;; TODO there's no de-duplication (because I used objects...)

(defun extract-symbols (x)
  (remove-if-not #'symbolp (alexandria:flatten x)))

(defun index (expert target x index-name
              &aux (index (slot-value expert index-name)))
  (map nil (lambda (symbol)
             (pushnew target (gethash symbol index)))
       (extract-symbols x)))

(defun add-rule (expert rule)
  (index expert rule (antecedant rule) 'antecedant-index)
  (index expert rule (consequent rule) 'consequent-index)
  (push (gethash rule (rules expert)) rule))

(defun add-fact (expert fact)
  (index expert fact (name fact) 'fact-index)
  ;; maybe map the facts by their "name"?
  (setf (gethash fact (facts expert)) fact))

(defmethod add ((expert expert) (rule rule))
  (add-rule expert rule))

(defmethod add ((rule rule) (expert expert))
  (add-rule expert rule))

(defmethod add ((expert expert) (fact fact))
  (add-fact expert fact))

(defmethod add ((fact fact) (expert expert))
  (add-fact expert fact))

#++
(documentation
 (make-expert) t)

#++
(let ((e (make-expert))
      (r1 (make-rule "Rule 1" 'x 'y))
      (r2 (make-rule "Rule 2" 'y 'z))
      (x (make-fact 'x "x")))
  (add e r1)
  (add e r2)
  (add e x)
  e)

(defun find-fact (expert goal)
  (find goal
        (loop
          :with index = (slot-value expert 'fact-index)
          :for symbol :in (extract-symbols goal)
          :for candidates = #1=(gethash symbol index)
            :then (intersection candidates #1#)
          ;; No candidates left, early exit
          :while candidates
          ;; Only 1 candidate left, early exit
          :when (null (rest candidates))
            :do (return candidates)
          :finally (return candidates))
        :key #'name
        ;; TODO replace #'eq by some unification or matching
        :test #'eq))

(defun match-a-consequent (rule goal)
  ;; TODO actually check if the goal matches one of the rule's
  ;; consequents
  t)

(defun reduce-index (index needles reducer)
  (loop
    :for needle :in needles
    :for accumulator = (gethash needle index)
      :then (funcall reducer
                     accumulator
                     (gethash needle index))
    :finally (return accumulator)))

(trace find-rules-by-consequent)
(defun find-rules-by-consequent (expert goal)
  (remove-if-not
   (lambda (rule)
     (match-a-consequent goal rule))
   ;; Find all the rules that contains some of the symbols of the goal
   (reduce-index
    (slot-value expert 'consequent-index)
    (extract-symbols goal)
    #'union)
   #++ ;; Find all the rules that contains all the symbols of the goal
   (loop
     :with index = (slot-value expert 'consequent-index)
     :for symbol :in (extract-symbols goal)
     :for candidates = #1=(gethash symbol index)
       :then (intersection candidates #1#)
     ;; No candidates left, early exit
     :while candidates
     ;; Only 1 candidate left, early exit
     :when (null (rest candidates))
       :do (return candidates)
     :finally (return candidates))))

;; TODO maybe it needs something else?
(defun fire-rule (expert rule))

(defun prove-step (expert goal)
  (or
   ;; check if GOAL is in the FACTS
   (find-fact expert goal)
   ;; TODO check if GOAL is in one of the consequents
   (find-rules-by-consequent expert goal)))

(defun prove (expert goal fire-rule-hook)
  (loop
        :repeat 10
        :with goal-stack = (list goal)
        :for goal = (first goal-stack)
        :for next-step = (prove-step expert goal)
        :do
        (cond
          ;; The current goal was proven
          ((factp next-step)
           (pop goal-stack))
          ;; Else (prove-step) returned a list of rules
          ;; If it's empty, it's stuck
          ((null next-step)
           (return (list :stuck goal-stack)))
          ;; Otherwise we check if they can be fired
          (t ))
        :if (factp next-step)
        :do
        :else
        :do (setf goal-stack (append next-step goal-stack))
        :while (and goal-stack next-step)))

(let ((e (make-expert))
      (r1 (make-rule "Rule 1" 'x 'y))
      (r2 (make-rule "Rule 2" 'y 'z))
      (x (make-fact 'x "x")))
  (add e r1)
  (add e r2)
  (add e x)
  (list
   (prove-step e 'x)
   (prove-step e 'z)))


(let ((e (make-expert))
      (r1 (make-rule "Rule 1" 'x 'y))
      (r2 (make-rule "Rule 2" 'y 'z))
      (x (make-fact 'x "x")))
  (add e r1)
  (add e r2)
  (add e x)
  (prove e 'z))
