;; Note: this file is not "clean" but it's still possible to eval it
;; without errors.

(in-package #:common-lisp-user)

(defpackage #:compressed-bitmap-index
  (:nicknames ":cbi")
  (:use :cl))

(in-package #:compressed-bitmap-index)

#|

Just toying around compression schemes that can be answer queries
without decompressing.

Namely: Byte-aligned Bitmap Compression (BBC) because it's the simplest.
Here the patent: https://patents.google.com/patent/US6205442B1/en

Alternative (to name a few)

BAH
CAMP
COMPAX
CONCISE
EWAH
MASC
PLWAH
PLWAH+
PWAH
Roaring
SBH
SECOMPAX
SPLWAH
WAH

It's basicaly Run-Length Encoding (RLE) that doesn't try to compress
the imcompressible.

P.S. I'm not trying to be efficient.

|#



;;; as a warmup: RLE compression

(defun rle (vector)
  (check-type vector vector)
  (unless (zerop (length vector))
    (let ((current-element (aref vector 0))
          (count 0))
      (append
       (loop
         :for new-element :across vector
         :if (eq current-element new-element)
           :do (incf count)
         :else
           :collect (prog1 (cons current-element count)
                      (setf current-element new-element
                            count 1)))
       (list (cons current-element count))))))

(rle #(0 0 0 1 1 2 3))
;; => ((0 . 3) (1 . 2) (2 . 1) (3 . 1))

(defun unrle (list)
  (loop :for (element . count) :in list
        :append (loop :for i :below count :collect element)))

(unrle (rle #(0 0 0 1 1 2 3)))
;; => (0 0 0 1 1 2 3)



;;; then BBC compression - with only the information from the first
;;; page of the paper (not the patent)
;;
;; - gaps are runs of 0s or 1s (the byte 255)
;; - maps are strethces of 0s AND 1s
;; - gaps and maps comes in pairs

(deftype octet ()
  '(unsigned-byte 8))

(deftype octets (&optional (length '*))
  `(simple-array octet (,length)))

(defun make-octets (seq)
  (map `(octets ,(length seq)) #'identity seq))

(defun zero-or-ff-p (octet)
  (declare (type octet octet))
  (or
   (eq 0 octet)
   (eq 255 octet)))

(defun compute-gap (octets start)
  "Returns the index of the gap's end and if it's a run of 0s or 1s,
returns nil if the gap's lenght is 0."
  (declare (type octets octets))
  (unless (>= start (length octets))
    (let ((element (aref octets start)))
      (when (zero-or-ff-p element)
        (let ((end (or
                    (position-if #'(lambda (x) (not (eq element x)))
                                 octets
                                 :start start)
                    (length octets))))
          (values (if (zerop element) 0 1) end))))))

(compute-gap (make-octets #(0 0 0 1)) 0)
;; => 3

(compute-gap (make-octets #(2 0 0 1)) 0)
;; => nil

(defun compute-map (octets start)
  "Returns the map, returns nil if it's length is 0."
  (declare (type octets octets))
  (unless (>= start (length octets))
    (let ((element (aref octets start)))
      (unless (zero-or-ff-p element)
        (let ((end (or
                    (position-if #'(lambda (x) (zero-or-ff-p x))
                                 octets
                                 :start start)
                    (length octets))))
          (make-octets
           (subseq octets start end)))))))


(compute-map (make-octets #(0 0 0 1)) 0)
;; => nil

(compute-map (make-octets #(2 0 0 1)) 0)
;; => #(2)

(defun bbc (octets)
  (check-type octets (octets *))
  (let ((index 0))
    (loop :while (< index (length octets))
          :for element = (aref octets index)
          :for i :below 50 ;; safe guard against infinite loop
          :collect
          (cons
           (multiple-value-bind (type new-index)
               (compute-gap octets index)
             (if new-index
                 (prog1
                     (list (- new-index index) type)
                   (setf index new-index))
                 0))
           (let ((map (compute-map octets index)))
             (when map
               (prog1 map (setf index (+ index (length map))))))))))

;; no corner-case
(bbc (make-octets #(0 0 32 241 32 255 255 255 2 0 0 32)))
#+nil
(((2 0) . #(32 241 32)) ((3 1) . #(2)) ((2 0) . #(32)))

(mapcar (lambda (x) (cons (type-of (car x)) (type-of (cdr x))))
        (bbc (make-octets #(0 0 32 241 32 255 255 255 2 0 0 32))))
#+nil
((CONS SIMPLE-ARRAY (UNSIGNED-BYTE 8) (3))
 (CONS SIMPLE-ARRAY (UNSIGNED-BYTE 8) (1))
 (CONS SIMPLE-ARRAY (UNSIGNED-BYTE 8) (1)))

;; both corner-cases (doens't start with a gap, doesn't end with a map.
(bbc (make-octets #(32 241 32 255 255 255 2 0 0 32 0)))
#+nil
((0 . #(32 241 32)) ((3 1) . #(2)) ((2 0) . #(32)) ((1 0)))

(defun bbc-length (bbc)
  (loop
    :for (gap . map) :in bbc
    :sum (+ (if (consp gap) (car gap) 0) (if map (length map) 0))))

(let ((octets (make-octets #(32 241 32 255 255 255 2 0 0 32 0))))
  (- (length octets)
     (bbc-length (bbc octets))))
;; => 0
;; looks good, for that one test


(defun unbbc (bbc)
  (let* ((length (bbc-length bbc))
         (result (make-array length :element-type 'octet))
         (index 0))
    (loop :for (gap . map) :in bbc
          :do
             (when (consp gap)
               (destructuring-bind (gap-length type) gap
                 ;; If it's run of 0s, we just skip
                 (unless (zerop type)
                   (loop :for i :below gap-length
                         :do (setf (aref result (+ index i)) 255)))
                 ;; update index
                 (incf index gap-length)))
             (when map
               (replace result map :start1 index)
               ;; update index
               (incf index (length map))))
    result))


(let ((octets (make-octets #(32 241 32 255 255 255 2 0 0 32 0))))
  (equalp octets (unbbc (bbc octets))))
;; => T


;;;; Next time:
;;
;; - the paper says: "single map byte having only one bit different than the
;; others is encoded directly into a control byte using a "diffetent bit"
;; position within this map byte.
;; - test with randomly-generated arrays to make sure the base is alright.
;; - compute intersection and usion of compressed bitmaps.
;; - try to serialize the bbc's to an array of octets (for persistance)
;; - more esoteric: compress a bloom filter with this compression scheme.
