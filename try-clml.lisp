(in-package #:common-lisp-user)

(defpackage #:try-clml
  (:nicknames ":tclml")
  (:use :cl))

(in-package #:try-clml)

;; Must start sbcl with a certain minimum dynamic heap size.

;; This must be called before loading clml
(setf *read-default-float-format* 'double-float)

;; Also, clml will try to load libssl

(handler-bind
    ((cffi:load-foreign-library-error #'(lambda (condition)
                                          (when (and (eq (type-of condition)
                                                         'cffi:load-foreign-library-error))
                                            (invoke-restart-interactively
                                             (find-restart 'asdf/action:accept))))))
  (ql:quickload '(clml clml.docs)))


;; Generate the documentation
(clml.docs:generate-api-docs)


(apropos-list "dataset")
#|
CLML.ASSOCIATION-RULE::ASSOC-RESULT-DATASET
:CLML.DATA.R-DATASETS
:CLML.DATA.R-DATASETS-PACKAGE
:DATASET
:NUMBER-OF-DATASETS
:R-DATASETS
CLML.ASSOCIATION-RULE::LABELED-DATASET
CLML.ASSOCIATION-RULE::RESULT-DATASET
CLML.ASSOCIATION-RULE::UNSP-DATASET
CLML.CLUSTERING.NMF::CORPUS-DATASET
CLML.CLUSTERING.NMF::DATASET-LENGTH
CLML.CLUSTERING.NMF::NUMERIC-DATASET-DIMENSIONS
CLML.CLUSTERING.NMF::NUMERIC-DATASET-LENGTH
CLML.CLUSTERING.OPTICS::DATASET
CLML.DATA.R-DATASETS
CLML.DATA.R-DATASETS::DATASET
CLML.DATA.R-DATASETS::DATASET-DIRECTORY
CLML.DATA.R-DATASETS::DATASET-FILE
CLML.DATA.R-DATASETS:DATASET-DOCUMENTATION
CLML.DATA.R-DATASETS:GET-DATASET
CLML.DATA.R-DATASETS:GET-R-DATASET-DIRECTORY
CLML.DECISION-TREE.DECISION-TREE::VALIDATION-DATASET
CLML.DECISION-TREE.RANDOM-FOREST::VALIDATION-DATASET
CLML.GRAPH.GRAPH-ANOMALY-DETECTION::DATASET
CLML.HJS.K-MEANS::DATASET
CLML.HJS.READ-DATA::CATEGORY-DATASET
CLML.HJS.READ-DATA::DATASET
CLML.HJS.READ-DATA::DATASET-IN
CLML.HJS.READ-DATA::MAKE-CATEGORY-DATASET
CLML.HJS.READ-DATA::NUMBER-OF-DATASETS
CLML.HJS.READ-DATA:CONCATENATE-DATASETS
CLML.HJS.READ-DATA:COPY-DATASET
CLML.HJS.READ-DATA:DATASET-CATEGORY-POINTS
CLML.HJS.READ-DATA:DATASET-CLEANING
CLML.HJS.READ-DATA:DATASET-DIMENSIONS
CLML.HJS.READ-DATA:DATASET-NAME-INDEX-ALIST
CLML.HJS.READ-DATA:DATASET-NUMERIC-POINTS
CLML.HJS.READ-DATA:DATASET-POINTS
CLML.HJS.READ-DATA:DEDUP-DATASET!
CLML.HJS.READ-DATA:DIVIDE-DATASET
CLML.HJS.READ-DATA:MAKE-BOOTSTRAP-SAMPLE-DATASETS
CLML.HJS.READ-DATA:MAKE-NUMERIC-AND-CATEGORY-DATASET
CLML.HJS.READ-DATA:MAKE-NUMERIC-DATASET
CLML.HJS.READ-DATA:MAKE-NUMERIC-MATRIX-AND-CATEGORY-DATASET
CLML.HJS.READ-DATA:MAKE-NUMERIC-MATRIX-DATASET
CLML.HJS.READ-DATA:MAKE-UNSPECIALIZED-DATASET
CLML.HJS.READ-DATA:NUMERIC-AND-CATEGORY-DATASET
CLML.HJS.READ-DATA:NUMERIC-DATASET
CLML.HJS.READ-DATA:NUMERIC-MATRIX-AND-CATEGORY-DATASET
CLML.HJS.READ-DATA:NUMERIC-MATRIX-DATASET
CLML.HJS.READ-DATA:SHUFFLE-DATASET!
CLML.HJS.READ-DATA:SPECIALIZED-DATASET
CLML.HJS.READ-DATA:UNSPECIALIZED-DATASET
CLML.HJS.READ-DATA:WRITE-DATASET
CLML.NEAREST-SEARCH.NEAREST::DATASET
CLML.NEAREST-SEARCH.NEAREST::TEST-DATASET
CLML.NONPARAMETRIC.DPM-INTERFACE::DATASET
CLML.NONPARAMETRIC.DPM::MAKE-MULTIVAR-GAUSS-DATASET
CLML.NONPARAMETRIC.DPM::MAKE-REAL-GAUSS-DATASET
CLML.PCA::DATASET
CLML.PCA::FACE-DATASET
CLML.PCA::TRAIN-DATASET
CLML.TEXT.HDP-LDA::DATASET
CLML.TIME-SERIES.READ-DATA:TIME-SERIES-DATASET
|#


;; Interesting:
;; clml.data.r-datasets


(clml.hjs.read-data:make-unspecialized-dataset
 '("x" "y")
 #((0 1) (2 3)))

#|
So, the first 2 parameters for make-unspecialized-dataset are:
- all-column-names: must be a list of string
- data: must be a simple-vector
|#


(ql:system-apropos-list "jpeg")
;; => (#<QL-DIST:SYSTEM cl-jpeg / cl-jpeg-20170630-git / quicklisp 2020-02-18>)

(ql:quickload 'cl-jpeg)
(defparameter *images-root* (merge-pathnames "programmation/machine-learning/receipts/photos/"
                                             (user-homedir-pathname)))

;; List files in directory *images*
(cl-fad:list-directory *images-root*)

;; List jpg files in directory *images*
(length
 (directory
  (make-pathname :directory (pathname-directory *images-root*)
                 :name :wild
                 :type "jpg")))
;; => 112

(time
 (defparameter *images*
   (mapcar (lambda (path)
             (multiple-value-list
              (cl-jpeg:decode-image path)))
           (butlast
            (directory
             (make-pathname :directory (pathname-directory *images-root*)
                            :name :wild
                            :type "jpg"))
            108))))
;; Took 60 seconds for all of them, which is pretty slow for my
;; computer...

(defparameter *image* (first *images*))

(time
 (defparameter *image*
   (multiple-value-list
    (cl-jpeg:decode-image
     (first
      (directory
       (make-pathname :directory (pathname-directory *images-root*)
                      :name :wild
                      :type "jpg")))))))
;; 0.5 seconds

(mapcar #'type-of *images*)
;; =>
;; ((SIMPLE-ARRAY (UNSIGNED-BYTE 8) (36578304))
;;  (INTEGER 0 4611686018427387903)
;;  (INTEGER 0 4611686018427387903)
;;  (INTEGER 0 4611686018427387903)
;;  NULL)
(rest *image*)
;; => (3024 4032 3 NIL)
;;     y    x    d ?

(apply #'= (mapcar (lambda (x) (length (first x))) *images*))
;; => T
;; They all have the same size


;;; Trying to createa a dataset from the image

(clml.hjs.read-data:make-unspecialized-dataset
 '("pixels")
 `#(,*image*))

(clml.hjs.read-data:make-numeric-dataset
 '("pixels")
 #(#(1) #(2) #(3))
 )
;; => The value of (AREF CLML.HJS.READ-DATA::SPECIALIZED-DATA 0) is
;; #(1), which is not of type CLML.HJS.META:DVEC.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Numeric dataset means each datapoint is a vector ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(clml.hjs.meta:make-dvec 2 .0)
;; => #(0.0 0.0)

(type-of
 (clml.hjs.meta:make-dvec 2 .0))
;; => (SIMPLE-ARRAY DOUBLE-FLOAT (2))

(clml.hjs.read-data:make)

(defparameter *image-dvec*
  (map '(simple-array (simple-array (unsigned-byte 8) (1)) (36578304))
       #'(lambda (x)
           ;; (coerce x 'double-float)
           (clml.hjs.meta:make-dvec 1 (coerce x 'double-float)))
       (first *image*)))

(defparameter *image-dataset*
  (clml.hjs.read-data:make-numeric-dataset
   '("pixels")
   *image-dvec*))
;; So here I was able to create a dataset with one column, and one row per pixel
;; How do I get to have has many columns as pixels?

;; Make some room
(map nil #'makunbound '(*images-dataset* *image-dvec*))


(defparameter *image-dataset*
  (clml.hjs.read-data:make-numeric-matrix-dataset
   '("pixels")
   (map '(simple-vector) #'first *images*)))

(defun image-to-dvec (image)
  (let ((data (first image)))
    (map 'clml.hjs.meta:dvec #'(lambda (x) (coerce x 'double-float)) data)))

(time
 (defparameter *image-data* (image-to-dvec *image*)))

(type-of *image-data*)
;; => (SIMPLE-ARRAY DOUBLE-FLOAT (36578304))

(clml.hjs.read-data:make-unspecialized-dataset
 '("pixel")
 `#(,*image-data*))


(defparameter *images-data*
  (map '(simple-array double-float (* *))
       #'image-to-dvec
       *images*))


(apropos-list "make-dmat")
(CLML.HJS.MATRIX:MAKE-DMAT)


(clml.hjs.matrix:make-dmat )
