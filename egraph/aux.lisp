(in-package #:catchall.egraph.tests)

;; vector< was just a prototype for enode<

#++
(defun vector< (v1 v2)
  (unless (eq v1 v2)
    (loop
      :for el1 :across v1
      :for el2 :across v2
      :if (< el1 el2)
        :do (return t)
      :else
        :if (> el1 el2)
          :do (return nil)
      :finally
         ;; if we get there it's because either the 2 vectors are equal, or
         ;; one is the prefix of the other.
         ;;
         ;; So we return true, if v1 is shorter or has the same length than
         ;; v2.
         (< (length v1) (length v2)))))

#++
(define-test+run "vector<"
  (false (vector< #1=#() #1#))
  (false (vector< #() #()))
  (true (vector< #(0) #(1)))
  (false (vector< #(0) #(0)))
  (false (vector< #(1) #(0))))



(define-test+run "enode<"
  ;; eq
  (progn
    (false (enode< #1=#() #1#))
    (false (enode< #2=1 #2#))
    (false (enode< 'x 'x)))
  ;; malformed enodes
  (progn
    (false (enode< #() #()))
    (false (enode< #(x) #(y)))
    (false (enode< #(x) #(x)))
    (false (enode< #(y) #(x))))
  ;; proper enodes with children
  (progn
    (false (enode< #() #()))
    (true (enode< #(x 0) #(y 1)))
    (false (enode< #(x 0) #(x 0)))
    (false (enode< #(y 1) #(x 0))))
  ;; symbols
  (progn
    (true (enode< 'x 'y))
    (false (enode< 'y 'x)))
  ;; symbols v.s. vectors
  (progn
    (true (enode< 'x #()))
    (false (enode< #() 'x)))
  ;; symbols v.s. numbers
  (progn
    (true (enode< 0 'x))
    (false (enode< 'x 0)))
  ;; vectors v.s. numbers
  (progn
    (true (enode< 0 #()))
    (false (enode< #() 0)))
  ;; numbers v.s. numbers
  (progn
    (true (enode< 0 1))
    (false (enode< 0 0))))



(defun dump-enodes (egraph)
  "dump enodes"
  (sort
   (loop
     :for enode :being :the :hash-key :of (enode-eclasses egraph)
       :using (hash-value eclass-id)
     :collect (list :enode (if (vectorp enode)
                               (copy-seq enode)
                               enode)
                    :eclass-id eclass-id))
   #'enode<
   :key #'second))

(defun hash-table-values (hash-table)
  (loop
    :for _ :being :the :hash-key :of hash-table :using (hash-value value)
    :collect value))

(defun dump-eclass (egraph eclass &aux (eclass-id (id eclass)))
  `(
    :eclass-id ,eclass-id
    :enodes ,(copy-seq (enodes eclass))
    ,@(when (plusp (hash-table-count (parents eclass)))
        (list :parents (sort (hash-table-values (parents eclass))
                             #'<)))
    ,@(let ((cannonical-id (eclass-find egraph eclass-id)))
        (unless (= eclass-id cannonical-id)
          (list := cannonical-id))))  )

(defun dump-eclasses (egraph)
  "dump eclasses"
  (sort
   (loop
     :for eclass-id :being :the :hash-key :of (eclasses egraph)
       :using (hash-value eclass)
     :collect (dump-eclass egraph eclass))
   #'<
   :key #'second))

(defun dump-egraph (egraph)
  `(,@(when (plusp (hash-table-count (enode-eclasses egraph)))
        (list :enodes (dump-enodes egraph)))
    ,@(when (plusp (hash-table-count (eclasses egraph)))
        (list :eclasses (dump-eclasses egraph)))
    ,@(when (pending egraph)
        (list :pending (pending egraph)))))
