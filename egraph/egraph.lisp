#|

My stab at trying to implement E-graphs. Mostly for my own
understanding.

|#

(cl:in-package #:cl-user)

;; https://github.com/fstamour/disjoint-sets
(ql:quickload '#:disjoint-sets)

(ql:quickload '#:parachute)



(defpackage #:catchall.egraph
  (:documentation "")
  (:use #:cl #:disjoint-sets)
  #++
  (:export
   #:make-enode
   #:op
   #:operands
   #:leafp)
  (:export
   #:make-eclass
   #:id
   #:enodes
   #:parents)
  (:export
   #:make-egraph
   #:egraph-eclass
   #:enode-eclasses
   #:union-find
   #:eclasses
   #:pending)
  (:export
   #:eclass-find
   #:cannonicalize
   #:enode-eclass-id
   #:add
   #:add-form
   #:merge-eclass
   #:rebuild)
  (:export
   #:root-eclasses))

(in-package #:catchall.egraph)

;;; - e-graphs ≅ set of e-classes
;;; - e-classes ≅ set of e-nodes
;;; - e-node ≅ an operator + operands e-classes (not e-nodes)


#++
(progn
  (defun ensure-simple-vector (sequence)
    (etypecase sequence
      (list (concatenate 'simple-vector sequence))
      (simple-vector sequence)
      (vector (concatenate 'simple-vector sequence))))

  (defun check-all-ids (vector)
    "Check that all elements of VECTOR are e-class ids."
    (loop :for eclass-id :across vector
          :do (check-type eclass-id (integer 0)
                          "an eclass identifier (an integer >= 0)"))
    vector)

  (defclass enode ()
    ((op
       :initform (error "enode: \"op\" must be defined.")
       :initarg :op
       :accessor op)
     (operands
      :type (simple-vector *)
      :initarg :operands
      :accessor operands)))

  (defun make-enode (op &optional operands)
    (if operands
        (make-instance
         'enode
         :op op
         :operands (check-all-ids (ensure-simple-vector operands)))
        (make-instance 'enode :op op)))

  (defmethod leafp (enode)
    (not (slot-boundp enode 'operands))))

;; TODO this should be a generic function + methods
(defun enode< (a b)
  (unless (eq a b)
    (cond
      ((eq a b) nil)
      ;; if one is a vector, but not the other
      ((and (not (vectorp a)) (vectorp b)) t)
      ((and (vectorp a) (not (vectorp b))) nil)
      ;; if a and b are symbols
      ((and (symbolp a) (symbolp b)) (string< a b))
      ;; if one is a number, but not the other
      ((and (not (numberp a)) (numberp b)) nil)
      ((and (numberp a) (not (numberp b))) t)
      ;; if they're both numbers
      ((and (numberp a) (numberp b)) (< a b))
      ;; if they're both vectors:
      ((and (vectorp a) (vectorp b))
       (loop
         :for i :from 0
         :for el1 :across a
         :for el2 :across b
         :unless (zerop i)
           :do (cond
                 ;; _could_ recurse here
                 ((< el1 el2) (return t))
                 ((> el1 el2) (return nil)))
         :finally
            ;; if we get there it's because either the 2 vectors are equal, or
            ;; one is the prefix of the other.
            ;;
            ;; So we return true, if a is shorter or has the same length than
            ;; b.
            (< (length a) (length b)))))))



(defun make-enode-map ()
  "Make an empty map from enodes to eclass ids"
  (make-hash-table :test 'equalp))

(defclass eclass ()
  ((id
    :initform (error "eclass: ID must be specified.")
    :initarg :id
    :accessor id)
   (enodes
    :initform (make-array '(0) :adjustable t :fill-pointer t)
    :initarg :enodes
    :accessor enodes)
   (parents
    :initform (make-enode-map)
    :initarg :parents
    :accessor parents))
  (:documentation "An e-class"))

(defun make-eclass (id enodes &optional parents)
  (make-instance
   'eclass
   :id id
   :enodes (make-array (list (length enodes))
                       :adjustable t
                       :fill-pointer t
                       :initial-contents enodes)
   :parents (or parents (make-enode-map))))

(defmethod add (eclass enode)
  "Add an e-node to an e-class"
  (vector-push-extend enode (enodes eclass)))

(defmethod add-parent (eclass parent-enode parent-eclass-id)
  (setf (gethash parent-enode (parents eclass)) parent-eclass-id))

(defmethod print-object ((eclass eclass) stream)
  (print-unreadable-object
      (eclass stream :type t :identity t)
    (if (= 1 (length (enodes eclass)))
        (format stream "~d (1 enode: ~s, ~d parent enodes)"
                (id eclass)
                (aref (enodes eclass) 0)
                (hash-table-count (parents eclass)))
        (format stream "~d (~d enodes, ~d parent enodes)"
                (id eclass)
                (length (enodes eclass))
                (hash-table-count (parents eclass))))))



(defclass egraph ()
  ((enode-eclasses ;; e-node -> e-class id
    :initform (make-hash-table :test 'equalp)
    :accessor enode-eclasses
    :documentation "A mapping from e-node to e-class ID.")
   (eclasses
    :initform (make-hash-table)
    :initarg :eclasses
    :accessor eclasses
    :documentation "A mapping of e-class ID to e-class (said otherwise: this is the set of
e-classes, indexed by their IDs.")
   (union-find
    :initform (make-disjoint-sets)
    :initarg :union-find
    :accessor union-find
    :documentation "A union-find data structure, that keeps track of equivalences between
e-classes.")
   (pending
    :initform (list)
    :initarg :pending
    :accessor pending
    :documentation "A list of pending e-class ids to fix their invariants."))
  (:documentation "An equivalence graph"))

(defmethod print-object ((egraph egraph) stream)
  (print-unreadable-object
      (egraph stream :type t :identity t)
    (format stream "(~d e-nodes across ~d (~d) e-classes; ~d pending repair)"
            (hash-table-count (enode-eclasses egraph))
            (hash-table-count (eclasses egraph))
            (length (union-find egraph))
            (length (pending egraph)))))

(defun make-egraph ()
  (make-instance 'egraph))



;; TODO maybe rename just "eclass"
(defun egraph-eclass (egraph eclass-id)
  "Get an e-class from its ID."
  (gethash eclass-id (eclasses egraph)))

;; TODO maybe rename just "eclass"
(defun (setf egraph-eclass) (eclass egraph eclass-id)
  "Add an e-class, indexed by its ID."
  (setf (gethash eclass-id (eclasses egraph)) eclass))

;; TODO maybe rename just "eclass-id"
(defun enode-eclass-id (egraph enode)
  "Get an eclass from an enode."
  (gethash enode (enode-eclasses egraph)))

;; TODO maybe rename just "eclass-id"
(defun (setf enode-eclass-id) (eclass-id egraph enode)
  "Set an e-node's e-class."
  (setf (gethash enode (enode-eclasses egraph)) eclass-id))

(defun eclass-find (egraph eclass-id)
  "Find the cannonical id for e-class ECLASS-ID."
  (disjoint-sets-find (union-find egraph) eclass-id))



;; TODO
(defmethod cannonicalize (egraph enode)
  (loop
    :with changed
    :with new-enode = (copy-seq enode)
    :for i :from 1 :below (length enode)
    :for eclass-id = (aref enode i)
    :for cannonical-eclass-id = (eclass-find egraph eclass-id)
    :unless (= eclass-id cannonical-eclass-id)
      :do (setf changed t
                (aref new-enode i) cannonical-eclass-id)
    :finally (return (values (if changed
                                 new-enode
                                 enode)
                             changed))))

(defmethod add (egraph enode)
  "Add an e-node to an e-graph, creating a new e-class if necessary."
  (or (enode-eclass-id egraph enode)
      (let* ((id (disjoint-sets-add (union-find egraph)))
             (eclass (make-eclass id (list enode))))
        (setf (enode-eclass-id egraph enode) id)
        (setf (egraph-eclass egraph id) eclass)
        (when (vectorp enode)
          (loop
            :for i :from 1 :below (length enode)
            :for child-eclass-id := (aref enode i)
            :for child-eclass := (egraph-eclass egraph child-eclass-id)
            :do (add-parent child-eclass enode id)
                ;; (setf (gethash enode (parents child-eclass)) id)
                ;; (vector-push-extend (cons enode id) (parents child-eclass))
            ))
        id)))

(defmethod add-form (egraph form)
  "Add a FORM to an e-graph, creating e-classes if necessary."
  (add egraph form))

(defmethod add-form (egraph (form cons))
  "Add a FORM to an e-graph, creating e-classes if necessary."
  (add egraph (apply #'vector (car form) (mapcar (lambda (element) (add-form egraph element)) (rest form)))))


;;; union

(defmethod merge-eclass (egraph id1 id2)
  (let ((cannonical-id1 (eclass-find egraph id1))
        (cannonical-id2 (eclass-find egraph id2)))
    ;; (break)
    (if (= cannonical-id1 cannonical-id2)
        cannonical-id1
        (let ((new-cannonical-id
                (disjoint-sets-union (union-find egraph) id1 id2)))
          (push new-cannonical-id (pending egraph))
          new-cannonical-id))))


;;; TODO rebuilding

(defmethod repair-parent-enodes (egraph eclass)
  "Make sure each e-nodes is cannonical and points to a cannonical
e-class."
  (loop
    :for parent-enode
      :being :the :hash-key :of (parents eclass)
        :using (hash-value parent-eclass-id)
    :for new-enode = (cannonicalize egraph parent-enode)
    ;; Make sure the eclass ids contained in the enode are all
    ;; cannonical eclass ids
    :unless (eq new-enode parent-enode)
      :do (remhash parent-enode (enode-eclasses egraph))
    :do
       ;; Make sure the enode points to a cannonical eclass id
       (setf (enode-eclass-id egraph new-enode)
             (eclass-find egraph parent-eclass-id))))

;; N.B. 2023-11-05 this is the part I understand less. I _think_ it's
;; for restoring the "transitivity" invariant.
(defmethod repair-transitivity (egraph eclass)
  (loop
    :with new-parent-enodes = (make-enode-map)
    ;; :for (parent-enode . parent-eclass-id) :across (parents eclass)
    :for parent-enode
      :being :the :hash-key :of (parents eclass)
        :using (hash-value parent-eclass-id)
    :for new-enode = (cannonicalize egraph parent-enode)
    :for equivalent-eclass-id = (gethash new-enode new-parent-enodes)
    :do
       (when equivalent-eclass-id
         (merge-eclass egraph
                       parent-eclass-id
                       equivalent-eclass-id))
       (setf (gethash new-enode new-parent-enodes)
             (eclass-find egraph parent-eclass-id))
    :finally (setf (parents eclass) new-parent-enodes)))

(defmethod repair (egraph eclass-id
                   &aux (eclass (egraph-eclass egraph eclass-id)))
  (repair-parent-enodes egraph eclass)
  (repair-transitivity egraph eclass))

(defun to-set (sequence)
  (let ((set (make-hash-table)))
    (map nil (lambda (element)
               (setf (gethash element set) element))
         sequence)
    set))

(defmethod rebuild (egraph)
  (loop
    :while (pending egraph)
    :do
       (let ((todo (to-set (mapcar (lambda (eclass-id)
                                     (eclass-find egraph eclass-id))
                                   (pending egraph)))))
         (setf (pending egraph) nil)
         (loop
           :for eclass-id :being :the :hash-key :of todo
           :do (repair egraph eclass-id)))))


;;; TODO e-matching

(defmethod %root-eclasses (egraph)
  (loop
    :for eclass-id :being
      :the :hash-key :of (eclasses egraph)
        :using (hash-value eclass)
    :when (zerop (hash-table-count (parents eclass)))
      :collect eclass))

(defmethod root-eclasses (egraph)
  (let* ((roots (%root-eclasses egraph))
         (root-ids (to-set (mapcar #'id roots))))
    (loop
      :for eclass-id :being
        :the :hash-key :of (eclasses egraph)
          :using (hash-value eclass)
      :for cannonical-id = (eclass-find egraph eclass-id)
      :when (gethash cannonical-id root-ids)
        :collect eclass)))

(defun smallest-enodes (eclasses)
  (let* ((all-nodes (sort (apply #'concatenate 'vector
                                 (mapcar #'enodes eclasses))
                          #'catchall.egraph.tests::enode<))
         (shortest (aref all-nodes 0)))
    (remove-if (lambda (enode)
                 (catchall.egraph.tests::enode< shortest enode))
               all-nodes)))
