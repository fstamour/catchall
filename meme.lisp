
;;; mapca[ad]{1,4}r

(defun mapcaar (&rest lists)
  (apply #'mapcar #'car lists))

;; TODO eval-when
(defun int->ad (x n)
  (coerce
   (loop :for i :below n
         :collect (if (zerop (logand (ash 1 i) x))
                      #\A
                      #\D))
   'string))

;; (int->ad 3 4)
;; => "DDAA"

;; TODO eval-when
(defun gen-ad+ ()
  (loop :for n :from 1 :upto 4
        :append
        (loop :for i :below (expt 2 n)
              :collect (int->ad i n))))

(gen-ad+)
'("A" "D" "AA" "DA" "AD" "DD"
  "AAA" "DAA" "ADA" "DDA" "AAD" "DAD" "ADD" "DDD"
  "AAAA" "DAAA" "ADAA" "DDAA" "AADA" "DADA" "ADDA" "DDDA"
  "AAAD" "DAAD" "ADAD" "DDAD" "AADD" "DADD" "ADDD" "DDDD")


(macrolet ((mapca[ad]+r ()
             `(progn
                ,@(loop
                    :for ad :in (gen-ad+)
                    :collect
                    `(defun ,(intern (format nil "MAPCA~AR" AD))
                         (&rest lists)
                       (apply #'mapcar
                              #',(intern (format nil "C~AR" AD))
                              lists))))))
  (mapca[ad]+r))


(let ((name (symbol-name 'mapcaar)))
  (format nil "C~AR"
          (subseq name #. (length "MAPCA")
                  (1- (length name)))))


;;; A.k.A. pOkEmOn CaSe

(defun alt-case1 (string &optional invertp)
  (let ((i 0))
    (map 'string
         #'(lambda (char)
             (prog1
                 (if (if invertp (evenp i) (oddp i))
                     (char-upcase char)
                     (char-downcase char))
               (incf i)))
         string)))


(defun alt-case2 (string &optional invertp)
  (let ((flag invertp))
    (map 'string
         #'(lambda (char)
             (prog1
                 (if flag
                     (char-upcase char)
                     (char-downcase char))
               (setf flag (not flag))))
         string)))

(defun alt-case3 (input &aux (input (copy-seq input)))
  (prog ((i 0))
   odd
     (setf (char input i) (char-upcase (char input i)))
     (incf i)
     (if (>= i (length input))
         (go end)
         (go even))
   even
     (setf (char input i) (char-downcase (char input i)))
     (incf i)
     (if (>= i (length input))
         (go end)
         (go odd))
   end
     (return input)))

(alt-case3 "this was worth it")
;; => "ThIs wAs wOrTh iT"



;; https://discord.com/channels/297478281278652417/297478350145060875/1217487872723124224
(defun cutify (string)
  "Switch every character in string into U or W, depending
on whether or not it has an odd index.

Hello, world! => Uwuwu, wuwuw!"
  (let ((j 0))
    (map 'string
         (lambda (i)
           (incf j)
           (cond ((not (alpha-char-p i)) i)
                 ((upper-case-p i) (if (oddp j) #\U #\W))
                 ((lower-case-p i) (if (oddp j) #\u #\w))))
         string)))

(cutify "Hello, world!")
;; => "Uwuwu, wuwuw!"


;;; A quine

#1=(let ((*print-circle* t))
     (prin1-to-string '#1#))




(deftype thiccint (integer most-positive-fixnum))



(defpackage :💩 (:use :cl))

(defpackage | | (:use :cl))

(in-package | |)

(defun non-breaking-space ()
  (package-name #.*package*))

(non-breaking-space)

;; https://discord.com/channels/297478281278652417/297478350145060875/1219989581286608916
(defparameter 🎧🧦🎧 "remember-your-earmuffs")



(do-symbols (sym :alexandria) (import sym))



;; macroexpansion as a service
(defmacro my-macro (&whole whole)
  (let* ((expansion-str (http-request "maas.com/expand" (write-to-string whole)))
         (expansion (read-from-string expansion-str)))
    expansion))



(defmacro my/and (&rest forms)
  (if (every #'identity forms)
      (let ((block-sym (gensym "block"))
            (ht-sym (gensym "ht"))
            (result-sym (gensym "rez"))
            (butlast (butlast forms))
            (last (car (last forms))))
        `(block ,block-sym
           (let ((,ht-sym (make-hash-table)))
             (setf (gethash nil ,ht-sym) #'(lambda () (return-from ,block-sym nil)))
             ,@(mapcar (lambda (form)
                         `(let ((,result-sym ,form))
                            (setf (gethash ,result-sym ,ht-sym) (lambda ()))
                            (funcall (gethash ,result-sym ,ht-sym))))
                       butlast))
           (return-from ,block-sym ,last)))
      nil))

(my/and 1 2 nil)

(my/and 1 2 3)

(my/and 1 2 (null 3))


;;; Using &aux to redefine the current function

(defun f (x &aux (r (defun f () x))))
(f 2) ; => nil
(f) ; => 2
(f 3) ; => invalid number of arguments: 1

(defun f (&aux (x 0) (r (defun f () (incf x)))) x)



(defun aux (a b &aux (&aux (cons a b)))
  (setf (car &aux) (* (car &aux) (car &aux))
        (cdr &aux) (* (cdr &aux) (cdr &aux))
        &aux (+ (car &aux) (cdr &aux))
        &aux (sqrt &aux)))

(defun aux (aux &aux
                  (&aux aux)
                  (&aux (cons (* (car &aux) (car &aux))
                              (* (cdr &aux) (cdr &aux))))
                  (&aux (+ (car &aux) (cdr &aux)))
                  (&aux (sqrt &aux)))
  &aux)

(aux (cons 3 4))
