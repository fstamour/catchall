
(defmacro with-overwrite-file ((var file) &body body)
  `(with-open-file (,var ,file :direction :output
                               :if-exists :supersede
                               :if-does-not-exist :create)
     ,@body))
