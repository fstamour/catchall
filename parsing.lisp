(defpackage #:parsing
  (:documentation "Parsing stuff...")
  (:use #:cl))

(in-package #:parsing)

(defclass parser-state ()
  ((index
    :initform 0
    :accessor parser-index
    :documentation "Where the parser is at in the input string")
   (input
    :type string
    :initarg :input
    :accessor parser-input
    :documentation "The string being parsed."))
  (:documentation
   "Class that keeps track of everything used for parsing."))

(defun make-parser-state (input)
  "Create a starting parser state."
  (make-instance 'parser-state
                 :input input))

(defparameter *parser-state* nil
  "Special variable, so we can implicitely pass the parser's state
to every functions.")

(defun parser-input* ()
  (parser-input *parser-state*))

(defun parser-index* ()
  (parser-index *parser-state*))

(defun parser-incf (&optional (n 1))
  (incf (parser-index *parser-state*) n))

(defun parser-char* ()
  (char (parser-input*) (parser-index*)))

(defun (setf parser-index*) (new-value)
  (setf (parser-index *parser-state*) new-value))

(defun parse-directive ()
  "Parse the directive (currently it's defined as the first 2
characters of the string."
  (let* ((directive (subseq (parser-input*)
                            0
                            (min 2 (length (parser-input*))))))
    (prog1 (cond
             ((string= "||" directive) :vertical)
             ((string= "==" directive) :horizontal)
             (t (error "Invalid directive: ~s" directive)))
      (parser-incf 2))))

(defun parse-range ()
  (declare (optimize (debug 3) (safety 3) (speed 0)))
  (when (digit-char-p (parser-char*))
    (multiple-value-bind (integer0 index)
        (parse-integer (parser-input*)
                       :start (parser-index*)
                       :junk-allowed t)
      (multiple-value-bind (integer1 index)
          (parse-integer (parser-input*)
                         ;; Skip the ","
                         :start (1+ index)
                         :junk-allowed t)
        (setf (parser-index*) index)
        (cons integer0 integer1)))))

(defun take-while (predicate)
  (let ((from (parser-index*))
        (to (position-if-not
             predicate
             (parser-input*)
             :start (parser-index*))))
    (setf (parser-index*) to)
    (subseq (parser-input*) from to)))

(defun take-until (predicate)
  (take-while (complement predicate)))

(let ((input "||1234,4321stuff"))
  (let* ((*parser-state* (make-parser-state input))
         (directive  (parse-directive))
         (range (parse-range)))
    (list directive range (subseq input (parser-index*)))))
;; (:VERTICAL (1234 . 4321) "stuff")

(defmacro with-input ((string) &body body)
  `(let ((*parser-state* (make-parser-state ,string)))
     ,@body))

(with-input ("asdf1234qwer")
  (list
   (take-while #'alpha-char-p)
   (take-while #'digit-char-p)
   (take-while #'alpha-char-p)))
;; => ("asdf" "1234" "qwer")



(let ((input "bvvc255abc"))
  (let ((state 'start)
        (buffer ())
        (length (length input))
        (index 0)
        (results))
    (labels ((finish-buffer ()
               (push (coerce (nreverse buffer) 'string) results)
               (setf buffer nil))
             (push-char ()
               (push (char input index) buffer)
               (incf index))
             (unexpected-character ()
               (error "Unexpected character \"~c\"."
                      (char input index))))
      (loop
        :while (< index length)
        :for guard :below 1000 ; to avoid inifite loop during development
        :for char = (char input index)
        :for alphap = (alpha-char-p char)
        :for digitp = (digit-char-p char)
        :do
           (format t "~&~a ~a ~a" state char results)
           (case state
             (start (cond
                      (alphap
                       (push-char)
                       (setf state 'alpha))
                      (digitp
                       (push-char)
                       (setf state 'digit))
                      (t (unexpected-character))))
             (alpha (cond
                      (alphap
                       (push-char))
                      (digitp
                       (finish-buffer)
                       (setf state 'digit))
                      (t (unexpected-character))))
             (digit (cond
                      (digitp
                       (push-char))
                      (alphap
                       (finish-buffer)
                       (setf (car results)
                             (parse-integer (car results)))
                       (setf state 'alpha))
                      (t (unexpected-character))))))
      (finish-buffer)
      (setf results (nreverse results))
      results)))
;; => ("bvvc" 255 "abc")







(defun take-while (stream predicate)
  (with-output-to-string (output)
    (loop :for c = (peek-char nil stream nil nil)
          :while (and c (funcall predicate c))
          :do (write-char (read-char stream) output))))

(defun read-integer (stream &optional (radix 10))
  ;; Skip whitespaces
  (peek-char t stream)
  (let ((string (take-while stream
                            #'(lambda (char)
                                (digit-char-p char radix)))))
    ;; Maybe add a check that string != ""
    (parse-integer string :radix radix)))

(with-input-from-string (stream "123 456 789 deadbeef")
  (values
   (read-integer stream)
   (read-integer stream)
   (read-integer stream)
   (read-integer stream 16)))
;; => 123, 456, 789, 3735928559

(defun read-integer* (&optional (stream *standard-input*) (radix 10))
  (loop :with int = 0
        :for char = (read-char stream nil nil)
        :while char
        :for num = (digit-char-p char radix)
        :while num
        :do (setf int (+ (* int radix) num))
        :finally (return int)))

(with-input-from-string (stream "123 456 789 deadbeef")
  (values
   (read-integer* stream)
   (read-integer* stream)
   (read-integer* stream)
   (read-integer* stream 16)))



;;; Trees!

(defstruct binary-tree
  (value)
  (left)
  (right))

(make-binary-tree :value 42)
;; =>  #S(BINARY-TREE :VALUE 42 :LEFT NIL :RIGHT NIL)


(defparameter *t*
  #s(binary-tree
     :value 1
     :left #s(binary-tree :value 2)
     :right #s(binary-tree :value 3)))

(binary-tree-p *t*)
;; => T
(binary-tree-value *t*)
;; => 1
(binary-tree-left *t*)
;; => #S(BINARY-TREE :VALUE 2 :LEFT NIL :RIGHT NIL)
(binary-tree-right *t*)
;; => #S(BINARY-TREE :VALUE 3 :LEFT NIL :RIGHT NIL)

(defun dig (tree path)
  (loop
    :with current = tree
    :for leftp :in path
    :while current
    :do
       (setf current (if leftp
                         (binary-tree-left current)
                         (binary-tree-right current)))
    :finally (return current)))

;; Insert a new tree
(setf (binary-tree-left (binary-tree-right *t*))
      #s(binary-tree :value 4))

(dig *t* '(nil t))
;; => #S(BINARY-TREE :VALUE 4 :LEFT NIL :RIGHT NIL)


;; Using a stack to turn a list into a tree

(loop
  :with stack = (list (list))
  :for sym :in '(|(| abc 1 2 |(| foo 3 4 |)| |(| bar 5 6 |)| |)|)
  :do
     ;; (format t "~&stack: ~a~&sym: %a" stack sym)
     (cond
       ((eq '|(| sym)
        (push (list) stack))
       ((eq '|)| sym)
        (push (nreverse (pop stack)) (car stack)))
       (t (push sym (car stack))))
  :finally (return (car stack)))
((ABC 1 2 (FOO 3 4) (BAR 5 6)))
