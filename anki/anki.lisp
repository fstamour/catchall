(in-package #:common-lisp-user)

(defpackage #:anki
  (:use :cl))

(in-package #:anki)

(ql:quickload '(dexador jonathan))

;; Dexador - HTTP client
;;   repo: https://github.com/fukamachi/dexador
;; Jonathan - JSON library
;;   doc: http://rudolph-miller.github.io/jonathan/overview.html
;;  repo: https://github.com/Rudolph-Miller/jonathan

(defparameter *anki-port* 8765)
(defparameter *anki-host* "127.0.0.1")
(defparameter *anki-connect-version* "6")

#|

https://foosoft.net/projects/anki-connect/

Every request consists of a JSON-encoded object containing an action,
a version, contextual params, and a key value used for
authentication (which is optional and can be omitted by default).

AnkiConnect will respond with an object containing two fields:
result and error.

The result field contains the return value of the
executed API, and the error field is a description of any exception
thrown during API execution (the value null is used if execution
completed successfully).

|#

(defun anki-url ()
  (format nil "http://~A:~A/"
          *anki-host* *anki-port*))

(defun request-body (action)
  (jojo:to-json
   `(("action" . ,action)
     ("version" . ,*anki-connect-version*))
   :from :alist))

(request-body "deckNames")
"{\"action\":\"deckNames\",\"version\":\"6\"}"

;; Get decks
(jojo:parse
 (dex:post (anki-url)
           :content (request-body "deckNames")))
