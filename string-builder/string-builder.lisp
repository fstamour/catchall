(ql:quickload '(#:random-state
                #:trivial-benchmark))

;; http://shinmera.github.io/random-state/
;; http://shinmera.github.io/trivial-benchmark/

(defpackage catchall.string-builder
  (:documentation "How could we create strings faster and more flexibly than w-o-t-s")
  (:use #:cl)
  (:import-from #:trivial-benchmark
                #:with-timing))

(in-package #:catchall.string-builder)

#|

The problem(s):

- When using with-output-to-string, each characters are pushed one by
one (need to check the actual impact on performance).

- When using make-string-output-stream, the get-output-stream-string
clears the stream

- with-output-to-string can take an existing string to start
from (e.g. to modify in-place)

|#

(defun random-string (gen)
  (with-output-to-string (*standard-output*)
    (let ((alphabet "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"))
      (loop :for i :below (random-state:random-int gen 0 50)
            :do (write-char (aref alphabet (random-state:random-int gen 0 (1- (length alphabet)))))))))

(defun random-string-list (n seed)
  (let ((gen (random-state:make-generator :mersenne-twister-32 seed)))
    (loop :for i :below n :collect (random-string gen))))


(defparameter *strings* (random-string-list 10000 42))


;;; Utilities for getting information out

(defun disassemble-to-file (function &optional filename)
  (with-open-file (*standard-output*
                   (or filename
                       (format nil "disassembly_~a.txt" function))
                   :direction :output
                   :if-exists :overwrite
                   :if-does-not-exist :create)
    (format t "The dissasembly of ~S:~%" function)
    (disassemble function)))

;; TODO something similar to get the benchmarking data to something
;; more amenable to analysis (like a csv)


;;; Write-string + pre-allocated (empty) array

(with-timing (1000)
  (let ((s (make-array 0 :element-type 'character :fill-pointer 0 :adjustable t)))
    (with-output-to-string (stream s)
      (loop for string in *strings*
            do (write-string string stream)))))
#|
-                SAMPLES  TOTAL       MINIMUM   MAXIMUM   MEDIAN    AVERAGE    DEVIATION
REAL-TIME        1000     1.681433    0.001527  0.007366  0.001642  0.001681   0.000309
RUN-TIME         1000     1.703125    0         0.015625  0         0.001703   0.004869
USER-RUN-TIME    1000     1.671875    0         0.015625  0         0.001672   0.00483
SYSTEM-RUN-TIME  1000     0.03125     0         0.015625  0         0.000031   0.000698
PAGE-FAULTS      1000     0           0         0         0         0          0.0
GC-RUN-TIME      1000     15.625      0         15.625    0         0.015625   0.493859
BYTES-CONSED     1000     2827437872  2675792   2827712   2827616   2827437.8  4836.0854
EVAL-CALLS       1000     0           0         0         0         0          0.0
NIL
|#


;;; Write-string + pre-allocated (right size) array

(defun with-output-to-string--preallocated ()
  (let ((s (make-array (reduce #'+ (mapcar #'length *strings*))
                       :element-type 'character
                       :fill-pointer 0 :adjustable t)))
    (with-output-to-string (stream s)
      (loop for string in *strings*
            do (write-string string stream)))))

(disassemble-to-file 'with-output-to-string--preallocated)

(with-timing (1000)
  (with-output-to-string/preallocated))

#|
-                SAMPLES  TOTAL       MINIMUM   MAXIMUM   MEDIAN   AVERAGE    DEVIATION
REAL-TIME        1000     0.984125    0.000887  0.008274  0.00095  0.000984   0.000252
RUN-TIME         1000     1           0         0.015625  0        0.001      0.003824
USER-RUN-TIME    1000     0.984375    0         0.015625  0        0.000984   0.003796
SYSTEM-RUN-TIME  1000     0.03125     0         0.015625  0        0.000031   0.000698
PAGE-FAULTS      1000     0           0         0         0        0          0.0
GC-RUN-TIME      1000     0           0         0         0        0          0.0
BYTES-CONSED     1000     1165587376  1126272   1202192   1136560  1165587.4  32561.186
EVAL-CALLS       1000     0           0         0         0        0          0.0
NIL
|#



;;; Write-string, array allocated by with-output-to-string
(with-timing (1000)
  (with-output-to-string (stream)
    (loop :for string :in *strings*
          :do (write-string string stream))))

#|
-                SAMPLES  TOTAL       MINIMUM   MAXIMUM   MEDIAN    AVERAGE    DEVIATION
REAL-TIME        1000     1.145028    0.001016  0.007611  0.001111  0.001145   0.000316
RUN-TIME         1000     1.140625    0         0.015625  0         0.001141   0.004065
USER-RUN-TIME    1000     1.109375    0         0.015625  0         0.001109   0.004013
SYSTEM-RUN-TIME  1000     0.03125     0         0.015625  0         0.000031   0.000698
PAGE-FAULTS      1000     0           0         0         0         0          0.0
GC-RUN-TIME      1000     0           0         0         0         0          0.0
BYTES-CONSED     1000     2305718592  2176688   2305968   2305904   2305718.5  4230.544
EVAL-CALLS       1000     0           0         0         0         0          0.0
NIL
|#


;;; Write-string, special variable (*standard-output*)

(with-timing (1000)
  (with-output-to-string (*standard-output*)
    (loop :for string :in *strings*
          :do (write-string string))))


#|
-                SAMPLES  TOTAL       MINIMUM  MAXIMUM   MEDIAN    AVERAGE    DEVIATION
REAL-TIME        1000     1.16367     0.00102  0.00771   0.001141  0.001164   0.000322
RUN-TIME         1000     1.1875      0        0.015625  0         0.001188   0.004141
USER-RUN-TIME    1000     1.171875    0        0.015625  0         0.001172   0.004115
SYSTEM-RUN-TIME  1000     0           0        0         0         0          0.0
PAGE-FAULTS      1000     0           0        0         0         0          0.0
GC-RUN-TIME      1000     31.25       0        15.625    0         0.03125    0.698072
BYTES-CONSED     1000     2305283792  2143408  2305968   2305904   2305283.8  9884.648
EVAL-CALLS       1000     0           0        0         0         0          0.0
NIL
|#


;;; Print, special variable (*standard-output*)

(with-timing (1000)
  (with-output-to-string (*standard-output*)
    (loop :for string :in *strings*
          :do (print string))))

#|
-                SAMPLES  TOTAL       MINIMUM  MAXIMUM   MEDIAN    AVERAGE    DEVIATION
REAL-TIME        1000     4.135992    0.00328  0.011623  0.003567  0.004136   0.001265
RUN-TIME         1000     4.125       0        0.015625  0         0.004125   0.006887
USER-RUN-TIME    1000     3.625       0        0.015625  0         0.003625   0.006595
SYSTEM-RUN-TIME  1000     0.5         0        0.015625  0         0.0005     0.00275
PAGE-FAULTS      1000     0           0        0         0         0          0.0
GC-RUN-TIME      1000     31.25       0        15.625    0         0.03125    0.698072
BYTES-CONSED     1000     3196467664  2943552  3212240   3197472   3196467.8  15500.009
EVAL-CALLS       1000     0           0        0         0         0          0.0
|#


;;; Apply + concatenate

(defun apply-concatenate* ()
  (apply #'concatenate 'string *strings*))

(with-timing (1000)
  (apply #'concatenate 'string *strings*))

#|
-                SAMPLES  TOTAL       MINIMUM   MAXIMUM   MEDIAN    AVERAGE   DEVIATION
REAL-TIME        1000     0.474121    0.000374  0.008867  0.000405  0.000474  0.000353
RUN-TIME         1000     0.5         0         0.015625  0         0.0005    0.00275
USER-RUN-TIME    1000     0.453125    0         0.015625  0         0.000453  0.002622
SYSTEM-RUN-TIME  1000     0.046875    0         0.015625  0         0.000047  0.000855
PAGE-FAULTS      1000     0           0         0         0         0         0.0
GC-RUN-TIME      1000     15.625      0         15.625    0         0.015625  0.493859
BYTES-CONSED     1000     1005488000  1005488   1005488   1005488   1005488   0.0
EVAL-CALLS       1000     0           0         0         0         0         0.0
|#








;; TODO unicode
;; TODO different distributions of size of strings
;; TODO different scales (e.g. for 10 strings, for 1000 strings for 100,000 strings)
;; TODO include the construction of the list of strings
;; TODO Test format + string with a fill-pointer


