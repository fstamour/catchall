(ql:quickload '(alexandria))

;; make it easier to debug
(declaim (optimize (speed 0) (safety 3) (debug 3)))

#|

Goal: Try out the wheel factorization algorithm

Motivation: because I love prime number, for some reason...

What am I going to try first:
1. find the lcm of a list of numbers
2. generate the wheel from a basis
3. generate numbers from the wheels
4. check for primality using those numbers
5. couple a wheel and a bloom filter! or try to :P

|#


;; Find the lcm, easy...
(apply #'lcm '(2 3 5))

;; Generating the wheel (inefficiently, but "debuggably")
(let* ((basis '(2 3 5))
       (lcm (apply #'lcm basis))
       (wheel (make-array (1+ lcm))))
  (loop :for i :in basis :do
    (loop :for j :from 1 :upto (1+ lcm)
          :for multiple = (* i j)
          :while (<= multiple (1+ lcm))
          :do (setf (aref wheel (1- multiple)) multiple)))
  (values wheel
          (loop :for i :from 1
                :for n :across wheel
                :when (and (zerop n)
                           (/= 1 i))
                  :collect i)))

;; N.B. Technically "1" should be part of the wheel, but removing it
;; and adding 1 + lcm is equivalent
