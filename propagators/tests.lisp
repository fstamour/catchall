
(in-package #:catchall.propagators)

(define-test+run nothingp
  (false (nothingp nil))
  (true (nothingp t)))

;; Welp... I used it only once 🙃
(defmacro with-trace (() &body body)
  `(let ((trace))
     (flet ((add-trace (cell)
              (push
               ;; TODO would be nice to _copy_ those
               (list cell
                     (content cell)
                     (length (listeners cell)))
               trace))
            (done-tracing () (setf trace (nreverse trace))))
       ,@body)))

(define-test+run cell
  (true (make-cell))
  (true (make-cell 'a))
  (is eq 'a (name (make-cell 'a)))
  (is string-equal
      "#<CELL T (0 listeners)>"
      (format nil "~s" (make-cell)))
  (is string-equal
      "#<CELL A T (0 listeners)>"
      (format nil "~s" (make-cell 'a)))
  ;; TODO test (content cell)
  ;; a listener must be called when added
  (let ((calledp nil)
        (c (make-cell)))
    (false calledp)
    (add-listener c (lambda (cell)
                      (declare (ignore cell))
                      (setf calledp t)))
    (true calledp))
  ;; testing the listeners & content with 2 cells
  (with-trace ()
    (let* ((a (make-cell 'a))
           (b (make-cell 'b))
           (a+1 (lambda (input-cell)
                  (unless (nothingp input-cell)
                    (add-content b (1+ (content input-cell)))))))
      (add-listener a #'add-trace)
      (true (nothingp a))
      (add-listener a a+1)
      (true (nothingp b))
      (add-listener b #'add-trace)
      (add-content a 41)
      (is = 41 (content a))
      (is = 42 (content b))
      (done-tracing)
      ;; check the trace (calls to listeners)
      (is equalp `(,a t 1) (first trace))
      (is equalp `(,b t 1) (second trace))
      (is equalp `(,a 41 2) (third trace))
      (is equalp `(,b 42 1) (fourth trace))))
  (flet ((bidi ()
           (let ((a (make-cell 'a))
                 (b (make-cell 'b)))
             (add-listener a (lambda (cell)
                               (unless (nothingp cell)
                                 (add-content b (+ 2 (content cell))))))
             (add-listener b (lambda (cell)
                               (unless (nothingp cell)
                                 (add-content a (- (content cell) 2)))))
             (values a b))))
    (multiple-value-bind (a b) (bidi)
      (true (nothingp a))
      (true (nothingp b))
      (is = 1 (length (listeners a)))
      (is = 1 (length (listeners b))))
    (multiple-value-bind (a b) (bidi)
      (add-content a 5)
      (is = 7 (content b)))
    (multiple-value-bind (a b) (bidi)
      (add-content b 5)
      (is = 3 (content a))))
  ;; same as previous test, but using propagator objects
  (flet ((bidi ()
           (let* ((a (make-cell 'a))
                  (b (make-cell 'b))
                  (a->b (make-propagator :inputs (list a) :output b
                                         :fn (lambda (x) (+ x 2))))
                  (b->a (make-propagator :inputs (list b) :output a
                                         :fn (lambda (x) (- x 2)))))
             (add-listener a a->b)
             (add-listener b b->a)
             (values a b))))
    (multiple-value-bind (a b) (bidi)
      (true (nothingp a))
      (true (nothingp b))
      (is = 1 (length (listeners a)))
      (is = 1 (length (listeners b))))
    (multiple-value-bind (a b) (bidi)
      (add-content a 5)
      (is = 7 (content b)))
    (multiple-value-bind (a b) (bidi)
      (add-content b 5)
      (is = 3 (content a))))
  ;; same as previous, but using "builders"
  (flet ((bidi ()
           (let* ((a (define-cell 'a))
                  (b (define-cell 'b)))
             (c/sum2 b a 2)
             (values a b))))
    (multiple-value-bind (a b) (bidi)
      (true (nothingp a))
      (true (nothingp b))
      (is = 1 (length (listeners a)))
      (is = 1 (length (listeners b))))
    (multiple-value-bind (a b) (bidi)
      (add-content a 5)
      (is = 7 (content b)))
    (multiple-value-bind (a b) (bidi)
      (add-content b 5)
      (is = 3 (content a)))))

(define-test+run propagator
  (true (make-propagator))
  (flet ((sum ()
           (let ((a (define-cell 'a))
                 (b (define-cell 'b))
                 (c (define-cell 'c)))
             (c/sum c a b)              ; constraint: c = a + b
             (values a b c))))
    (multiple-value-bind (a b c) (sum)
      (add-content a 40)
      (add-content b 2)
      (is = 42 (content c)))
    (multiple-value-bind (a b c) (sum)
      (add-content c 40)
      (add-content b 2)
      (is = 38 (content a)))
    (multiple-value-bind (a b c) (sum)
      (add-content c 40)
      (add-content a 2)
      (is = 38 (content b)))))

;; TODO network...

(define-test+run derive
  (with-network ((make-instance 'network))
    (let ((a (define-cell 'a))
          (b (define-cell 'b))
          (c (define-cell 'c)))
      (c/sum c a b) ; c = a + b
      (handler-bind
          ((request (lambda (request &aux (cell (cell request)))
                      (case (name cell)
                        (c ;; do nothing
                         nil)
                        (a (add-content a 40) t)
                        (b (add-content b 2) t)))))
        (is equal (list 42) (derive *network* c))))))


;;; Combine, contradictoryp & equivalentp

(define-test+run contradiction
  ;; Not handling the signaled contradiction furthers signals an
  ;; error.
  (fail (contradiction 'a 'b))
  ;; Handling the signaled contradiction does not cause an error to be
  ;; signaled.
  (is eq 'handled
      (handler-case
          (contradiction 'a 'b)
        (contradiction (condition)
          'handled))))

;; TODO notify
;; TODO add-content

(define-test+run equivalentp
  ;; TODO rationals, floats, strings, arrays... etc
  (progn
    (is equivalentp nil nil)
    (is equivalentp t t)
    (is equivalentp 0 0)
    (is equivalentp 1 1)
    (is equivalentp 'a 'a)
    (is equivalentp '(a) '(a))
    (is equivalentp
        (make-contingent 'a '(premise1))
        (make-contingent 'a '(premise1))))
  (progn
    (isnt equivalentp nil t)
    (isnt equivalentp t 'a)
    (isnt equivalentp 0 1)
    (isnt equivalentp -1 1)
    (isnt equivalentp 'a 'b)
    (isnt equivalentp '(a) '())
    (isnt equivalentp '() '(a))
    (isnt equivalentp '(a) '(a b))
    (isnt equivalentp
          (make-contingent 'a '(premise1))
          (make-contingent 'a '(premise1 premise2)))
    (isnt equivalentp
          (make-contingent 'a nil)
          (make-contingent 'b nil))
    (isnt equivalentp
          (make-contingent 'a '(premise1))
          (make-contingent 'b '(premise1)))))

(define-test+run contradictoryp
  ;; Special case: t is never contradictoryp
  (progn
    (isnt contradictoryp t t)
    (isnt contradictoryp nil t)
    (isnt contradictoryp t 'a)
    (isnt contradictoryp 'a t))
  (is contradictoryp
      (make-contingent 'a)
      (make-contingent 'b))
  (isnt contradictoryp
        (make-contingent 'a)
        (make-contingent 'a))
  (isnt contradictoryp
        (make-contingent 'a '(premise 1))
        (make-contingent 'a '(premise 1)))
  (isnt contradictoryp
        (make-contingent 'a '(premise1))
        (make-contingent 'a '(premise2)))
  (isnt contradictoryp
        (make-contingent t '(premise1))
        (make-contingent 'a '(premise2)))
  ;; TODO More special cases? (e.g. intervals, TMSes)
  ;; Everything else is, because we assume that the arguments are
  ;; already not equivalentp.
  (progn
    (is contradictoryp nil nil)
    (is contradictoryp 0 0)
    (is contradictoryp 1 1)
    (is contradictoryp 'a 'a)
    (is contradictoryp '(a) '(a)))
  (progn
    (is contradictoryp 0 1)
    (is contradictoryp -1 1)
    (is contradictoryp 'a 'b)
    (is contradictoryp '(a) '())
    (is contradictoryp '() '(a))
    (is contradictoryp '(a) '(a b))))


(define-test+run combine
  (progn
    (is eq t (combine t t))
    (is eq nil (combine nil t))
    (is eq 'a (combine t 'a))
    (is eq 'a (combine 'a t)))
  (flet ((test-equivalents (x y)
           (unless (equivalentp x y)
             (error
              "This test requires ~s and ~s to be equivalentp."
              x y))
           (is eq x (combine x y))))
    (test-equivalents nil nil)
    (test-equivalents t t)
    (test-equivalents 'a 'a)
    (test-equivalents 0 0)
    (test-equivalents 1 1)
    (test-equivalents '(a) '(a)))
  (flet ((test-combine (x y expected)
           (when (equivalentp x y)
             (error
              "This test requires ~s and ~s to NOT be equivalentp."
              x y))
           (when (contradictoryp x y)
             (error
              "This test requires ~s and ~s to NOT be contradictoryp."
              x y))
           (let ((result (combine x y)))
             (case expected
               ((t)
                (is eq x result
                    "Expected the result of combine to be the first argument (~s)." x))
               ((nil)
                (is eq y result
                    "Expected the result of combine to be the second argument (~s)." y))
               (otherwise
                (is equivalentp expected result
                    "Expected (combine ~s ~s) to return ~s."
                    x y expected))))))
    (test-combine (make-contingent 'a '(premise1))
                  (make-contingent 'a '(premise1 premise2))
                  t)
    (test-combine (make-contingent 'a '(premise1 premise2))
                  (make-contingent 'a '(premise1))
                  nil)
    (test-combine (make-contingent t '(premise1 premise2))
                  (make-contingent 'a '(premise3))
                  (make-contingent 'a '(premise1 premise2 premise3)))
    ;; TODO I need to test combining contingents that have partial
    ;; information objects as content.
    #++
    (test-combine (make-contingent TODO '(premise1 premise2))
                  (make-contingent TODO '(premise3))
                  (make-contingent TODO '(premise1 premise2 premise3)))))



(defun test-set-subset-p (set1 set2 expected &optional strict)
  (is eq expected (set-subset-p
                   (ensure-set set1)
                   (ensure-set set2)
                   :strict strict)))

(define-test+run "ensure-set and set-subset-p"
  (progn
    (test-set-subset-p '(a b) '(a b c) t)
    (test-set-subset-p '(a b) '(a b) t)
    (test-set-subset-p '(a b c) '(a b) nil)
    (test-set-subset-p nil nil t))
  (progn
    (test-set-subset-p '(a b) '(a b c) t t)
    (test-set-subset-p '(a b) '(a b) nil t)
    (test-set-subset-p '(a b c) '(a b) nil t)
    (test-set-subset-p nil nil nil t)))
