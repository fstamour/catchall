#++
(ql:quickload '(alexandria serapeum parachute))

;; make it easier to debug
(declaim (optimize (speed 0) (safety 3) (debug 3)))

#|

Goal: Implement a propagators-like system

Motivation: There are multiple ideas that I found interesting, like:
- "append-only" information refinements (they call it "monotonicity")
- how TMS are "distributed" - how unification is generalized
to "refinements" (my words, not theirs).
- I want to implement some of it in common lisp to get a better
understanding

Another, ulterior motivation: I think I can see a way of using some of
this for some kind of declarative UI thingy.

What am I going to try first: Start with basic Cells and propagators
and the "proper" generic methods.

Try to document the design decisions plz

|#


#|

Design decisions:


In the papers, a singleton value is used to represent the absence of
information. I chose to use the symbol T instead. It
represents "anything". I think it'll work out...

In the papers, cells are composed of "content" and "neighbors", I
prefer the term "listeners" over "neighbors"


In the papers, propagators closes over their inputs, I'm not sure if I
want that because it feels like it would restrain how the whole
network can be build. It's also harder to debug... and to mutate,
which might not be something we want. We'll see... the whole point of
writing this code is to figure out those details.

As I was writing down the methods and their arguments, I started to
feel that there should be 2 abstractions: listeners and
propagators. The listeners would be the ***functions*** (thunks
probably... altough I think that passing the cell as an argument might
be a more flexible design...) that are called unconditionally when a
cell's content changes. Whereas the propagators would be ***objects***
that has a little more smarts to them, like only calling a function
when all the input cells have content, maybe even keep typing
information...

|#

(defpackage #:catchall.propagators
  (:documentation "")
  (:use #:cl)
  (:import-from #:parachute
                #:define-test
                #:define-test+run
                #:is
                #:isnt
                #:true
                #:false
                #:fail)
  (:import-from #:serapeum
                #:vect))

(in-package #:catchall.propagators)


;;; Cell

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defclass cell ()
    ((content
      :initform t
      :initarg :content
      :accessor content)
     (name
      :initform nil
      :initarg :name
      :accessor name
      :documentation "Optional name.")
     ;; TODO I feel like this could be stored in the "network" only.
     (listeners
      :initform (vect)
      :accessor listeners
      :documentation "List of functions to call when the content changes."))
    ;; Yes, that's generic af
    (:documentation "A class used to store information...")))

(defun make-cell (&optional name)
  (make-instance 'cell :name name))

(defmethod print-object ((cell cell) stream)
  (print-unreadable-object
      (cell stream :type t :identity nil)
    (format stream "~@[~s ~]~s (~d listeners)"
            (name cell)
            (content cell)
            (length (listeners cell)))))

(defmethod cellp ((cell cell)) t)

(defmethod cellp ((object t)) nil)


;;; Nothing (how to represent the absence of information)

(defmethod nothingp (x)
  (eq t x))

(defmethod nothingp ((cell cell))
  (nothingp (content cell)))


;;; Propagator

(defclass propagator ()
  ((fn
    :initform (constantly t) ; not sure about this
    :initarg :fn
    :accessor fn)
   (inputs
    :initform nil
    :initarg :inputs
    :accessor inputs
    :documentation "List of input cells that have no content")
   (output
    :initform nil
    :initarg :output
    :accessor output
    :documentation "The output cell"))
  (:documentation "A class used to store computations..."))

#++
(defclass propagator (propagator)
  ((fn
    :initform (constantly t) ; not sure about this
    :initarg :fn
    :accessor fn)
   (ready
    :initform nil
    :accessor ready
    :documentation "List of input cells that have some content.")
   (waiting
    :initform nil
    :initarg :waiting
    :accessor waiting
    :documentation "List of input cells that have no content")
   (output
    :initform nil
    :initarg :output
    :accessor output
    :documentation "The output cell"))
  (:documentation "A class used to store computations..."))


#++
(defun make-propagator (&key inputs output fn)
  (make-instance 'propagator
                 :waiting inputs
                 :output output
                 :fn fn))


(defun make-propagator (&key inputs output fn)
  (make-instance 'propagator
                 :inputs inputs
                 :output output
                 :fn fn))

(defmethod print-object ((propagator propagator) stream)
  (print-unreadable-object
      (propagator stream :type t :identity nil)
    (format stream "~s" (fn propagator))))


;;; Network

(defclass network ()
  ((cells
    :initform nil
    :initarg :cells
    :accessor cells)
   (propagators
    :initform nil
    :initarg :propagators
    :accessor propagators)
   #++ ;; just an idea (for now)
   (links
    :initform nil
    :initarg :links
    :accessor links))
  (:documentation "An explicit representation of the network of cells and propagators."))

(defparameter *network* nil
  "The current propagator network.")

(defun define-cell (&optional name)
  (let ((new-cell (make-cell name)))
    (when *network*
      (push new-cell (cells *network*)))
    new-cell))

(defun define-propagator (&key inputs output fn)
  (let ((new-propagator (make-propagator :inputs inputs
                                         :output output
                                         :fn fn)))
    (when *network*
      (push new-propagator (propagators *network*)))
    new-propagator))

(defun call-with-network (network thunk)
  (let ((*network* network))
    (funcall thunk)))

(defmacro with-network ((network) &body body)
  `(call-with-network
    (progn ,network)
    (lambda () ,@body)))


;;; Contradictions

(defun format-contradiction (contradiction stream)
  (format stream "Contradiction: cannot combine the increment ~s to the existing content ~s."
          (content contradiction)
          (increment contradiction)))

(define-condition contradiction ()
  ((content :reader content :initarg :content)
   (increment :reader increment :initarg :increment))
  (:report format-contradiction)
  (:documentation "Condition used to signal that a contradiction was detected."))

;; Made this a method, so we can have the possibility of signaling
;; more information when specific types contradicts.
(defmethod contradiction (content increment)
  "Method to make an signal a contradiction condition."
  (let ((contradiction (make-condition
                        'contradiction :content content :increment increment)))
    (signal contradiction)
    ;; We expect the signal to be handled, otherwise it's an error
    (error "Unhandled contradiction: ~a"
           (format-contradiction contradiction nil))))

#++
(contradiction 1 2)


;;; Notifying on cell's content change.

(defmethod notify ((cell cell) (listener symbol))
  (funcall listener cell))

(defmethod notify ((cell cell) (listener function))
  (funcall listener cell))

(defmacro deletef (item place)
  `(setf ,place (delete ,item ,place)))

#++
(defmethod notify ((cell cell) (propagator propagator))
  (unless (nothingp cell)
    ;; TODO I should use hash-tables instead
    ;; TODO This ain't right: I can't have the same cell as input multiple times
    (unless (find cell (ready propagator))
      (deletef cell (waiting propagator))
      (push cell (ready propagator)))
    (unless (waiting propagator)
      (add-content (output propagator)
                   (apply (fn propagator)
                          (mapcar #'content (ready propagator)))))))


(defmethod notify ((cell cell) (propagator propagator))
  (unless (some #'nothingp (inputs propagator))
    (add-content (output propagator)
                 (apply (fn propagator)
                        (mapcar #'content (inputs propagator))))))



(defmethod add-listener ((cell cell) listener)
  "Add a listener to a cell."
  (unless (find listener (listeners cell))
    (vector-push-extend listener (listeners cell))
    (notify cell listener)))

(defmethod add-listener ((cells cons) listener)
  "Add a listener to a list of cells."
  (dolist (cell cells)
    (add-listener cell listener)))


;;; add-content, combine, equivalentp and contradictoryp

(defmethod add-content ((cell cell) content)
  (let* ((old-content (content cell))
         (new-content (combine old-content content)))
    (unless (eq old-content new-content)
      (setf (content cell) new-content)
      (map nil (lambda (listener)
                 (notify cell listener))
           (listeners cell)))))

(defmethod add-content (_ (eql t))
  #| this is just an optimization |#)

(defmethod equivalentp (x y)
  (equal x y))

(defmethod contradictoryp (x y)
  ;; This assumes that (equivalentp x y) is false. So everything is
  ;; contradictory, unless there's a specialization for it.
  t)

(defmethod contradictoryp ((x (eql t)) y))
(defmethod contradictoryp (x (y (eql t))))

(defmethod combine :around (x y)
  (cond
    ((equivalentp x y) x)
    ((contradictoryp x y) (contradiction x y))
    (t (call-next-method))))

(defmethod combine (x y)
  (error
   "Don't know how to combine arbitrary data that is not equivalentp nor contradictoryp (~s and ~s)."
   x y))

(defmethod combine ((x (eql t)) y) y)
(defmethod combine (x (y (eql t))) x)



;;; Basic propagators
;;; TODO

(defun c/sum2 (x y c)
  "Add the constraint x = y + c where c is a constant"
  (check-type x cell)
  (check-type y cell)
  (check-type c number)
  (let ((x->y (define-propagator :inputs (list x) :output y
                                 :fn (lambda (d) (- d c))))
        (y->x (define-propagator :inputs (list y) :output x
                                 :fn (lambda (d) (+ d c)))))
    (add-listener x x->y)
    (add-listener y y->x)))

(defun c/sum (sum &rest summands)
  ;; sum = (+ summands)
  (let ((->sum (define-propagator
                   :inputs summands :output sum
                   :fn (lambda (&rest summands) (apply '+ summands)))))
    (add-listener summands ->sum))
  ;; summands_i = sum - (+ other-summands)
  (loop
    :for i :below (length summands)
    :for summand :in summands
    :for other-summands = (loop
                            :for j :below (length summands)
                            :for other-summand :in summands
                            :when (/= i j) :collect other-summand)
    :for inputs = (cons sum other-summands)
    :do (add-listener
         inputs
         (define-propagator
             :inputs inputs :output summand
             :fn (lambda (sum &rest other-summands)
                   (- sum
                      (apply '+ other-summands)))))))


;;; Conditional propagator
;;; TODO


;;; Set utilities (using hash-tables)

;; serapeum:set-hash-table : list -> ht | keys == values

(defun ensure-set (x)
  (etypecase x
    ((or null cons) (serapeum:set-hash-table x))
    (hash-table x)))

(defun set-subset-p (hash-table1 hash-table2 &key strict)
  "Return T if every element in HASH-TABLE1 is also in HASH-TABLE2.
If STRICT is true, HASH-TABLE1 must have less elements than HASH-TABLE2."
  (and (funcall (if strict #'< #'<=)
                (hash-table-count hash-table1)
                (hash-table-count hash-table2))
       (loop :for key :being :the :hash-key :of hash-table1
             ;; we don't check the second value of gethash, which means we
             ;; don't support sets that contains the symbol NIL.
             :unless (gethash key hash-table2)
               :do (return)
             :finally (return t))))

(defun set-union (hash-table1 hash-table2)
  (serapeum:merge-tables hash-table1 hash-table2))

(defmethod equivalentp ((x hash-table) (y hash-table))
  ;; This also compares the values... I don't think it's going to be
  ;; an issue anytime soon.
  (equalp x y))


;;; Contingents

(defclass contingent ()
  ((content
    :initform nil
    :initarg :content
    :accessor content
    :documentation "A value that is contingent on the support.")
   (support
    :initform nil
    :initarg :support
    :accessor support
    :documentation "A set of premises that support the content."))
  (:documentation "A value supported by a set of premises."))

(defun make-contingent (content &optional support)
  (make-instance 'contingent
                 :content content
                 :support (ensure-set support)))

;; TODO print-object
(defmethod print-object ((contingent contingent) stream)
  (print-unreadable-object
      (contingent stream :type t :identity nil)
    (format stream "~s ~s"
            (content contingent)
            (alexandria:hash-table-keys (support contingent)))))

(defmethod equivalent-content-p (x y)
  (equivalentp (content x)
               (content y)))

(defmethod contradictory-content-p (x y)
  (contradictoryp (content x)
                  (content y)))

(defmethod equivalentp ((x contingent) (y contingent))
  (and
   (equivalent-content-p x y)
   (equivalentp (support x) (support y))))

(defmethod contradictoryp ((x contingent) (y contingent))
  (and (not (equivalent-content-p x y))
       (contradictory-content-p x y)))

(defmethod combine ((x contingent) (y contingent))
  ;; remember: "combine" assumes that x and y are nor equivalent, nor
  ;; inconsistent.
  (if
   (equivalent-content-p x y)
   ;; Both contingents represents the same information, but the one
   ;; that has less premises contains more [precise] information,
   ;; choose that one. (TODO this is wrong? validate...)

   ;; TODO Should use set-subset-p instead of <=
   (if (<= (hash-table-count (support x))
           (hash-table-count (support y)))
       x
       y)
   ;; Otherwise, the content is nor equivalent, inconsistent, so we
   ;; can combine them and make a new contingent.
   (make-contingent
    (combine (content x) (content y))
    (set-union (support x) (support y)))))


;;; Ephemerals

(defclass ephemeral ()
  ((content
    :initform nil
    :initarg :content
    :accessor content
    :documentation "A set of contingents (supported values).")
   (timestamp
    :initform most-negative-double-float
    :initarg :timestamp
    :accessor timestamp
    :type number
    :documentation "A timestamo"))
  (:documentation "A timestamped-value. When combine only keep the content of the latest one."))

(defun ephemeral (value &optional (timestamp (/ (get-internal-real-time)
                                                internal-time-units-per-second)))
  (make-instance 'ephemeral :content value :timestamp timestamp))

(defmethod print-object ((ephemeral ephemeral) stream)
  (format stream "(ephemeral ~s ~s)"
          (content ephemeral)
          (timestamp ephemeral)))

(defmethod equivalentp ((x ephemeral) (y ephemeral))
  (equivalent-content-p x y))

(defmethod contradictoryp ((x ephemeral) (y ephemeral))
  (and (contradictory-content-p x y)
       (= (timestamp x) (timestamp y))))

(defmethod combine ((x ephemeral) (y ephemeral))
  (if (> (timestamp x) (timestamp y)) x y))

#++
(let ((x (ephemeral 'a 1))
      (y (ephemeral 'b 2)))
  (combine x y))



;;; Truth Maintenance Systems (TMSes)

(defclass tms ()
  ((content
    :initform nil
    :initarg :content
    :accessor content
    :documentation "A set of contingents (supported values)."))
  (:documentation "Container of values that "))

;; TODO print-object

(defmethod equivalentp ((x tms) (y tms))
  (error "Not implemented yet."))

(defmethod contradictoryp ((x tms) (y tms))
  (error "Not implemented yet."))

(defmethod combine ((x tms) (y tms))
  (error "Not implemented yet."))


;;; Intervals

#|
* /
emptyp
intersect
|#


;;; Truth Maintenance System


;;; Backward chaining

(define-condition request ()
  ((cell
    :reader cell
    :initarg :cell))
  (:documentation "A condition signaled for requesting the content of a cell."))

(defmethod request ((cell cell))
  "Signals a condition of type REQUEST. By conventions, handlers should
return t if it was able to update the cell's content and nil
otherwise."
  (signal 'request :cell cell))


(defun call-with-read-request-handler (thunk)
  "Setup a handler for the REQUEST signals, uses y-or-no-p and read to
query the user."
  (handler-bind
      ((request (lambda (request &aux (cell (cell request)))
                  (cond
                    ((not (y-or-n-p
                           "Do you know the value of ~a" (name cell)))
                     ;; TODO mark the cell as "unknown by the user"
                     ;; it could just be (add-content cell +unknown+)
                     ;; TODO add a contingent value instead
                     )
                    (t (format *query-io*
                               "~&Enter a value for ~a: " (name cell))
                       (let* ((eof (gensym "eof"))
                              (user-value (read *query-io* nil eof)))
                         (unless (eq eof user-value)
                           ;; TODO add a contingent value instead
                           (add-content cell user-value))))))))
    (funcall thunk)))

(defmacro with-read-request-handler (() &body body)
  `(call-with-read-request-handler (lambda () ,@body)))

(setf (documentation 'with-read-request-handler 'function)
      (documentation 'call-with-read-request-handler 'function))

(defun find-propagators-with-output (network output)
  "Find all propagators in NETWORK that has OUTPUT as their output."
  (remove-if-not (lambda (propagator)
                   (eq output (output propagator)))
                 (propagators network)))

;; TODO I think this could easily support deriving multiple goals at
;; the same time
(defun derive (network &rest goals)
  (let ((;; cells that we really don't know their value. a.k.a. "seen"
         seen (make-hash-table))
        ;; cells that we plan on asking for their content
        ;; TODO use a hash-table for "todo"
        (todo (copy-list goals)))
    (flet ((add-todo (propagators)
             "Givens a list of PROPAGATORS and a set of SEEN and TODO add new
cells to the list of todo."
             (dolist (inputs (mapcar #'inputs propagators))
               (dolist (input inputs)
                 (when (and
                        (nothingp input)
                        (not (gethash input seen))
                        ;; TODO use a hash-table for "todo"
                        (not (member input todo)))
                   (push input todo))))
             todo))
      (loop
        :while (and
                ;; The goals are not fulfilled
                (some #'nothingp goals)
                ;; there's still stuff that we can ask
                todo)
        :for current = (pop todo)
        :do (setf (gethash current seen) t)
            (unless (request current)
              ;; If request fails, find propagators that have CURRENT as
              ;; output; add their inputs to the todo list.
              (add-todo (find-propagators-with-output network current))))))
  (alexandria:when-let ((failed (remove-if-not #'nothingp goals)))
    (error "Failed to derive goals ~a" failed))
  (mapcar #'content goals))
