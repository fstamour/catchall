
(truename ".")

(load "propagators.lisp")

(in-package #:catchall.propagators)

(parachute:test *package*)

(make-cell)
(make-cell 'some-name)
(make-propagator)


(trace add-content
       add-listener
       notify
       combine
       equivalentp
       contradictoryp)
(untrace)


