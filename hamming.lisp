(in-package #:common-lisp-user)

(ql:quickload '(#:flexi-streams
                #:random-state
                #:fast-io
                #:damn-fast-priority-queue
                ;; #:trivial-bit-streams
                ))

(defpackage #:hamming
  (:use :cl)
  (:local-nicknames (#:r #:random-state)
                    (#:q #:damn-fast-priority-queue)
                    (#:fio #:fast-io)))

(in-package #:hamming)

;;; Trying to compute the hamming weight of strings (of the same
;;; length) in order to increase the speed of a hamming-distance
;;; k-nearest search.

(defun octets-popcount (octets)
  (declare (type
            (simple-array (unsigned-byte 8) (*))
            octets))
  (loop :for octet :across octets
        :sum (logcount octet)))

#+nil
(octets-popcount
 (flexi-streams:string-to-octets "asdf"))

#+nil
(loop :for octet :across (flexi-streams:string-to-octets "asdf")
      :collect (format nil "~b => ~d"
                       octet
                       (logcount octet)))

;; Generate a bunch of random strings
(progn
  (defparameter *m* 1000
    "Number of strings")
  (defparameter *n* 4
    "Number of bytes in each strings")
  (defparameter *db*
    (coerce
     (let ((generator
             (random-state:make-generator
              :linear-congruence
              42))) ;; <== Seed
       (loop :repeat *m*
             :collect
             (fio:with-fast-output (buffer)
               (loop :for byte = (random-state:random-bytes generator 8)
                     :repeat *n*
                     :do (fast-io:writeu8 byte buffer)))))
     'vector)))

(defun integer-hamming-distance (b1 b2)
  (logcount
   (lognot (logxor b1 b2))))

(defun octets-hamming-distance (s1 s2)
  (loop :for b1 :across s1
        :for b2 :across s2
        :sum (logcount
              (lognot (logxor b1 b2)))))

;; ""Benchmarking"" finding nearest strings using
;; octets-hamming-distance
(time
 (let ((k 5))
   (loop :for s1 :across *db*
         :for queues
           = (loop
               :with queue = (q:make-queue (1+ k))
               :for s2 :across *db*
               :for distance = (octets-hamming-distance s1 s2)
               :do
                  (q:enqueue queue s2
                             ;; The queue is a min-queue, so it'll
                             ;; dequeue the smallest numbers first.
                             ;; But we want it to keep the smallest
                             ;; distances. So we "complement" it.
                             (- (* 8 *n*) distance))
                  (when (> (q:size queue) k)
                    (q:dequeue queue))
               :finally (return queue))
         :collect (cons s1 queues))))
;; => ~ 0.080 seconds


(defun octets-to-bit-vector (octets)
  "Convert an array of octets to a bit vector."
  (let ((bit-vector (make-array
                     (list (* 8 (length octets)))
                     :element-type 'bit)))
    (loop :for octet :across octets
          :for i :from 0
          :do
             (loop :for j :from 7 :downto 0
                   :do
                      (setf (aref bit-vector (+ (- 7 j) (* 8 i)))
                            (ldb (byte 1 j) octet))))
    bit-vector))

#+nil
(octets-to-bit-vector
 (flexi-streams:string-to-octets "asdf"))
#+nil
(format nil "#*~{~8,'0b~}" (coerce
                            (flexi-streams:string-to-octets "asdf")
                            'list))

(defun integer-to-bit-vector (octet bit-vector-size)
  (let ((bit-vector (make-array
                     (list bit-vector-size)
                     :element-type 'bit)))
    (loop :for j :from (1- bit-vector-size) :downto 0
          :do
             (setf (aref bit-vector (- (1- bit-vector-size) j))
                   (ldb (byte 1 j) octet)))
    bit-vector))

;; (integer-to-bit-vector 3 4)


;;; This is where the code went wrong. I tried to compute something
;;; for each strings that I could compare to get an approximation of
;;; the hamming distance. After a lot of tries, I found the
;;; [Gap-Hamming
;;; problem](https://en.wikipedia.org/wiki/Gap-Hamming_problem) which
;;; puts a very high lower bounds on what I can actually achieve
;;;
;;; In theory, give two N-bits strings, it's impossible to compute the
;;; hamming distance between the two strings ±√N using less than n
;;; bits.
;;;
;;; I left the code I used to figure out that my approach couldn't
;;; work. (I found out about the gap-hamming problem later.)

(defun bit-vector-subseq-summary (bit-vector chunk-size)
  (declare (type
            (simple-array bit (*))
            bit-vector))
  (when (/= 1 (logcount chunk-size))
    (error "Chunk size must be a power of 2."))
  (let ((bit-vector-summary (make-array
                             (list (ceiling (length bit-vector) chunk-size))
                             :element-type 'bit))
        (threshold (floor chunk-size 2)))
    (loop :for i :below (floor (length bit-vector) chunk-size)
          :do (setf (aref bit-vector-summary i)
                    (if (> threshold
                           (count-if
                            (complement #'zerop)
                            (subseq bit-vector
                                    (* i chunk-size)
                                    (* (1+ i) chunk-size))))
                        0 1)))
    bit-vector-summary))

#+nil
(let* ((bit-vector
         (octets-to-bit-vector
          (flexi-streams:string-to-octets "asdf")))
       (chunk 4))
  (bit-vector-subseq-summary
   bit-vector chunk))

(defun bit-vector-hamming-distance (bv1 bv2)
  (declare (type
            (simple-array bit (*))
            bv1 bv2))
  (loop :for bit1 :across bv1
        :for bit2 :across bv2
        :count (if (= bit1 bit2) 0 1)))

(defparameter *index*
  (map 'vector #'octets-popcount *db*))



(defun freq (list)
  (let ((ht (make-hash-table)))
    (loop :for x :in list
          :do
             (if (gethash x ht)
                 (incf (gethash x ht))
                 (setf (gethash x ht) 1)))
    ht))


(alexandria:hash-table-alist
 (freq
  (let* ((n 8)
         (chunk-size 8))
    (sort
     (loop :for i :below (expt 2 n)
           :append
           (loop :for j :below (expt 2 n)
                 :for summary1 = (bit-vector-subseq-summary
                                  (integer-to-bit-vector i n) chunk-size)
                 :for summary2 = (bit-vector-subseq-summary
                                  (integer-to-bit-vector j n) chunk-size)
                 :for distance-approx = (* chunk-size (bit-vector-hamming-distance
                                                       summary1
                                                       summary2))
                 :for distance = (integer-hamming-distance i j)
                 :collect (- distance-approx distance)))
     #'<))))

;; distance-approx is always greater than distance, so it's not useful
;; to find the k-nearest.
