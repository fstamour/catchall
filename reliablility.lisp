(in-package #:common-lisp-user)

(ql:quickload '(alexandria))

(defpackage #:reliable
  (:use :cl)
  (:import-from #:alexandria
                #:with-gensyms
                #:once-only))

(in-package #:reliable)

(defvar *rate-limiters* (make-hash-table)
  "Container for the rate limiters.")

(defun make-rate-limiter ()
  "Create and initialise a new rate-limiter."
  (cons (get-universal-time) 0))

(defun rate-limiter-last-time (rate-limiter)
  "Get a rate-limiter's last-time."
  (car rate-limiter))

(defun rate-limiter-count (rate-limiter)
  "Get a rate-limiter's count."
  (cdr rate-limiter))

(defun rate-limiter-update (rate-limiter)
  "Increment a rate-limiter count or update it's time and reset the counter."
  (let ((now (get-universal-time)))
    (if (= now
           (rate-limiter-last-time rate-limiter))
        (incf (cdr rate-limiter))
        (setf (car rate-limiter) now
              (cdr rate-limiter) 1))))

(defmacro rate-limit ((&key name (max-times-per-seconds 1)) &body body)
  "Create a rate limiter and make sure it's respected."
  (check-type name symbol)
  (with-gensyms (rate-limiter)
    (once-only (max-times-per-seconds)
               `(symbol-macrolet ((,rate-limiter (gethash ',name *rate-limiters*)))
                  ;; Initialise the rate-limiter
                  (unless ,rate-limiter
                    (setf ,rate-limiter (make-rate-limiter)))
                  ;; Update the rate-limiter
                  (rate-limiter-update ,rate-limiter)
                  ;; Check the limit
                  (when (<= ,max-times-per-seconds
                            (rate-limiter-count ,rate-limiter))
                    ;; Sleep 1 second if limit is reached
                    (sleep 1))
                  (progn ,@body)))))


;;; Example

(defun double (x)
  (rate-limit (:name double :max-times-per-seconds 1)
              (* x 2)))

;; this should take at least 3 seconds to run
(time
 (loop :for i :below 3 :collect
                       (double i)))


;;; Next:
;; Maybe sleeping for 1 second isn't what we want when we reach the rate limit,
;; Maybe we want to return right away.
;; or return a different value.


(define-condition rate-limit-exceeded () ())

(defmacro rate-limit* ((&key name (max-times-per-seconds 1)) &body body)
  "Create a rate limiter and make sure it's respected."
  (check-type name symbol)
  (with-gensyms (rate-limiter call-body)
    (once-only (max-times-per-seconds)
               `(symbol-macrolet ((,rate-limiter (gethash ',name *rate-limiters*)))
                  ;; Initialise the rate-limiter
                  (unless ,rate-limiter
                    (setf ,rate-limiter (make-rate-limiter)))
                  ;; Update the rate-limiter
                  (rate-limiter-update ,rate-limiter)
                  (flet ((,call-body ()
                           ,@body))
                    (restart-case
                        ;; Check the limit
                        (if (<= ,max-times-per-seconds
                                (rate-limiter-count ,rate-limiter))
                            ;; Signal a rate-limit exeeded condition
                            (error 'rate-limit-exceeded)
                            ;; Run the body
                            (,call-body))
                      (run-anyway ()
                        :report "Execute the rate limited part."
                        (,call-body))))))))

(defun square (x)
  (rate-limit* (:name double :max-times-per-seconds 1)
               (* x x)))

;; This should signal a condition.
(loop :for i :below 3 :collect
                      (square i))

(defmacro with-rate-limit-handler ((&body on-rate-limit-exeeded) &body body)
  `(handler-bind
       ((rate-limit-exceeded #'(lambda (condition)
                                 (declare (ignore condition))
                                 (flet ((run-anyway () (invoke-restart (find-restart 'run-anyway))))
                                   ,@on-rate-limit-exeeded
                                   (run-anyway)))))
     ,@body))

;; Here we get the same behaviour as the first version.
(loop :for i :below 3 :collect
                      (with-rate-limit-handler ((sleep 1))
                        (square i)))


;;; Note (few years after having writen this code), there's a
;;; difference between debouncing and throttling.
