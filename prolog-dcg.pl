/*

and: use ,
or:  use ;
or:  define the rule multiple times
not: use \+

*/

:- set_prolog_flag(double_quotes, chars).


as --> [].
as --> [a], as.

% phrase(as, Ls).


abs --> [].
abs --> [a], abs.
abs --> [b], abs.

% phrase(abs, Ls).
% length(Ls, _), phrase(abs, Ls).

% Describing lists using DCGs
seq([]) --> [].
seq([E|Es]) --> [E], seq(Es).

% ?- phrase(("abc", seq(X), "ghi"), "abcdefghi").
% X = "def"


whitespace(C) :- char_type(C, white) ;
                 char_type(C, end_of_line).

not_whitespace(C) :- \+ whitespace(C).

% 0 or more whitespaces
ws0 --> [W], { whitespace(W) }, ws0.
ws0 --> [].

% 1 or more whitespaces
ws1 --> [W], { whitespace(W) }, ws1.
ws1 --> [W], { whitespace(W) }.


% length(W, _), phrase(ws, W).

% token --> [C], { not_whitespace(C) }, token.
% token --> [C], { not_whitespace(C) }.

% token([C|Cs]) --> [C], { not_whitespace(C) }, token(Cs).
% token([C]) --> [C], { not_whitespace(C) }.

token([C|Cs]) --> [C], { not_whitespace(C) }, token_r(Cs).
token_r([C|Cs]) --> [C], { not_whitespace(C) }, token_r(Cs).
token_r([]) --> [].

tokens([E|Es]) -->
    ws0, token(E), ws0,
    !, % single solution: longest input match
    tokens(Es).
tokens([]) --> [].

% length(T, _), phrase(token, T).

parse(String, Tokens) :- phrase(tokens(Tokens), String).

% ?- parse(" a b ewf", T).
% T = [[a], [b], [e, w, f]].

% ?- phrase_from_file(seq(Chars), "prolog-dcg.pl"), write(Chars).

% ?- phrase_from_file(tokens(Tokens)), "prolog-dcg.pl", ), write(Tokens).

:- use_module(library(readutil)).

?- read_file_to_string("prolog-dcg.pl", S, []), write(S).
