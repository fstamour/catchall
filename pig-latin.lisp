;; Original code:
#|

(ql:quickload "cl-ppcre")

(defun vowelp (chr)
  (cond
    ((not (characterp chr))
     (error "not character"))
    ((not (alpha-char-p chr))
     (error "not an alpha char"))
    ((string-equal (car (multiple-value-list (cl-ppcre:scan-to-strings "[aeiouAEIOU]" (string chr)))) (string chr))
     t)
    ((string-equal (car (multiple-value-list (cl-ppcre:scan-to-strings "{^[aeiouAEIOU]}" (string chr)))) (string chr))
     nil)))

(defun string->pig-latin (str)
  "Returns a string in pig latin"
  (let ((processed-string "")
        (word (cl-ppcre:split "\\s+" str)))
    (labels ((string-processing-helper (lst str)
               (if (null lst) nil
                   (let* ((popped-word (car lst))
                          (first-letter (char popped-word 0))
                          (length-of-word (1- (length popped-word)))
                          (consonant-cluster
                            (car (nreverse
                                   (loop for x from 0 to length-of-word
                                         until (vowelp (char popped-word x))
                                         collect x)))))
                     (if (vowelp first-letter)
                         (progn
                           (setf processed-string
                                 (string-concat
                                   processed-string
                                   popped-word "way "))
                           (string-processing-helper (cdr lst) str))
                         (progn
                           (setf processed-string
                                 (string-concat
                                   processed-string
                                   (subseq popped-word (1+ consonant-cluster) (length popped-word))
                                   (subseq popped-word 0 (1+ consonant-cluster))
                                   "ay "))
                           (string-processing-helper (cdr lst) str)))))
               processed-string)); return string
      (string-processing-helper word str))))
|#



(defun vowelp (chr)
  (cond
    ((not (characterp chr))
     (error "not character"))
    ((not (alpha-char-p chr))
     (error "not an alpha char"))
    ((string-equal (car (multiple-value-list (cl-ppcre:scan-to-strings "[aeiouAEIOU]" (string chr)))) (string chr))
     t)
    ((string-equal (car (multiple-value-list (cl-ppcre:scan-to-strings "{^[aeiouAEIOU]}" (string chr)))) (string chr))
     nil)))

(defun vowelp (char)
  (member char '#.(coerce "aeiou" 'list) :test #'char-equal))


(let* ((popped-word "eater")
       (length-of-word (length popped-word)))
  (loop for x from 0 to length-of-word
        until (vowelp (char popped-word x))
        collect x))

(let* ((popped-word "tractor")
       (length-of-word (length popped-word)))
  (loop for x from 0 to length-of-word
        until (vowelp (char popped-word x))
        collect x))

(let* ((word "tractor"))
  (subseq word 0 (position-if #'vowelp word)))
;; => "tr"

(let* ((word "eater"))
  (subseq word 0 (position-if #'vowelp word)))
;; => ""

;; This might be easier to read
(cond
  (condition branch-if-true)
  (t branch-if-false))

;; Than:
(if condition
    (progn branch-if-true)
    (progn branch-if-false))

```lisp
;; Some refactors, starting from this:

(if (vowelp first-letter)
    (progn
      (setf processed-string
            (string-concat
             processed-string
             popped-word "way "))
      (string-processing-helper (cdr lst) str))
    (progn
      (setf processed-string
            (string-concat
             processed-string
             (subseq popped-word (1+ consonant-cluster) (length popped-word))
             (subseq popped-word 0 (1+ consonant-cluster))
             "ay "))
      (string-processing-helper (cdr lst) str)))
```

```lisp
;; The recursive call to string-processing-helper is the same for both
;; branches, so it can be extracted:

(if (vowelp first-letter)
    (progn
      (setf processed-string
            (string-concat
             processed-string
             popped-word "way ")))
    (progn
      (setf processed-string
            (string-concat
             processed-string
             (subseq popped-word (1+ consonant-cluster) (length popped-word))
             (subseq popped-word 0 (1+ consonant-cluster))
             "ay "))))
(string-processing-helper (cdr lst) str)
```

```lisp
;; Now the 2 progns are not useful anymore, so we can remove them:

(if (vowelp first-letter)
    (setf processed-string
          (string-concat
           processed-string
           popped-word "way "))
    (setf processed-string
          (string-concat
           processed-string
           (subseq popped-word (1+ consonant-cluster) (length popped-word))
           (subseq popped-word 0 (1+ consonant-cluster))
           "ay ")))
(string-processing-helper (cdr lst) str)
```

```lisp
;; We can easily see that for both branch, we set the same variable,
;; we can invert the setfs and the if:

(setf processed-string
      (if (vowelp first-letter)
          (string-concat
           processed-string
           popped-word "way ")
          (string-concat
           processed-string
           (subseq popped-word (1+ consonant-cluster) (length popped-word))
           (subseq popped-word 0 (1+ consonant-cluster))
           "ay ")))
(string-processing-helper (cdr lst) str)
```

```lisp
;; We can try to do the same thing with string-concat, but I think
;; it's a matter of preference whether this new version is better or
;; not

(setf processed-string
      (apply #'string-concat processed-string
             (if (vowelp first-letter)
                 (list popped-word "way ")
                 (list
                  (subseq popped-word (1+ consonant-cluster) (length popped-word))
                  (subseq popped-word 0 (1+ consonant-cluster))
                  "ay "))))
(string-processing-helper (cdr lst) str)
```


(defun pig-latinize-word (word)
  (let ((first-vowel (position-if #'vowelp word)))
    (format nil "~{~a~}"
            (if (zerop first-vowel)
                (list word "way")
                (list
                 (subseq word first-vowel (length word))
                 (subseq word 0 first-vowel)
                 "ay")))))

(defun pig-latinize-word (word)
  (let ((first-vowel (position-if #'vowelp word)))
    (concatenate
     'string
     (subseq word first-vowel (length word))
     (if (zerop first-vowel) "w" (subseq word 0 first-vowel))
     "ay")))

(defun wordp (string)
  (every #'alpha-char-p string))

(defun pig-latinize-word (word)
  (if (wordp word)
      (let* ((first-vowel (position-if #'vowelp word))
             (prefix (subseq word first-vowel (length word)))
             (suffix (if (zerop first-vowel) "w" (subseq word 0 first-vowel)))
             (capitalizedp (upper-case-p (char word 0)))
             (pig-latinize-word (string-downcase
                                 (concatenate 'string prefix suffix "ay"))))
        (if capitalizedp
            (nstring-capitalize pig-latinize-word)
            pig-latinize-word))
      word))

(mapcar #'pig-latinize-word
        '("pig"
          "latin"
          "banana"
          "friends"
          "smile"
          "string"
          "eat"
          "omelet"
          "are"
          "every"
          "another"))
=>
("igpay" "atinlay" "ananabay" "iendsfray" "ilesmay" "ingstray" "eatway"
         "omeletway" "areway" "everyway" "anotherway")



||```
;;; Now with capitalization! It also leaves the "non word" parts alone

(defun vowelp (char)
  (member char '#.(coerce "aeiou" 'list) :test #'char-equal))

(defun wordp (string)
  (and (plusp (length string))
       (every #'alpha-char-p string)))

(defun pig-latinize-word (word)
  (if (wordp word)
      (let* ((first-vowel (position-if #'vowelp word))
             (prefix (subseq word first-vowel (length word)))
             (suffix (if (zerop first-vowel)
                         "w"
                         (subseq word 0 first-vowel)))
             (capitalizedp (upper-case-p (char word 0)))
             (pig-latinized-word (string-downcase
                                  (concatenate 'string prefix suffix "ay"))))
        (if capitalizedp
            (nstring-capitalize pig-latinized-word)
            pig-latinized-word))
      word))

(defun pig-latinize-sentence (string)
  (format nil "~{~a~}"
          (mapcar #'pig-latinize-word
                  (cl-ppcre:split "((?:\\s|[^\\w])+)" string
                                  :with-registers-p t))))

(pig-latinize-sentence "==> Look ma! <===")
;; "==> Ooklay amay! <==="

```||
