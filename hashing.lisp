(defpackage #:catchall.hashing
  (:documentation "Trying to a dead-simple custom hash-table")
  (:use #:cl))

(in-package #:catchall.hashing)

;; The plan: just use a plain hash-table, but use the result of a
;; custom hash function as the keys and figure out some collision
;; resolution scheme (this implies a custom comparison function).
;;
;; The "value" associated with each key will be a vector of cons
;; (one cons for each collision).
;;
;; The hash and comparison functions will be generic functions. It's
;; not as customizable/flexible as having some object that holds the 2
;; functions, but it's much simpler IMHO.
;;

;; Default hash
(defmethod hash ((object t))
  (sxhash object))

;; Default comparison
(defmethod compare ((a t) (b t))
  (equalp a b))


(defmethod get-custom-hash (key hash-table)
  (let* ((hash (hash key))
         (entries (gethash hash hash-table)))
    (if (vectorp entries)
        (let ((position (position key entries :test #'compare :key #'car)))
          (if position
              ;; found!
              (values (cdr (aref entries position)) t)
              ;; not found
              (values nil nil)))
        ;; not found
        (values nil nil))))

(defmethod set-custom-hash (key hash-table value)
  (let* ((hash (hash key))
         (entries (gethash hash hash-table
                           (make-array 0
                                       :element-type 'cons
                                       :adjustable t
                                       :fill-pointer 0))))
    ;; Add the vector of entries to the hash-table
    (when (zerop (length entries))
      (setf (gethash hash hash-table) entries))
    (let ((position (position key entries :test #'compare :key #'car)))
      (if position
          ;; found! replace the entrie's value
          (setf (cdr (aref entries position)) value)
          ;; not found, add a new entry
          (vector-push-extend (cons key value) entries)))))

;; TODO map-custom-hash; with-custom-hash-iterator (maybe)
;; TODO custom-hash-keys, custom-hash-values


;;; Smoke tests

(let ((h (make-hash-table)))
  (set-custom-hash 1 h 'a)
  (list
   (multiple-value-list (get-custom-hash 1 h))
   (multiple-value-list (get-custom-hash 2 h))))
((A T) (NIL NIL))

(let ((h (make-hash-table)))
  (set-custom-hash nil h 'a)
  (multiple-value-list (get-custom-hash nil h)))


;; Forcing hash collisions
(defmethod hash ((n fixnum))
  (mod (call-next-method) 5))

(let ((h (make-hash-table)))
  (loop :for i :below 100 :do
    (set-custom-hash i h (1+ i)))
  (list
   h
   (loop :for i :below 100 :collect (get-custom-hash i h))))

;; undoing that hack
(remove-method #'hash (find-method #'hash () (list (find-class 'fixnum))))

(let ((h (make-hash-table)))
  (loop :for i :below 100 :do
    (set-custom-hash i h (1+ i)))
  (list
   h
   (loop :for i :below 100 :collect (get-custom-hash i h))))


;;; A dummy, but proper, example

(defstruct (list-as-set)
  (elements () :type cons))

(defmethod hash ((set list-as-set))
  (sxhash
   (loop :for el :in (list-as-set-elements set)
         :sum (hash el))))

;; This is not efficient, this is just to give an example
(defmethod compare ((a list-as-set) (b list-as-set))
  (let ((as (list-as-set-elements a))
        (bs (list-as-set-elements b)))
    (and (= (length as) (length bs))
         (every (lambda (ai)
                  (member ai bs :test #'compare))
                as))))

(=
 (hash (make-list-as-set :elements '(a b c)))
 (hash (make-list-as-set :elements '(a c b)))
 (hash (make-list-as-set :elements '(b a c)))
 (hash (make-list-as-set :elements '(b c a)))
 (hash (make-list-as-set :elements '(c a b)))
 (hash (make-list-as-set :elements '(c b a))))
;; => T

;; We're able to get the same element we inserted (even though they
;; are actually 2 different objects with the same structure.
(let ((h (make-hash-table)))
  (set-custom-hash (make-list-as-set :elements '(a b c)) h t)
  (get-custom-hash (make-list-as-set :elements '(a b c)) h))

;; This is were the magic shows, we have two different objects (with
;; different content) that hashes to the same thing and for which
;; #'compare returns true:
(let ((h (make-hash-table)))
  (set-custom-hash (make-list-as-set :elements '(a b c)) h 42)
  (get-custom-hash (make-list-as-set :elements '(a c b)) h))


;; It works with nested structures:
(let ((h (make-hash-table)))
  (set-custom-hash (make-list-as-set
                    :elements
                    `(c ,(make-list-as-set :elements '(d e f)) a))
                   h 42)
  (get-custom-hash (make-list-as-set
                    :elements
                    `(c a ,(make-list-as-set :elements '(e d f))))
                   h))
