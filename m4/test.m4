m4_divert(-1)
m4_changequote()
m4_changequote([,])

m4_ifdef([__gnu__], ,
[errprint(GNU M4 required.)
m4exit(2)])



# The LI_COUNT macro is redefined every time the H2 macro is used:
m4_define([LI_COUNT], 0)

m4_define([li], [m4_define([LI_COUNT], m4_incr(LI_COUNT))LI_COUNT. $1])

m4_define([probe_file],
        [m4_divert(1)import os.path[]m4_divert(2)os.path.exits($1)m4_divert(0)])
m4_divert(0)m4_dnl
probe_file([x])
m4_undivert(1)

li(hello)
li(hi)
li(allo)
m4_undivert(2)

Generated m4_esyscmd([date])
