(in-package #:common-lisp-user)

(defpackage #:human-sort
  (:use :cl)
  (:shadow #:sort))

(in-package #:human-sort)

(ql:quickload '(alexandria))

;; Make it easier to debug
(declaim (optimize (speed 0) (safety 3) (debug 3)))

#|

Goal: Try out algorithm to manually sort items

Motivation: Some things are subjective, so you can't sort them
automatically.  Other things are just too costly to sort
automatically. For example, you might want to sort photos by their
content, it's possible to use machine learning for that, but it's not
worth it if you're only going to do it once.

First lead: Binary insertion sort is interesting because it performs
n*log_2(n) comparisons in the worst case. Which is useful when the
cost of comparisons is higher that the cost of swaps. Which is the
case when asking a user to sort items.

Finally, here we concentrate on total ordering, which is not usually
what a human want. For example, when a user might want to sort his
movies by preference (how much he likes the movie), he would probably
prefer to just rate (which is linear) each movies and automatically
sort by that instead of actually sorting (which is super-linear) all
the movies by preference.

N.B. While using a human to compare elements, it is probably still
possible to use a function to test for equality.

N.B.Memoization (caching) is very nice, but what if you made a
mistake?

|#


;;; Section about asking the user, caching the answers and avoiding
;;; asking things we can deduce (assuming the comparison is transitive
;;; and symmetric).
;;;
;;; Transitivity: if a < b and b < c then a < c.
;;;
;;; Symmetry: if a < b then b > a

;; Let's say I have n items, I don't want any loop to last more than
;; (n * log_2(n)) iterations.
(defparameter *infinite-loop-guard*
  (let ((n 1000))
    (* n (expt n 2))))

(defparameter *comparison-cache* (make-hash-table :test 'equalp))

(defun clear-comparison-cache ()
  (clrhash *comparison-cache*))
;; (clear-comparison-cache)

(defmacro with-comparison-cache (&body body)
  `(let ((*comparison-cache* (make-hash-table :test 'equalp)))
     ,@body))

(defun %ask-user (a b)
  "Actually ask the user if a is lesser than b."
  (y-or-n-p "~&Should \"~a\" be sorted before \"~a\"? " a b))
;; (ask-user 1 2)

(defparameter *ask-user-function*
  ;; #'< ;; For automated tests
  #'%ask-user
  "Configure which function to call to ask the user to compare items.")

(defun ask-user (a b)
  "Calls *ask-user-function* to ask the user if a is lesser than b."
  (funcall *ask-user-function* a b))
;; (ask-user 1 2)

(defun ask-cache (a b &optional (comparison-cache *comparison-cache*))
  "Check in the cache if we know the answer.
Returns 2 value, LESSERP and FOUNDP (just like gethash)"
  (multiple-value-bind (lesserp foundp)
      (gethash (cons a b) comparison-cache)
    (if foundp
        (values lesserp foundp)
        (gethash (cons b a) comparison-cache))))

(defun ask-user-and-save (a b)
  "Ask the user and save the answer in *comparison-cache*."
  (let ((answer (ask-user a b)))
    (setf (gethash (if answer
                       (cons a b)
                       (cons b a))
                   *comparison-cache*)
          t)
    answer))

(defun dig (directed-graph starting-point looking-for
            &key (test #'eq))
  "This is a depth-first point-to-point path-finding on a directed-graph.
It is used to figure out if \"starting-point\" is lesser than
\"looking-for\". It uses *comparison-cache* as the directed-graph.  It
needs to be called up to 2 times to figure out if we need to ask the
user (see function \"compare\").
"
  (if (funcall test starting-point looking-for)
      nil ;; they are equal, hence they're not "lesser-than".
      (let* ((visited-edge (make-hash-table :test 'equalp))
             (stack (list starting-point)))
        (loop
          ;; infinite loop guard
          :for i :below 100000000
          ;; (hash-table-count directed-graph)
          :for current = (when stack (pop stack))
          :while current :do
            ;; (format t "~&Stack: ~a" stack)
            (loop :for (a . b) :being :the :hash-key :of directed-graph
                  :do
                     (when (funcall test current a)
                       (when (funcall test b looking-for)
                         (return-from dig t))
                       (unless (gethash (cons a b) visited-edge)
                         (setf (gethash (cons a b) visited-edge) t)
                         ;; (format t "~&To visit: ~s" b)
                         (push b stack))))))))

;; 0 is lesser than 1
(dig
 (alexandria:plist-hash-table '((0 . 1) t))
 0 1)
;; => T

;; we don't know if 1 is lesser than 0
;; we would need to dig the other way around
(dig
 (alexandria:plist-hash-table '((0 . 1) t))
 1 0)
;; => NIL

;; 0 is not lesser than 0
(dig
 (alexandria:plist-hash-table '((0 . 1) t))
 0 0)
;; => NIL

;; We don't know about 2
(dig
 (alexandria:plist-hash-table '((0 . 1) t))
 0 2)
;; => 2

;; Deeper
(dig
 (alexandria:plist-hash-table '((0 . 1) t
                                (1 . 2) t))
 0 2)
;; => T

(defun interactive-lessp (a b)
  ;; Depth-first search starting from a
  (if (dig *comparison-cache* a b)
      ;; a is lesser than b
      t
      ;; Depth-first search starting from b
      (if (dig *comparison-cache* b a)
          ;; We know that a is not lesser than b
          nil
          ;; We still don't know, ask the user.
          (ask-user-and-save a b))))

;; (clear-comparison-cache)
;; The first time, it should ask
;; (interactive-lessp 1 2)
;; The second time, it shouldn't
;; (interactive-lessp 2 1)

;; Let's add anoter comparison to the cache to test transitive search.
;; (interactive-lessp 1 0)
;;
;; The cache should look like this now.
;; (alexandria:hash-table-plist *comparison-cache*)
;; => ((0 . 1) T (1 . 2) T)
;;
;; Now we want to interactive-lessp 2 and 0 without asking the user
;; (interactive-lessp 2 0)
;; => NIL
;; (interactive-lessp 0 2)
;; => T


;;; Section on the sorting itself.

(defun find-insertion-point (sorted-array item
                             &key (test #'eq) (key #'identity))
  "Compute where to insert the ITEM in the SORTED-ARRAY.
It uses a linear scan to find the first equal element.  Then it uses a
binary search to find the right place with less comparisons."
  (or
   ;; If nothing is already sorted
   (when (zerop (length sorted-array)) 0)
   ;; Do a linear scan first
   (position item sorted-array :test test :key key)
   ;; if not found, do a binary search
   (let ((direction nil)) ;; nil is down, t is up
     (loop
       ;; infinite loop guard
       :for i :below (1+ (ceiling (log (length sorted-array) 2)))
       :for low = 0 :then (if direction (1+ middle) low)
       :for high = (length sorted-array) :then (if direction high middle)
       :for middle = (+ low (floor (- high low) 2))
       :for element = (if (< middle (length sorted-array))
                          (funcall key (aref sorted-array middle))
                          (return-from find-insertion-point middle))
       :do
          (setf direction (interactive-lessp element item))
          (when (<=  0 (- high low) 1)
            (return-from find-insertion-point (if direction
                                                  (1+ middle)
                                                  middle)))
       #+nil
        (format t "~&l: ~a; m=~a; h: ~a; element=~a; direction=~a"
                low middle high element direction)))))

(find-insertion-point #() 1)
;; => 0

(find-insertion-point #(0) 1)
;; => 1

(find-insertion-point #(0 1) 1)
;; => 1

(find-insertion-point #(0 1 2) 1)
;; => 1

(find-insertion-point #(1) 0)
;; => 0

(find-insertion-point #(0 1 2) 2)
;; => 2

(find-insertion-point #(0 1 2) 3)
;; => 3

(find-insertion-point #(0 1 2 4) 3)
;; => 3

(find-insertion-point #(-2 1 1 2 3 4 5 7 7 8) 6)
;; => 7

;; bigger test
(let ((seq #(-2 1 1 2 3 4 5 7 7 8)))
  (equalp
   (loop :for el :across seq
         :collect (find-insertion-point seq el))
   '(0 1 1 3 4 5 6 7 7 9)))

;; similar test with new, empty cache
(with-comparison-cache
    (let ((seq #(-2 1 1 2 3 4 5 7 7 8)))
      (find-insertion-point seq 6)))

(defun insert (array item position)
  "Insert an element in an array."
  (let ((array
          ;; adjust-array is not required to modify the array, it might
          ;; return a new one.
          (adjust-array array (1+ (length sequence))
                        :initial-element nil)))
    (loop
      :for i :from (1- (length array)) :downto (1+ position)
      :do (setf (aref array i) (aref sequence (1- i))))
    (setf (aref array position) item)
    array))

#|
(let ((seq #(0 1 2)))
(insert seq 'x 0))
;; => #(X 0 1 2)

(let ((seq #(0 1 2)))
(insert seq 'x 3))
;; => #(0 1 2 X)

(let ((seq #(0 1 2)))
(insert seq 'x 2))
;; => #(0 1 X 2)
|#


(defun interactive-sort (sequence &key (key #'identity))
  "Insertion sort."
  (let ((sorted-result (make-array 1 :initial-contents
                                   (list (aref sequence 0)))))
    (loop
      :for i :from 1 :to (1- (length sequence))
      :for element = (aref sequence i)
      :for insertion-point = (find-insertion-point sorted-result element
                                                   :test #'equalp
                                                   :key key)
      :do (setf sorted-result
                (insert sorted-result element insertion-point)))
    sorted-result))

(with-comparison-cache
    (interactive-sort #(5 6 3 6)))

(with-comparison-cache
    (interactive-sort #(5 6 3 6 2 2 2 1 4 4 4)))
;; 11 items
;; 6 unique items
;; 16 questions

;; I wonder if the with-comparison-cache work as I expect?

(with-comparison-cache
    (interactive-lessp 1 2)
  (interactive-lessp 1 2))
;; looks like it does.
