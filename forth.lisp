(defpackage #:catchall.forth
  (:documentation "")
  (:use #:cl))

(in-package #:catchall.forth)

(defun %read-token (stream)
  (let ((token (with-output-to-string (buffer)
                 (loop
                   :for next-char = (peek-char t stream nil)
                     :then (peek-char nil stream nil)
                   :while (and next-char
                               (not (eq #\Space next-char))
                               (graphic-char-p next-char))
                   :do (write-char (read-char stream) buffer)))))
    (unless (zerop (length token))
      (if (every #'digit-char-p token)
          (parse-integer token)
          (intern (string-upcase token) #.*package*)))))

#+nil
(mapcar #'(lambda (string)
            (with-input-from-string (stream string)
              (loop :for token = (%read-token stream)
                    :while token
                    :collect token)))
        '(""
          " "
          "a"
          " b"
          " c "
          " 1  2 "
          " a-longer-token "
          "look!
 no return"
          "no	tabs either "))
;; => (NIL NIL (A) (B) (C) (1 2) (A-LONGER-TOKEN) (LOOK! NO RETURN)
;; (NO TABS EITHER))

(let ((program "
2 1 + .
dup + 8 .
swap . - .
drop 5 2 /mod
drop drop ."))
  (with-input-from-string (input program)
    (macrolet ((fn-alist (fn-names)
                 `(list ,@(mapcar (lambda (name)
                                    `(cons ',name (function ,name)))
                                  fn-names))))
      (let (stack dictionnary)
        (labels #1=((dup () (push (car stack) stack))
                    (drop () (pop stack))
                    (swap () (rotatef (first stack) (second stack)))
                    (rot () (rotatef (first stack)
                                     (second stack)
                                     (third stack)))
                    ;; TODO Over, 2swap, 2drop, nip, tuck |.|, negate
                    (/mod ()
                          (let ((divisor (pop stack))
                                (number (pop stack)))
                            (multiple-value-bind (quotient remainder)
                                (floor number divisor)
                              (push remainder stack)
                              (push quotient stack))))
                    (funcall2 ()
                              (push (funcall (pop stack)
                                             (pop stack)
                                             (pop stack))
                                    stack))
                    (read-token ()
                                (let ((token (%read-token input)))
                                  (when token
                                    (etypecase token
                                      (number token)
                                      (symbol token)))))
                    (next ()
                          (let* ((word (read-token))
                                 (definition
                                   (cdr (assoc word dictionnary))))
                            ;; (format t "~&~a" word)
                            (when word
                              (cond
                                ((functionp definition)
                                 (funcall definition))
                                ((member word '(+ - * mod))
                                 (push (funcall word
                                                (pop stack)
                                                (pop stack))
                                       stack))
                                ((and definition
                                      (listp definition)) :todo)
                                (t (push word stack)))
                              ;; (format t "~&~a" stack)
                              t)))
                    (|:| ()
                         ;; TODO Read until ";"
                         )
                    (|.| ()
                         (print (car stack))))
          (setf dictionnary (fn-alist #. (mapcar #'car '#1#)))
          (loop
            :for not-done = (next)
            :while not-done)
          stack)))))
