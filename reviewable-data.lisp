(in-package #:common-lisp-user)

(defpackage #:reviewable-data
  (:use :cl))

(in-package #:reviewable-data)

(defparameter *root* "~/programmation/lisp/")

(defun read-all (stream &optional list)
  (let ((result list)
        (eof (gensym "eof")))
    (loop :for form := (read stream nil eof)
          :while (not (eq eof form))
          :do (push form result))
    result))


(defparameter +sample+ ";; a sample kb to test \"read-all\"
 (a b c)
 (d e f)")

(equal
 (with-input-from-string (stream +sample+)
   (read-all stream))
 '((D E F) (A B C)))

(equal
 (with-input-from-string (stream +sample+)
   (let ((list '((y e z) (h i j))))
     (read-all stream list)))
 '((D E F) (A B C) (Y E Z) (H I J)))


(defparameter *common-lisp* (merge-pathnames "common-lisp.data" *root*))

(with-open-file (stream *common-lisp*
                        :direction :output
                        :if-exists nil
                        :if-does-not-exist :create)
  (do-external-symbols (symbol 'cl)
    (let ((fboundp (fboundp symbol)))
      (format stream "~&~s" (list symbol :fboundp (when fboundp t)))
      (when fboundp
        (format stream "~&~s" (list symbol :pure :unknown))))))



(ql:quickload '(alexandria optima))

(defparameter *kb* (with-open-file (stream *common-lisp*)
                     (read-all stream)))

(defun index-kb (&optional (knowledge-base *kb*))
  (let* ((kb (reverse Knowledge-base))
         (index (make-hash-table)))
    (loop :for (identifier . properties) :in kb
          :do
             (if (gethash identifier index)
                 (push properties (gethash identifier index))
                 (setf (gethash identifier index) (list properties))))
    index))

(defparameter *kb-index* (index-kb *kb*))


(defun first-if-only (list)
  (if (null (rest list))
      (first list)
      list))

(defun get-property-history (identifier property
                             &optional (kb-index *kb-index*))
  (let* ((properties (gethash identifier kb-index))
         (values (loop :for (prop . value) :in properties
                       :when (eq prop property)
                         :collect value)))
    values))

(defun get-property (identifier property &optional (kb-index *kb-index*))
  (let ((history (get-property-history identifier property kb-index)))
    (values
     (first-if-only (car history))
     (not (null (rest history))))))

(get-property 'nsubst :pure)
;; => NIL, T
;; :pure it NIL and has been reviewed before

(get-property 'nsubst :fboundp)
;; => T, NIL
;; :fboundp it T, but has never been reviewed before



(defun certainty (list)
  (let ((histogram (make-hash-table :test 'equal))
        (delta (/ 1 2))
        (total 0))
    (loop :for element :in list
          :do
             (if (gethash element histogram)
                 (incf (gethash element histogram) delta)
                 (setf (gethash element histogram) delta))
             (incf total delta)
             (setf delta (/ delta 2)))
    ;; Adjust the results to make it a probability distribution.
    (maphash #'(lambda (key value)
                 (setf (gethash key histogram) (/ value total)))
             histogram)
    histogram))

(alexandria:hash-table-alist
 (certainty
  (remove '(:unknown)
          (get-property-history 'nsubst :pure)
          :test #'equal)))


;;; Start over
;; Here's what a user might want do do:
;; - Get the next fact to review
;; - Get the previous (n) fact (to change a review he just made)
;; - Undo
;; - Add a new review
;; - Add a new fact


;; In sbclrc:
#+nil
(setf (logical-pathname-translations "CATCHALL")
      '(("**;*.*" "~/catchall/**/*.*")))

(defparameter *kb-file* (translate-logical-pathname "catchall:reviewable-knowledge-base.sexp"))

(defparameter *kb* nil
  "Knowledge base")

(defun add-fact (fact)
  (push fact *kb*)
  (when *kb-file*
    (with-open-file (kb-file *kb-file*
                             :direction :output
                             :if-exists :append
                             :if-does-not-exist :create)
      (format kb-file "~a~%" fact))))

(defun get-previous-fact (n &optional (kb *kb*))
  (nth n kb))

(defun get-next-fact (&optional (kb *kb*))
  (loop
    :for (head . rest) :on kb
    :unless (find (first head) rest :key #'first)
      :return head))

(defmacro with-kb (() &body body)
  `(let ((*kb* ()))
     (progn
       ,@body)))

(with-kb () (add-fact '(is-a cat mammal)) *kb*)

(with-kb () (map nil #'add-fact '(a b c))
  (values *kb*
          (get-previous-fact 1)
          (get-previous-fact 2)))

(with-kb ()
  (add-fact '(is-a cat mammal))
  (get-next-fact))

(with-kb ()
  ;; Add a fact
  (add-fact '(cat is-a dog))
  ;; Correct a fact
  (add-fact '(cat is-a mammal))
  ;; Add a fact
  (add-fact '(dog is-a mammal))
  ;; Get the next "unrevied" fact
  (get-next-fact))
;; => (DOG IS-A MAMMAL)


#|
;; Emacs lisp/slime interface

(slime-interactive-eval "(reviewable-data::add-fact '(= (+ 2 2) 5))")

(destructuring-bind (output value)
    (slime-eval `(swank:eval-and-grab-output "(reviewable-data::get-next-fact)"))
  value)
|#
