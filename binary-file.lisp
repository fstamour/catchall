(in-package #:common-lisp-user)

(defpackage #:binary-file
  (:use :cl))

(in-package #:binary-file)

;; Goal: design some simple binary formats to store
;; - a set of string, I want it to support any kind of strings.
;;
;; Some keywords that are related: netstring, bencode

(ql:quickload '(;; standard utils
                alexandria
                ;; facilities to read different encoding
                flexi-streams
                ;; fast (buffered) byte vector read/write
                fast-io
                ;; a bit like fast-io, but works with "normal" streams
                nibbles))

;; I wasn't able to find how to "seek" with fast-io's "input buffer".
;; So I use flexi-streams + nibbles when I need to.

#| 2020-06-30
Conclusion of this session:
- it works well!
- functions to read/write sequence of bytes would be more generic that what
I wrote (working with strings)
- an actual file format shoul have a header, at least a magic number
- The current limit for an entry is currently 4GB (because I used 32-bits) to
indicate the size of the entry. Maybe provide something to read huge entries
piece by piece.

- What if we want to store bigger entries?
- maybe just save the entry as a file by itself.
- maybe just use tar...
- use bigger size (e.g. 64bits would permit 1TB per entry)
- make the size represent "pages" instead of "bytes", would require to save
the padding too, which is a small price compared to the rest.
- use variable length integers for the size (a la utf-8)

- What if we want to store many small entries? each entrie would have a non-
negligible overhead.
- use variable length integers for the size (a la utf-8)
- use smaller size
- sort the entries by size and only save the number of entries per size
e.g. (x entries of size y)+; that would give a good read/seek speed.
it would even let you search by size pretty efficiently
at the cost of pre-sorting. If there are really that many entries, it
might be impossible to sort in-memory


Related packages:
- https://github.com/conspack/cl-conspack
- zcdb

|#

(defun binary-write-string (string
                            buffer
                            &key (external-format :utf8))
  "Write the length of the string and the string"
  (let* ((octets (flexi-streams:string-to-octets
                  string
                  :external-format external-format))
         (length (length octets)))
    (fast-io:writeu32-le length buffer)
    (fast-io:fast-write-sequence octets buffer)))

(fast-io:with-fast-output (buffer)
  (binary-write-string "this is a dummy string" buffer))

;; =>
#(22 0 0 0 116 104 105 115 32 105 115 32 97 32 100 117 109 109 121
  32 115 116 114 105 110 103)

(fast-io:with-fast-output (buffer)
  (binary-write-string "this is a string with şömé unicode" buffer))

(defun binary-read-string (buffer
                           &key (external-format :utf8))
  "Read a 32-bit length and then read that many bytes into a string"
  (let* ((length (fast-io:readu32-le buffer))
         (sequence (fast-io:make-octet-vector length)))
    (fast-io:fast-read-sequence sequence buffer)
    (flexi-streams:octets-to-string
     sequence
     :external-format external-format)))

(fast-io:with-fast-input (buffer
                          (fast-io:with-fast-output (buffer)
                            (binary-write-string "this is a dummy string" buffer)))
  (binary-read-string buffer))
;; => "this is a dummy string"

(fast-io:with-fast-input (buffer
                          (fast-io:with-fast-output (buffer)
                            (binary-write-string "this is a string with şömé unicode" buffer)))
  (binary-read-string buffer))
;; => "this is a string with şömé unicode"


;;; Now trying to serialize multiple strings

(defparameter *strings* '("this"
                          "is"
                          "a"
                          "bunch"
                          "of"
                          "strings"))

(defmacro with-binary-string-output
    ((buffer &key (external-format :ut8)) &body body)
  `(fast-io:with-fast-output (,buffer)
     ,@body))

(defparameter *binary-strings*
  (with-binary-string-output (buffer)
    (map nil #'(lambda (string)
                 (binary-write-string string buffer))
         *strings*)))

(defmacro with-binary-string-output-to-file
    ((buffer filespec &key
                        ;; TODO add the keys of with-open-file
                        ) &body body)
  (alexandria:with-gensyms (output)
    `(with-open-file
         (,output ,filespec
                  :direction :output
                  :element-type '(unsigned-byte 8)
                  :if-exists :overwrite
                  :if-does-not-exist :create)
       (fast-io:with-fast-output (,buffer ,output)
         ,@body))))

(defparameter *test-file* "/tmp/strings.bin")

;; Write to test file
(with-binary-string-output-to-file
    (buffer *test-file*)
  (map nil #'(lambda (string)
               (binary-write-string string buffer))
       *strings*))


;;; and de-serialize

(fast-io:with-fast-input (buffer *binary-strings*)
  (loop :for string = (handler-case (binary-read-string buffer)
                        (end-of-file (condition) nil))
        :while string
        :collect string))
;; => ("this" "is" "a" "bunch" "of" "strings")

(defmacro with-binary-string-input-to-file
    ((buffer filespec &key
                        ;; TODO add the keys of with-open-file
                        ) &body body)
  (alexandria:with-gensyms (input)
    `(with-open-file
         (,input ,filespec
                 :direction :input
                 :element-type '(unsigned-byte 8))
       (fast-io:with-fast-input (,buffer nil ,input)
         ,@body))))

(with-binary-string-input-to-file (buffer *test-file*)
  (loop :for string = (handler-case (binary-read-string buffer)
                        (end-of-file (condition) nil))
        :while string
        :collect string))
;; => ("this" "is" "a" "bunch" "of" "strings")



;; Simple [in-memory or not] index

(defun index-buffer (buffer)
  (coerce
   (loop :for length = (handler-case (fast-io:readu32-le buffer)
                         (end-of-file (condition)
                           (declare (ignore condition)) nil))
         :while length
         :collect (prog1 (if (fast-io:input-buffer-stream buffer)
                             (file-position
                              (fast-io:input-buffer-stream buffer))
                             (fast-io:buffer-position buffer))
                    ;; advance the buffer...
                    (loop :for i :below length
                          :do (fast-io:fast-read-byte buffer))))
   'vector))


(defparameter *index*
  (fast-io:with-fast-input (buffer *binary-strings*)
    (index-buffer buffer)))
;; #(4 12 18 23 32 38)

(defun index-file (filespec)
  (with-open-file (input filespec
                         :direction :input
                         :element-type '(unsigned-byte 8))
    (fast-io:with-fast-input (buffer nil input)
      (index-buffer buffer))))

(index-file *test-file*)
;; => #(4 12 18 23 32 38)


;;; Using an index

;; Reading the nth element

(let* ((index *index*)
       (n 3)
       (position (aref index n)))
  (flexi-streams:with-input-from-sequence
      (stream *binary-strings*)
    (file-position stream (- position 4))
    (let* ((length (nibbles:read-ub32/le stream))
           (sequence (nibbles:make-octet-vector length)))
      (read-sequence sequence stream)
      (flexi-streams:octets-to-string
       sequence
       :external-format :utf8))))
;; => "bunch"

(defun binary-string-aref (input-stream index nth)
  (let ((position (aref index nth)))
    (file-position input-stream (- position 4)))
  (let* ((length (nibbles:read-ub32/le input-stream))
         (sequence (nibbles:make-octet-vector length)))
    (read-sequence sequence input-stream)
    (flexi-streams:octets-to-string
     sequence
     :external-format :utf8)))


(flexi-streams:with-input-from-sequence
    (stream *binary-strings*)
  (loop :for i :below (length *index*)
        :collect (binary-string-aref stream *index* i)))
;; => ("this" "is" "a" "bunch" "of" "strings")
